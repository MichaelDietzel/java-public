
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class RockPaperScissors3 {
    public static void main(String [] args){
        // Scanner and Random
        Scanner newInput = new Scanner(System.in);
        Random  randomizer = new Random();
        
        // array
        String [] TieWinLossArray;
        TieWinLossArray = new String[11];
        
        // Variabiles
        int userInput;
        int AiSelection;
        int outcome;
        int roundChoice = 0;
        
        String userChoice;
        String AiChoice;
        
        while (roundChoice == 0){
        System.out.println(" How many rounds would you like to play 1-10");
            roundChoice = newInput.nextInt();
            // good idea bad idea?
            roundChoice = errorMessage(roundChoice);
           
        }
        
        for (int roundNumber = 1; roundChoice >= roundNumber; roundNumber++ ){
            //input
            System.out.println("This is round" + roundNumber + " Of " + roundChoice);
            System.out.println("Please select : 1) rock 2) paper 3) Scissors ");
                userInput = newInput.nextInt();
                AiSelection = randomizer.nextInt(2) +1;
            
       
            outcome = decider(userInput,AiSelection); 
        
        
            if (outcome == 0){              
                 userChoice = output(userInput);
                 AiChoice = output(AiSelection);
                 TieWinLossArray [roundNumber ] = "Tie";
                 System.out.println("Tie");
                 System.out.println("You chose " + userChoice +" and Ai chose " +AiChoice);
            } else if(outcome == 1) {
                userChoice = output(userInput);
                AiChoice = output(AiSelection);
                TieWinLossArray [roundNumber] = "Win";
                System.out.println("Win!");
                System.out.println("You chose" + userChoice +" and Ai chose " +AiChoice);              
            } else if (outcome ==-1) {
                userChoice = output(userInput);
                AiChoice = output(AiSelection);
                TieWinLossArray [roundNumber] = "loss";
                System.out.println("loss");
                System.out.println("You chose " + userChoice +" and Ai chose " +AiChoice);  
            } else {
                userChoice = output(userInput);
                AiChoice = output(AiSelection);
                TieWinLossArray [roundNumber] = "invalid";
                System.out.println("loss");
                System.out.println("You chose invalid and Ai chose " +AiChoice); 
            }
        }
        matchResultsPrint(TieWinLossArray,roundChoice);
    }
// 1 rock  2 paper 3 scisiors             
private static int decider(int user,int Ai ){
    int outcome = -10;      
        if (user == Ai){
            outcome = 0;
        }else if ( user == 1 && Ai == 3 ){
            outcome = 1;
        } else if ( user == 1 && Ai == 2){
            outcome = -1;
        } else if ( user == 2 && Ai == 1){
            outcome = 1;
        } else if ( user == 2 && Ai == 3){
            outcome = -1;
        } else if ( user == 3 && Ai == 1){
            outcome = -1;
        } else if ( user == 3 && Ai == 2){
            outcome = 1;
        }
        
    return outcome;
}
    // not needed and probably a better way of doing this but why not get more practice.
    private static String output(int selection){
        String selected = "initalized";
            if ( selection == 1 ){
                selected = "rock";
            }else if (selection == 2){
                selected = "paper";
            }else if (selection == 3){
                selected = "scissors";
            }
        return selected;
    }
    
    private static int errorMessage(int roundChoice){
        if (roundChoice == 0 || roundChoice >= 11){
               System.out.println(" Invalid choice pick a number one to ten");
               roundChoice = 0;
           }
            return roundChoice;
    }
    
    private static void matchResultsPrint(String [] TieWinLossArray, int roundChoice){
     
        System.out.println("Here is the outcomes :");
        for (int roundNumber = 1 ; roundChoice >= roundNumber; roundNumber++ ){
            System.out.print(" " + roundNumber + ")" + TieWinLossArray[roundNumber]);
        }
    }
}
