/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary.dao;

import com.mycompany.dvdlibrary.dto.DVD;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author michael
 */
public class DVDLibraryDaoFileimpl implements DVDLibraryDao {

    private Map<Integer, DVD> Dvds = new HashMap<>();

    public static final String DVDLIBRARY_FILE = "Library.txt";
    public static final String DELIMITER = ",";

    @Override
    public List<DVD> findAllDvds() throws DvdLibraryDaoException {
        return new ArrayList<DVD>(Dvds.values());
    }

    @Override
    public DVD addDVD(DVD newDVD) throws DvdLibraryDaoException {
        loadLibrary();
        List<DVD> dvdList = this.findAllDvds();
        int lastDvd = dvdList.size();
        System.out.println(lastDvd);
        if (lastDvd > 0) {
            for (DVD currentDvd : dvdList) {
                int findHighestId = currentDvd.getId();
                if (Integer.valueOf(findHighestId) > lastDvd) {
                    lastDvd = Integer.valueOf(findHighestId);
                }
            }
        }
        lastDvd = lastDvd + 1;
        System.out.println(lastDvd);
        newDVD.setId(lastDvd);
        Dvds.put(newDVD.getId(), newDVD);
        return newDVD;
    }

    @Override
    public void removeDVD(int idSelection) throws DvdLibraryDaoException {
        this.loadAllChanges();
        Dvds.remove(idSelection);
        writeDVDlibrary();
    }

    @Override
    public List findDvdsByTitle(String title) throws DvdLibraryDaoException {
        List<DVD> sameTitle = new ArrayList();
        List<DVD> dvdList = this.findAllDvds();
        for (DVD currentDVD : dvdList) {
            String titleChecker = currentDVD.getTitle();
            if (titleChecker.equals(title)) {
                sameTitle.add(currentDVD);
            }
        }

        return sameTitle;
    }//return list 

    @Override
    public void saveAllChanges() throws DvdLibraryDaoException {
        writeDVDlibrary();
    }

    @Override
    public void loadAllChanges() throws DvdLibraryDaoException {
        loadLibrary();

    }

    private void loadLibrary() throws DvdLibraryDaoException {
        Scanner dvdScanner;
        try {
            dvdScanner = new Scanner(new BufferedReader(new FileReader(DVDLIBRARY_FILE)));
        } catch (FileNotFoundException e) {
            throw new DvdLibraryDaoException("Could not load roster data into memory.", e);
        }

        String currentDvdLine;
        String[] currentTokens;

        while (dvdScanner.hasNextLine()) {
            currentDvdLine = dvdScanner.nextLine();
            currentTokens = currentDvdLine.split(DELIMITER);
            DVD currentDvd = new DVD();
            currentDvd.setId(Integer.parseInt(currentTokens[0]));
            currentDvd.setTitle(currentTokens[1]);
            currentDvd.setReleaseDate(currentTokens[2]);
            currentDvd.setMpaaRating(currentTokens[3]);
            currentDvd.setDirectorName(currentTokens[4]);
            currentDvd.setStudio(currentTokens[5]);
            currentDvd.setNote(currentTokens[6]);
            Dvds.put(currentDvd.getId(), currentDvd);
        }
        dvdScanner.close();
    }

    private void writeDVDlibrary() throws DvdLibraryDaoException {
        PrintWriter out;
        try {
            out = new PrintWriter(new FileWriter(DVDLIBRARY_FILE));
        } catch (IOException e) {
            throw new DvdLibraryDaoException("Could not save student data", e);
        }

        List<DVD> DvdList = this.findAllDvds();

        for (DVD currentDvd : DvdList) {
            out.println(currentDvd.getId() + DELIMITER
                    + currentDvd.getTitle() + DELIMITER
                    + currentDvd.getReleaseDate() + DELIMITER
                    + currentDvd.getMpaaRating() + DELIMITER
                    + currentDvd.getDirectorName() + DELIMITER
                    + currentDvd.getStudio() + DELIMITER
                    + currentDvd.getNote() + DELIMITER
            );
            out.flush();
        }
        out.close();
    }

    @Override
    public DVD findDvdById(int id) throws DvdLibraryDaoException {
        DVD foundTheDvd = new DVD();
        List<DVD> dvdList = this.findAllDvds();
        for (DVD currentDVD : dvdList) {
            if (currentDVD.getId() == id) {
                foundTheDvd = currentDVD;
            }
        }
        return foundTheDvd;
    }

    @Override
    public void updateDVD(int id, DVD updateDVD) throws DvdLibraryDaoException {
        Dvds.put(id, updateDVD);
    }

}
