/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary.dao;

import com.mycompany.dvdlibrary.dto.DVD;
import java.util.List;

/**
 *
 * @author michael
 */
public interface DVDLibraryDao {

    // Create
    DVD addDVD(DVD newDVD) // return the DVD. 
            throws DvdLibraryDaoException;

    // Read
    //*** this is return type list<DVD>
    List<DVD> findDvdsByTitle(String title)
            throws DvdLibraryDaoException;

    DVD findDvdById(int id)
            throws DvdLibraryDaoException;

    List<DVD> findAllDvds()
            throws DvdLibraryDaoException;

    //update
    void updateDVD(int id, DVD updateDVD)
            throws DvdLibraryDaoException;

    // Delete
    void removeDVD(int idSelection)
            throws DvdLibraryDaoException;

    void saveAllChanges()
            throws DvdLibraryDaoException;

    void loadAllChanges()
            throws DvdLibraryDaoException;
}
