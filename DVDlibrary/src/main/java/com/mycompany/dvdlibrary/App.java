/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary;

import com.mycompany.dvdlibrary.controller.DVDLibraryController;
import com.mycompany.dvdlibrary.dao.DVDLibraryDao;
import com.mycompany.dvdlibrary.dao.DVDLibraryDaoFileimpl;
import com.mycompany.dvdlibrary.dao.DvdLibraryDaoException;
import com.mycompany.dvdlibrary.ui.DVDLibraryUserIO;
import com.mycompany.dvdlibrary.ui.DVDLibraryUserIOConsoleImpl;
import com.mycompany.dvdlibrary.ui.DVDlibraryView;

/**
 *
 * @author michael
 */
public class App {

    public static void main(String[] args) throws DvdLibraryDaoException {
        DVDLibraryUserIO myIo = new DVDLibraryUserIOConsoleImpl();
        DVDlibraryView myView = new DVDlibraryView(myIo);
        DVDLibraryDao myDao = new DVDLibraryDaoFileimpl();
        DVDLibraryController controller = new DVDLibraryController(myView, myDao);
        controller.run();
    }
}
