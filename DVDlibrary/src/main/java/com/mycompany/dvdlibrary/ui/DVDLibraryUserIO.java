/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary.ui;

/**
 *
 * @author michael
 */
public interface DVDLibraryUserIO {

    void print(String msg);

    double readDouble(String msg, double minRange, double maxRange);

    float readFloat(String msg, float minRange, float maxRange);

    long readLong(String msg, long minRange, long maxRange);

    int readInt(String msg, int minRange, int maxRange);

    int readInt(String msg);

    double readDouble(String msg);

    float readFloat(String msg);

    long readLong(String msg);

    String readString(String prompt);

}
