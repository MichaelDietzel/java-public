/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary.ui;

import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author michael
 */
public class DVDLibraryUserIOConsoleImpl implements DVDLibraryUserIO {

    Scanner dvdScanner = new Scanner(System.in);
    PrintWriter dvdPrinter = new PrintWriter(System.out);

    public DVDLibraryUserIOConsoleImpl() {

    }

    @Override
    public void print(String msg) {
        System.out.println(msg);
    }

    @Override
    public double readDouble(String msg, double minRange, double maxRange) {
        boolean badInput = true;;
        double result = 0;
        do {
            try {
                System.out.println(msg);
                result = Double.parseDouble(dvdScanner.nextLine());
                badInput = false;
                if(result < minRange){
                    System.out.println(" Please enter a number no lower than " + minRange);
                } else {
                    System.out.println(" Please enter a number no higher than " + maxRange);
                }
            } catch (NumberFormatException e) {
                System.out.println(" Please enter valid input, double");;

            }
        } while (result < minRange || result > maxRange || badInput == true);
        return result;
    }

    @Override
    public float readFloat(String msg, float minRange, float maxRange) {
        boolean badInput;
        float result = 0;
        do {
            try {
                System.out.println(msg);
                result = Float.parseFloat(dvdScanner.nextLine());
                badInput = false;
                if(result < minRange){
                    System.out.println(" Please enter a number no lower than " + minRange);
                } else {
                    System.out.println(" Please enter a number no higher than " + maxRange);
                }
                
            } catch (NumberFormatException e) {
                badInput = true;
               System.out.println(" Please enter valid input, float");;

            }
        } while (result < minRange || result > maxRange || badInput == true);
        return result;
    }

    @Override
    public long readLong(String msg, long minRange, long maxRange) {
        boolean badInput;
        long result = 0;
        do {
            try {
                System.out.println(msg);
                result = Long.parseLong(dvdScanner.nextLine());
                badInput = false;
                if(result < minRange){
                    System.out.println(" Please enter a number no lower than " + minRange);
                } else {
                    System.out.println(" Please enter a number no higher than " + maxRange);
                }
            } catch (NumberFormatException e) {
                badInput = true;
                System.out.println(" Please enter valid input, long");

            }
        } while (result < minRange || result > maxRange || badInput == true);
        return result;
    }

    @Override
    public int readInt(String msg, int minRange, int maxRange) {
        boolean badInput;
        int result = 0;
        do {
            try {
                System.out.println(msg);
                result = Integer.parseInt(dvdScanner.nextLine());
                badInput = false;
                if(result < minRange){
                    System.out.println(" Please enter a number no lower than " + minRange);
                } else {
                    System.out.println(" Please enter a number no higher than " + maxRange);
                }
            } catch (NumberFormatException e) {
                badInput = true;
                System.out.println(" Please enter valid input, int");
                
            }
        } while (result < minRange || result > maxRange || badInput == true);
        return result;
    }

    @Override
    public int readInt(String msg) {
        boolean badinput;
        int result = 0;
        do {
            try {
                System.out.println(msg);
                result = result = Integer.parseInt(dvdScanner.nextLine());
                badinput = false;
                
            } catch (NumberFormatException e) {
                badinput = true;
                System.out.println(" Please enter valid input, Int");
            }
        } while (badinput == true);
        return result;
    }

    @Override
    public double readDouble(String msg) {
        boolean badinput;
        double result = 0;
        do {
            try {
                System.out.println(msg);
                result = Double.parseDouble(dvdScanner.nextLine());
                badinput = false;
            } catch (NumberFormatException e) {
                badinput = true;
                System.out.println(" Please enter valid input, Double");
            }
        } while (badinput = true);
        return result;
    }

    @Override
    public float readFloat(String msg) {
        boolean badinput = true;
        float result = 0;
        do {
            try {
                System.out.println(msg);
                result = Float.parseFloat(dvdScanner.nextLine());
                badinput = false;
            } catch (NumberFormatException e) {

                System.out.println(" Please enter valid input, float");
            }
        } while (badinput = true);
        return result;
    }

    @Override
    public long readLong(String msg) {
        boolean badinput;
        long result = 0;
        do {
            try {
                System.out.println(msg);
                result = Long.parseLong(dvdScanner.nextLine());;
                badinput = false;
            } catch (NumberFormatException e) {
                badinput = true;
                System.out.println(" Please enter valid input, Long");
            }
        } while (badinput = true);
        return result;
    }

    @Override
    public String readString(String prompt) {
        System.out.println(prompt);
        return dvdScanner.nextLine();

    }

}
