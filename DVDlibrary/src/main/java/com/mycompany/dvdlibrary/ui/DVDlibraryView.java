/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary.ui;

import com.mycompany.dvdlibrary.dao.DvdLibraryDaoException;
import com.mycompany.dvdlibrary.dto.DVD;
import java.util.List;

/**
 *
 * @author michael
 */
public class DVDlibraryView {

    private DVDLibraryUserIO io;

    public DVDlibraryView(DVDLibraryUserIO io) {
        this.io = io;
    }

    public int printMenuAndCaptureInt() {
        io.print("Main Menu");
        io.print("1. Add a DVD to the collection");
        io.print("2. Remove a DVD from the collection");
        io.print("3. Edit the information for an existing DVD in the collection");
        io.print("4  Display the information for a particular DVD");
        io.print("5. Search for a DVD by title.");
        io.print("6  Save the DVD library and exit");
        io.print("7  Exit without Saving");

        return io.readInt("please Select from the following list", 1, 8);

    }

    public void printAddDvdBanner() {
        io.print("==============You have chosen to Add a DVD to your lIbrary======");
    }

    public DVD getDVDInfo(DVD newDVD) {

        String title = io.readString("Please enter the title ");
        String releaseDate = io.readString("Please enter the Release date");
        String mpaaRating = io.readString("Please enter the MPAA rating");
        String directorName = io.readString("Please enter the Directors Name");
        String studio = io.readString("Please enter the Studio");
        String note = io.readString("Please enter any notes you may have");

        newDVD.setTitle(title);
        newDVD.setReleaseDate(releaseDate);
        newDVD.setMpaaRating(mpaaRating);
        newDVD.setDirectorName(directorName);
        newDVD.setStudio(studio);
        newDVD.setNote(note);

        return newDVD;
    }

    public void printJobComplete() {
        io.readString("Your action was completed, enter any value to continue");

    }

    public void printAddJobComplete(DVD newDVD) {
        io.print("Your action was completed, your DVD id is: " + newDVD.getId());

    }

    public void printRemoveDvdBanner() {
        io.print("===== You have selected to remove a DVD from your library ========");
    }

    public int getIdSelection() {
        return io.readInt("Please enter the ID for this ");
    }

    public void displayDisplayOneBanner() {
        io.print(" You have selected the option to view a  DVD in your library");
    }

    public int getIdChoice() {
        return io.readInt(" Please enter the Id you wish to view");
    }

    public void displayDvdList(List<DVD> dvdList) {
        for (DVD currentDVD : dvdList) {
            io.print(currentDVD.getId() + ": "
                    + currentDVD.getTitle() + ": "
                    + currentDVD.getReleaseDate() + ": "
                    + currentDVD.getMpaaRating() + ": "
                    + currentDVD.getDirectorName() + ": "
                    + currentDVD.getStudio() + ": "
                    + currentDVD.getNote());
        }
    }

    public void printViewDVDBanner() {
        io.print("You have selected the option to view a single DVD");
    }

    public String getTitleChoice() {
        return io.readString("Please enter the title you would like to view");
    }

    public void displayDVD(DVD dvd) {
        if (dvd.getTitle() != null) {
            io.print(dvd.getId() + ": "
                    + dvd.getTitle() + ": "
                    + dvd.getReleaseDate() + ": "
                    + dvd.getMpaaRating() + ": "
                    + dvd.getDirectorName() + ": "
                    + dvd.getStudio() + ": "
                    + dvd.getNote());
        } else {
            io.print("No such DVD in library.");
        }
    }

    public void printError() throws DvdLibraryDaoException {
        io.readString("The value entered was out of range, please hit enter to continue");
    }

    public void printEditDvdBanner() throws DvdLibraryDaoException {
        io.print(" ===== You have elected to edit a DVD ========================");
    }

    public void printDvdToEditFromLibrary() throws DvdLibraryDaoException {
        io.print("This will be the DVD you are editing :");

    }

    public int printEditMenuAndCaptureInt() throws DvdLibraryDaoException {
        io.print("Main Menu, you can not enter an ID");
        io.print("1) Edit Title");
        io.print("2) Edit Release date");
        io.print("3) Edit Mpaa rating");
        io.print("4) Edit Director Name");
        io.print("5) Edit Studio");
        io.print("6) Edit note");
        io.print("7) Done editing");
        return io.readInt("please enter selection ", 1, 7);
    }

    public void printMakeAnotherSelection() throws DvdLibraryDaoException {
        io.print("Would you like to edit another field?");
    }

    public DVD updateTitle(DVD currentDvd) {
        String title = io.readString("Please enter a new title ");
        currentDvd.setTitle(title);
        return currentDvd;
    }

    public DVD updateReleaseDate(DVD currentDvd) {
        String ReleaseDate = io.readString("Please enter a new Release Date ");
        currentDvd.setReleaseDate(ReleaseDate);
        return currentDvd;
    }

    public DVD updateMpaaRating(DVD currentDvd) {
        String MpaaRating = io.readString("Please enter a new MpaaRating ");
        currentDvd.setMpaaRating(MpaaRating);
        return currentDvd;
    }

    public DVD updateDirectorName(DVD currentDvd) {
        String directorName = io.readString("Please enter a new Director Name ");
        currentDvd.setDirectorName(directorName);
        return currentDvd;
    }

    public DVD updateDvdStudio(DVD currentDvd) {
        String studio = io.readString("Please enter a new Studio ");
        currentDvd.setStudio(studio);
        return currentDvd;
    }

    public DVD updateDvdNote(DVD currentDvd) {
        String DvdNote = io.readString("Please enter a new Dvd note ");
        currentDvd.setNote(DvdNote);
        return currentDvd;
    }

}
