/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary.controller;

import com.mycompany.dvdlibrary.dao.DVDLibraryDao;
import com.mycompany.dvdlibrary.dao.DvdLibraryDaoException;
import com.mycompany.dvdlibrary.dto.DVD;
import com.mycompany.dvdlibrary.ui.DVDLibraryUserIO;
import com.mycompany.dvdlibrary.ui.DVDLibraryUserIOConsoleImpl;
import com.mycompany.dvdlibrary.ui.DVDlibraryView;
import java.util.List;

/**
 *
 * @author michael
 */
public class DVDLibraryController {

    private DVDlibraryView view;
    private DVDLibraryDao dao;

    public DVDLibraryController(DVDlibraryView view, DVDLibraryDao dao) {
        this.dao = dao;
        this.view = view;

    }

    public void run() throws DvdLibraryDaoException {
        boolean mainMenuIsActive = true;
        int mainMenuSelection = 0;
        loadInDvdLibrary();
        while (mainMenuIsActive == true) {
            mainMenuSelection = getMenuAndGetMenuSelection();

            switch (mainMenuSelection) {

                case 1:// add a DVD
                    addDvdToLibrary();
                    break;
                case 2:// remove a DVD
                    removeADvdFromLibrary();
                    break;
                case 3:// Edit a DVD
                    editADvdMenu();
                    break;
                case 4:// List the DVD Collection
                    listSingleDVD();
                    break;
                case 5:// Search for a DVD by title
                    viewDVDbyTitle();
                    break;
                case 6:// Save changes 
                    saveAllChanges();
                    mainMenuIsActive = false;
                    break;
                case 7: // don't save changes
                    mainMenuIsActive = false;
                    break;
                default: // print error
                    printError();
                    break;

            }

        }

    }

    private int getMenuAndGetMenuSelection() throws DvdLibraryDaoException {
        return view.printMenuAndCaptureInt();
    }

    private void addDvdToLibrary() throws DvdLibraryDaoException {
        DVD newDvd = new DVD();
        view.printAddDvdBanner();
        view.getDVDInfo(newDvd);
        newDvd = dao.addDVD(newDvd);
        view.printAddJobComplete(newDvd);

    }

    private void removeADvdFromLibrary() throws DvdLibraryDaoException {
        view.printRemoveDvdBanner();
        int idSelection = view.getIdSelection();
        dao.removeDVD(idSelection);
        view.printJobComplete();
    }

    private int getEditSelection() throws DvdLibraryDaoException {
        view.printEditDvdBanner();
        int idSelected = view.getIdChoice();
        dao.findDvdById(idSelected);
        return idSelected;

    }

    private void listSingleDVD() throws DvdLibraryDaoException {
        view.displayDisplayOneBanner();
        DVD printDvd = new DVD();
        int idSelected = view.getIdChoice();
        printDvd = dao.findDvdById(idSelected);
        view.displayDVD(printDvd);
        view.printJobComplete();
    }

    private void viewDVDbyTitle() throws DvdLibraryDaoException {
        view.printViewDVDBanner();
        String title = view.getTitleChoice();
        List<DVD> sameTitles = dao.findDvdsByTitle(title);
        view.displayDvdList(sameTitles);
        view.printJobComplete();
    }

    private void saveAllChanges() throws DvdLibraryDaoException {
        dao.saveAllChanges();
    }

    private void loadInDvdLibrary() throws DvdLibraryDaoException {
        dao.loadAllChanges();
    }

    private void printError() throws DvdLibraryDaoException {
        view.printError();
    }

    private void editADvdMenu() throws DvdLibraryDaoException {
        boolean editMenuIsActive = true;
        int dvdToEdit = getEditSelection();
        while (editMenuIsActive == true) {
            int fieldToEdit = getEditMenuSelection();
            switch (fieldToEdit) {
                case 1: //title *user may not alter id
                    edit1DvdTitle(dvdToEdit);
                    makeAnotherEditSelection();
                    break;
                case 2: //release date
                    edit1DvdReleaseDate(dvdToEdit);
                    makeAnotherEditSelection();
                    break;
                case 3:// mpaa Rating;
                    edit1DvdMpaaRating(dvdToEdit);
                    makeAnotherEditSelection();
                    break;
                case 4: //directorName;
                    edit1DvdDirectorName(dvdToEdit);
                    makeAnotherEditSelection();
                    break;
                case 5: //Studio;
                    edit1DvdStudio(dvdToEdit);
                    makeAnotherEditSelection();
                    break;
                case 6: // note;
                    edit1DvdNote(dvdToEdit);
                    makeAnotherEditSelection();
                    break;
                case 7:
                    editMenuIsActive = false;
                    break;

            }

        }
    }

    private int getEditMenuSelection() throws DvdLibraryDaoException {
        return view.printEditMenuAndCaptureInt();
    }

    private void edit1DvdTitle(int dvdToEdit) throws DvdLibraryDaoException {
        DVD foundDVD = dao.findDvdById(dvdToEdit);
        view.displayDVD(foundDVD);
        view.updateTitle(foundDVD);
        dao.updateDVD(dvdToEdit, foundDVD);

    }

    private void makeAnotherEditSelection() throws DvdLibraryDaoException {
        view.printMakeAnotherSelection();

    }

    private void edit1DvdReleaseDate(int dvdToEdit) throws DvdLibraryDaoException {
        view.displayDisplayOneBanner();
        DVD foundDVD = new DVD();
        foundDVD = dao.findDvdById(dvdToEdit);
        view.displayDVD(foundDVD);
        view.updateReleaseDate(foundDVD);
        view.printJobComplete();
        dao.updateDVD(dvdToEdit, foundDVD);
    }

    private void edit1DvdMpaaRating(int dvdToEdit) throws DvdLibraryDaoException {
        view.displayDisplayOneBanner();
        DVD foundDVD = new DVD();
        foundDVD = dao.findDvdById(dvdToEdit);
        view.displayDVD(foundDVD);
        view.updateMpaaRating(foundDVD);
        view.printJobComplete();
        dao.updateDVD(dvdToEdit, foundDVD);
    }

    private void edit1DvdDirectorName(int dvdToEdit) throws DvdLibraryDaoException {
        view.displayDisplayOneBanner();
        DVD foundDVD = new DVD();
        foundDVD = dao.findDvdById(dvdToEdit);
        view.displayDVD(foundDVD);
        view.updateDirectorName(foundDVD);
        view.printJobComplete();
        dao.updateDVD(dvdToEdit, foundDVD);
    }

    private void edit1DvdStudio(int dvdToEdit) throws DvdLibraryDaoException {;
        view.displayDisplayOneBanner();
        DVD foundDVD = new DVD();
        foundDVD = dao.findDvdById(dvdToEdit);
        view.displayDVD(foundDVD);
        view.updateDvdStudio(foundDVD);
        view.printJobComplete();
        dao.updateDVD(dvdToEdit, foundDVD);
    }

    private void edit1DvdNote(int dvdToEdit) throws DvdLibraryDaoException {
        view.displayDisplayOneBanner();
        DVD foundDVD = new DVD();
        foundDVD = dao.findDvdById(dvdToEdit);
        view.displayDVD(foundDVD);
        view.updateDvdNote(foundDVD);
        view.printJobComplete();
        dao.updateDVD(dvdToEdit, foundDVD);;
    }

}
