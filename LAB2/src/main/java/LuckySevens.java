

import static java.lang.Integer.parseInt;
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class LuckySevens {
    public static void main(String [] args){
        //scanner
        Scanner inputReader =  new Scanner(System.in);
        
        //random gen
        Random rGen = new Random();
        
        //Variables 
        
        int diceOne = 0;
        int diceTwo = 0;
        int diceTotal = 0;
        int money = 0;
        int rollCount = 0;
        int maxMoney = 0; 
        int maxRollCount = 0;
        String moneyString ;
        
        System.out.println("How much money will you wager : ");
        moneyString = inputReader.nextLine();
        
        money = parseInt(moneyString);
        
        // while loop
        while (money > 0){ 
                //rolls 
                rollCount ++;
                diceOne = rGen.nextInt(6) + 1;
                diceTwo = rGen.nextInt(6) + 1;
                
                diceTotal = diceOne + diceTwo;
                // Money calculation
                if (diceTotal == 7) {
                    
                    money = money + 4;
                    
                    if ( money > maxMoney)
                    {   
                        maxMoney = money;
                        maxRollCount = rollCount;
                        
                    }   
                    
                } else {
                    money = money - 1;
                    
                }   
                
                
            }
        System.out.println(" Number of dice rolls " + rollCount);
        System.out.println(" Highet number of money " + maxMoney);
        System.out.println(" You should have stoped on :" + maxRollCount);
    }
       
}
