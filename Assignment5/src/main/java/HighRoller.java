
import static java.lang.Integer.parseInt;
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class HighRoller {
        public static void main(String [] args){
       // random and scanner
       Random randomizer = new Random();
       Scanner newInput = new Scanner(System.in);
       // variable and strings
       int sides;
       int decider;
       String input;
       
       System.out.println("How many sided die did you want to roll  today? :");
              input = newInput.nextLine();
              sides = parseInt(input);
       
       
       decider = randomizer.nextInt(sides) + 1;
       
       System.out.println("this is the roll from the " + sides + " dice :" + decider);
       
       if (decider == sides ){
           System.out.println(" You rolled a critical hit");
       }else{
           System.out.println(" You rolled a critical miss");
       }
       }
}
