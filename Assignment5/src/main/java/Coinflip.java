
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class Coinflip {
    public static void main (String [] args)
    {
        Random randomizer = new Random();
        
        boolean coin = randomizer.nextBoolean();
        
        if (coin == true){
            System.out.println(" You got heads");
        }
        
        if (coin == false){
            System.out.println(" You got tails");
        }
    }
}
