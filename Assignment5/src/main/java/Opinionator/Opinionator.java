/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Opinionator;

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class Opinionator {
    public static void main( String [] args){
        Random randomizer = new Random();
        System.out.print(" I can't decide what animal I like best");
        System.out.print("I know1 random can decide FOR ME");
        
        //  5 isn't inclusive in randomizer,
        int x = randomizer.nextInt(6);
        System.out.println(" we chose :" + x);
        // 0 is not inclusive in randomizer 
        if (x == 0){
            System.out.println("llamas are the best");
        }else if (x == 1){
            System.out.println("Dodos are the best");
        }else if (x == 2){
            System.out.println("Wooly Mamoths are the best");
        }else if (x == 3){
            System.out.println("Sharks are the best");
        }else if (x == 4){
            System.out.println("Cockatoos are the best");
        }else if (x == 5){
            System.out.println("Have you seen a dog they are the best");
        }
        System.out.println(" thanks Random you are the best");
        // I think the bug in this case is the non-use of case, this breaks teh DRY rule and aare forced to end on a enlese if.
}
}