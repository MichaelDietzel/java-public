/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.dao;

import com.mycompany.vendingmachine.dto.Item;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author michael
 */
public class VendingMachineDaoStubImpl implements VendingMachineDao {

    Item onlyItem = new Item();
    List<Item> inventoryList = new ArrayList();
    BigDecimal cost = new BigDecimal("1.00");
    BigDecimal Funds = new BigDecimal("10.00");
   
   public VendingMachineDaoStubImpl(){
        
        onlyItem.setItemIndexlocation("A100");
        onlyItem.setItemName("potato");
        onlyItem.setItemCost(cost);
        onlyItem.setNumbeOfItems(1);
        inventoryList.add(onlyItem);
       
   }
   
    @Override
    public void getInventory() throws VendingMachineDaoException {
     
    }

    @Override
    public Item retreiveItem(String userVendingSelection) throws VendingMachineDaoException {
        if (onlyItem.getItemName().equals("Potato")){
        return onlyItem;   
        }else{
        return null;
       }     
    }

    @Override
    public void writeInventory(List AllItems) throws VendingMachineDaoException {
       
    }

    @Override
    public void createInventory(Item currentItem) throws VendingMachineDaoException {
        
    }

    @Override
    public void deleteInventory(Item CurrentItem) throws VendingMachineDaoException {
      
    }

    @Override
    public List<Item> allItems() throws VendingMachineDaoException {    
        return inventoryList;
    }

    @Override
    public void updateInventory(Item updateItem) throws VendingMachineDaoException {

    }
    
}
