/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.ui;

import java.math.BigDecimal;

/**
 *
 * @author michael
 */
public interface VendingMachineUserIO {
    void print(String msg);

    double readDouble(String msg, double minRange, double maxRange);

    float readFloat(String msg, float minRange, float maxRange);

    long readLong(String msg, long minRange, long maxRange);

    int readInt(String msg, int minRange, int maxRange);
    
    BigDecimal readBigDecimal(String prompt, BigDecimal minRange, BigDecimal maxrange);

    int readInt(String msg);

    double readDouble(String msg);

    float readFloat(String msg);

    long readLong(String msg);

    String readString(String prompt);
    
    BigDecimal readBigDecimal (String prompt);
    
    
}
