/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.service;


import com.mycompany.vendingmachine.dto.Coins;
import com.mycompany.vendingmachine.dto.Item;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author michael
 */
public interface VendingMachineServiceLayer {
     
     
    BigDecimal addmoney(BigDecimal funds)
        throws VendingMachineServiceInsufficentFundsException;
 
    public List<Item> getInventory()
        throws VendingMachineServiceException;
    public void subtractFunds( Item selectedItem)
        throws VendingMachineServiceInsufficentFundsException;    

    public Item retreiveItem(String userVendingSelection)
        throws VendingMachineServiceException;
    public void updateInventory(Item selectedItem)
        throws VendingMachineServiceException;    

    public Coins changeDispense()
        throws VendingMachineServiceInsufficentFundsException;

   
}
