/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.service;

import com.mycompany.vendingmachine.dao.VendingMachineAuditDao;
import com.mycompany.vendingmachine.dao.VendingMachineDao;
import com.mycompany.vendingmachine.dao.VendingMachineDaoException;
import com.mycompany.vendingmachine.dto.Coins;

import com.mycompany.vendingmachine.dto.Item;
import java.math.BigDecimal;
import static java.math.BigDecimal.ZERO;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 *
 * @author michael
 */
public class VendingMachineServiceLayerImpl implements VendingMachineServiceLayer {

    private VendingMachineAuditDao audit;
    private VendingMachineDao dao;
    private BigDecimal totalFunds = new BigDecimal("0.00");
     
    

    public VendingMachineServiceLayerImpl(VendingMachineDao dao, VendingMachineAuditDao audit) {
        this.dao = dao;
        this.audit = audit;
    }

    @Override
    public BigDecimal addmoney(BigDecimal funds) throws VendingMachineServiceInsufficentFundsException {
        totalFunds.setScale(2, RoundingMode.HALF_UP);
        totalFunds = totalFunds.add(funds);
        return totalFunds;   
    }

    @Override
    public List<Item> getInventory() throws VendingMachineServiceException {

        List<Item> allItems = null;
        List<Item> PositiveInventory = new ArrayList<Item>();
        try {
           dao.getInventory();
           allItems = dao.allItems();
        } catch (VendingMachineDaoException ex) {
            Logger.getLogger(VendingMachineServiceLayerImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Item currentItems : allItems) {
            if (currentItems.getItemIndexlocation() == null
                    || currentItems.getItemName() == null
                    || currentItems.getItemCost() == null
                    || currentItems.getNumbeOfItems() <= -1) {
                throw new VendingMachineServiceException("Null value found in inventory item");
            }
        }
        //instructor discussion  for loop in a lambda. 
        for (Item positiveItem : allItems) {
            if (positiveItem.getNumbeOfItems() > 0){
                PositiveInventory.add(positiveItem);
            }
        }
        return PositiveInventory;
    }

    @Override
    public void subtractFunds(Item selectedItem) throws VendingMachineServiceInsufficentFundsException {
        BigDecimal noFunds = new BigDecimal("0.00");       
            totalFunds = totalFunds.subtract(selectedItem.getItemCost());
            int fundsCheck = totalFunds.compareTo(noFunds);
            if (fundsCheck < 0) {
                totalFunds = totalFunds.add(selectedItem.getItemCost());
                throw new VendingMachineServiceInsufficentFundsException("insufficent funds");
            }
        }

    

    @Override
    public Item retreiveItem(String userVendingSelection) throws VendingMachineServiceException {

        Item foundItem = null;
        try {
            foundItem = dao.retreiveItem(userVendingSelection);
        } catch (VendingMachineDaoException ex) {
            Logger.getLogger(VendingMachineServiceLayerImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

            if (foundItem.getItemIndexlocation() == null
                    || foundItem.getItemName() == null
                    || foundItem.getItemCost() == null
                    || foundItem.getNumbeOfItems() <= 0) {
                throw new VendingMachineServiceException("Null value found in inventory item");
                       
            }
        return foundItem;
    }

    @Override
    public void updateInventory(Item selectedItem) throws VendingMachineServiceException {

        
            if (selectedItem.getNumbeOfItems() <= 0) {
                throw new VendingMachineServiceException(" no inventory left");
            }
            int quanity = selectedItem.getNumbeOfItems();
            quanity = quanity - 1;
            selectedItem.setNumbeOfItems(quanity);
            try {
                dao.updateInventory(selectedItem);
            } catch (VendingMachineDaoException ex) {
                Logger.getLogger(VendingMachineServiceLayerImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            //try {
                //audit.writeAuditEntry(selectedItem.getItemIndexlocation());
           // } catch (VendingMachineDaoException ex) {
             //   Logger.getLogger(VendingMachineServiceLayerImpl.class.getName()).log(Level.SEVERE, null, ex);
           // }
    }

    @Override
    public Coins changeDispense() throws VendingMachineServiceInsufficentFundsException {
        int coins = 0;
        Coins change = new Coins();
        
        List<BigDecimal> CoinList = new ArrayList<BigDecimal>();
        BigDecimal converter = new BigDecimal("100");

        converter = converter.multiply(totalFunds);
        converter.setScale(0, RoundingMode.HALF_UP);
        coins = converter.intValueExact();
        change.setNumberOfQuarters(coins);
        coins = coins - (change.getNumberOfQuarters()* 25);
        
        change.setNumberOfDimes(coins);
        coins = coins - (change.getNumberOfQuarters()* 10);
        
        change.setNumberOfNickles(coins);
        coins = coins - (change.getNumberOfNickles() * 5);
        
        change.setNumberOfNickles(coins);
        coins = coins - (change.getNumberOfPennies() * 1);
        
        // 0's out Total funds for future purchases/add. 
        totalFunds = totalFunds.multiply(ZERO);
        return change;
    }


}
