/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.service;

import com.mycompany.vendingmachine.dao.VendingMachineAuditDao;
import com.mycompany.vendingmachine.dao.VendingMachineDao;
import com.mycompany.vendingmachine.dto.Coins;
import com.mycompany.vendingmachine.dto.Item;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author michael
 */
public class VendingMachineServiceLayerStubImpl implements VendingMachineServiceLayer {
    private VendingMachineAuditDao audit;
    private VendingMachineDao dao;
    
    
  
    
 private  Coins onlyCoin = new Coins();
 private   Item onlyItem = new Item();
 private   List<Item> inventoryList = new ArrayList();
 private   BigDecimal cost = new BigDecimal("1.00");
 private   BigDecimal funds = new BigDecimal("10.00");
 private   BigDecimal potato = new BigDecimal("0.00");
    public VendingMachineServiceLayerStubImpl() {

        onlyItem.setItemIndexlocation("A100");
        onlyItem.setItemName("potato");
        onlyItem.setItemCost(cost);
        onlyItem.setNumbeOfItems(1);
        inventoryList.add(onlyItem);
        
        this.dao = dao;
        this.audit = audit;
        
        onlyCoin.setNumberOfQuarters(40000);
        onlyCoin.setNumberOfDimes(10000);
        onlyCoin.setNumberOfNickles(10000);
        onlyCoin.setNumberOfPennies(10000);
        
        
    }


    @Override
    public BigDecimal addmoney(BigDecimal funds) throws VendingMachineServiceInsufficentFundsException {
        
        potato = potato.add(funds);
        
        return potato;
    }

    @Override
    public List<Item> getInventory() throws VendingMachineServiceException {
        List<Item> myList = new ArrayList<Item>();
        myList = inventoryList;
        return myList;
    }

    @Override
    public void subtractFunds(Item selectedItem) throws VendingMachineServiceInsufficentFundsException {

    }

    @Override
    public Item retreiveItem(String userVendingSelection) throws VendingMachineServiceException {
        if (userVendingSelection.equals(onlyItem.getItemIndexlocation())) {
            return onlyItem;
        }else{
        return null;

        }
    }

    @Override
    public void updateInventory(Item selectedItem) throws VendingMachineServiceException {

    }

    @Override
    public Coins changeDispense() throws VendingMachineServiceInsufficentFundsException {
       
        return onlyCoin;
    }

}
