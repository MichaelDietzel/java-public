/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.ui;

import com.mycompany.vendingmachine.dto.Coins;
import com.mycompany.vendingmachine.dto.Item;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author michael
 */
public class VendingMachineView {
        private VendingMachineUserIO io;
   
    public VendingMachineView(VendingMachineUserIO io) {
        this.io = io;
    }
    public int menuSelection() {
        io.print("******in order to continue please add money ****");
        io.print("1. Add Money ");
        io.print("2. Make Purchase ");
        io.print("3. Get Change ");
        io.print("4  Exit the Program");
  
        return io.readInt("please Select from the following list", 1, 4);
        
    }


    public void displayMoney(BigDecimal totalFunds) {
       io.print(" You have " + totalFunds + "To make your purchase");
    }


    public String getVendingSelection(List<Item> inventory) {
         for (Item currentItem : inventory) {
             io.print(currentItem.getItemIndexlocation() + " Location "
                    + currentItem.getItemName()          + " Name "
                    + currentItem.getItemCost()          + " Cost ");
        }
       return io.readString(" Please make a selection :");
    }

    public void dispensed(Item selectedItem) {
             io.print( selectedItem.getItemName() + " Name "
                    + selectedItem.getItemCost()+ "Cost ");
             io.print(" Has been dispenesed");
    }

    public void dispenseMoney(Coins coin) {
        io.print(coin.getNumberOfQuarters() + " Number of quarters Dispensed ");
        io.print(coin.getNumberOfDimes() + " Number of Dimes Dispensed ");
        io.print(coin.getNumberOfNickles() + " Number of Nickles Dispensed ");
        io.print(coin.getNumberOfPennies() + " Number of Pennies Dispensed ");
      }

    public void intialInventorydisplay(List<Item> inventory) {
       for (Item currentItem : inventory) {
            io.print(currentItem.getItemIndexlocation() + "Location "
                    + currentItem.getItemName() + " Name "
                    + currentItem.getItemCost()+ "Cost ");
        }
    }

    public void inventorySoldOut(String userVendingSelection) {
        io.print("*************************************************************************");
        io.print("*                  Item is sold out    " + userVendingSelection + "                                 *");
        io.print("*************************************************************************");
        io.print("Please make another selection :");
    }

    public BigDecimal addMoneyToMachine() {
        
       return io.readBigDecimal("How much Money would you like to add :");
    }

    public void anErrorOccured() {
        io.print("Null Value in vending machine items!");    }

    public void insufficentfunds(String userVendingSelection) {
        io.print("*************************************************************************");
        io.print("*                  Insufficent funds        " + userVendingSelection + "                                 *");
        io.print("*************************************************************************");
        io.print("Please make another selection :");
    }
    }
    


