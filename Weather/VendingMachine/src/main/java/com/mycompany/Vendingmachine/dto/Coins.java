/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author michael
 */
public class Coins {
    
    private int numberOfQuarters;
    private int numberOfDimes;
    private int numberOfNickles;
    private int numberOfPennies;

    public int getNumberOfQuarters() {
        return numberOfQuarters;
    }

    public void setNumberOfQuarters(int pennies) {
        int quarters = 25;
        this.numberOfQuarters = pennies/quarters;
    }

    public int getNumberOfDimes() {
        return numberOfDimes;
    }

    public void setNumberOfDimes(int pennies) {
        int dimes = 10;
        this.numberOfDimes = pennies/dimes;
    }

    public int getNumberOfNickles() {
        return numberOfNickles;
    }

    public void setNumberOfNickles(int pennies) {
        int nickles = 5;
        this.numberOfNickles = pennies/nickles;
    }

    public int getNumberOfPennies() {
        return numberOfPennies;
    }

    public void setNumberOfPennies(int numberOfPennies) {
        this.numberOfPennies = numberOfPennies;
    }
}
