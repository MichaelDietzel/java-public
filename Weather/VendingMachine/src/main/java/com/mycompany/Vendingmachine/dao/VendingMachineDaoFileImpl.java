/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.dao;

import com.mycompany.vendingmachine.dto.Item;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 *
 * @author michael
 */
public class VendingMachineDaoFileImpl implements VendingMachineDao {

    private Map<String, Item> inventory = new HashMap<>();

    public static final String INVENTORY_FILE = "Library.txt";
    public static final String DELIMITER = ",";

    @Override
    public void getInventory() throws VendingMachineDaoException {
        Scanner inventoryScanner;
        try {
            inventoryScanner = new Scanner(new BufferedReader(new FileReader(INVENTORY_FILE)));
        } catch (FileNotFoundException e) {
            throw new VendingMachineDaoException("Could not load roster data into memory.", e);
        }

        String currentInventoryLine;
        String[] currentTokens;
        while (inventoryScanner.hasNextLine()) {
            currentInventoryLine = inventoryScanner.nextLine();
            currentTokens = currentInventoryLine.split(DELIMITER);
            Item currentItem = new Item();
            currentItem.setItemIndexlocation(currentTokens[0]);
            currentItem.setItemName(currentTokens[1]);
            BigDecimal cost = new BigDecimal(currentTokens[2]);
            currentItem.setItemCost(cost);
            currentItem.setNumbeOfItems(Integer.parseInt(currentTokens[3]));

            inventory.put(currentItem.getItemIndexlocation(), currentItem);

        }
        inventoryScanner.close();
    }

    @Override
    public Item retreiveItem(String userVendingSelection) throws VendingMachineDaoException {

        List<Item> foundItem = inventory.values()
                .stream()
                .filter(s -> s.getItemIndexlocation().equalsIgnoreCase(userVendingSelection))
                .collect(Collectors.toList());

        Item exactItem = foundItem.get(0);
        return exactItem;
    }

    @Override
    
    // find out who calls it and restrict to dao
    public void writeInventory(List AllItems) throws VendingMachineDaoException {

        PrintWriter out;
        try {
            out = new PrintWriter(new FileWriter(INVENTORY_FILE));
        } catch (IOException e) {
            throw new VendingMachineDaoException("Could not save student data", e);
        }

        List<Item> itemList = this.findAllitems();

        for (Item writeItem : itemList) {
            out.println(writeItem.getItemIndexlocation() + DELIMITER
                    + writeItem.getItemName() + DELIMITER
                    + writeItem.getItemCost() + DELIMITER
                    + writeItem.getNumbeOfItems() + DELIMITER);
            out.flush();
        }
        out.close();
    }

    @Override
    public void createInventory(Item currentItem) throws VendingMachineDaoException {
        inventory.put(currentItem.getItemIndexlocation(), currentItem);
    }

    @Override
    public void deleteInventory(Item currentItem) throws VendingMachineDaoException {
        inventory.remove(currentItem.getItemIndexlocation(), currentItem);
    }

    public List<Item> allItems() throws VendingMachineDaoException {
        List<Item> allItems = this.findAllitems();

        return allItems;
    }

    @Override
    public void updateInventory(Item updateItem) throws VendingMachineDaoException {
        inventory.put(updateItem.getItemIndexlocation(), updateItem);
        List<Item> allItems = allItems();
        writeInventory(allItems);
    }

    private List<Item> findAllitems() throws VendingMachineDaoException {
        return new ArrayList<>(inventory.values());
    }
}
