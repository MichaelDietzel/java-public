/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Vendingmachine;

import com.mycompany.vendingmachine.controller.VendingMachineController;
import com.mycompany.vendingmachine.dao.VendingMachineAuditDao;
import com.mycompany.vendingmachine.dao.VendingMachineAuditDaoImpl;
import com.mycompany.vendingmachine.dao.VendingMachineAuditDaoStubImpl;
import com.mycompany.vendingmachine.dao.VendingMachineDao;
import com.mycompany.vendingmachine.dao.VendingMachineDaoException;
import com.mycompany.vendingmachine.dao.VendingMachineDaoFileImpl;
import com.mycompany.vendingmachine.dao.VendingMachineDaoStubImpl;
import com.mycompany.vendingmachine.service.VendingMachineServiceException;
import com.mycompany.vendingmachine.service.VendingMachineServiceInsufficentFundsException;
import com.mycompany.vendingmachine.service.VendingMachineServiceLayer;
import com.mycompany.vendingmachine.service.VendingMachineServiceLayerImpl;
import com.mycompany.vendingmachine.service.VendingMachineServiceLayerStubImpl;
import com.mycompany.vendingmachine.ui.VendingMachineUserIO;
import com.mycompany.vendingmachine.ui.VendingMachineUserIOConsoleImpl;
import com.mycompany.vendingmachine.ui.VendingMachineView;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author michael
 */
public class App {
    public static void main(String[] args) throws VendingMachineServiceException, VendingMachineServiceInsufficentFundsException, VendingMachineDaoException {

        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        VendingMachineController controller = ctx.getBean("controller", VendingMachineController.class);
        controller.run();
    }
}
