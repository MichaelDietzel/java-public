/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.service;

import com.mycompany.vendingmachine.controller.VendingMachineController;
import com.mycompany.vendingmachine.dao.VendingMachineAuditDao;
import com.mycompany.vendingmachine.dao.VendingMachineAuditDaoImpl;
import com.mycompany.vendingmachine.dao.VendingMachineAuditDaoStubImpl;
import com.mycompany.vendingmachine.dao.VendingMachineDao;
import com.mycompany.vendingmachine.dao.VendingMachineDaoFileImpl;
import com.mycompany.vendingmachine.dao.VendingMachineDaoStubImpl;
import com.mycompany.vendingmachine.dto.Coins;
import com.mycompany.vendingmachine.dto.Item;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author michael
 */
public class VendingMachineServiceLayerTest {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        VendingMachineServiceLayerStubImpl myService =  ctx.getBean("myServiceStub", VendingMachineServiceLayerStubImpl.class);
    
//private VendingMachineServiceLayer myService;
    
   // public VendingMachineServiceLayerTest () {
        
           //myService = new VendingMachineServiceLayerStubImpl();
         //   ApplicationContext ctx = new ClassPathXmlApplicationContext("applicaitonContext");
        //  myService = ctx.getBean("serviceLayer",VendingMachineServiceLayer.class);
    //}
    
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    @Test
    public void testAddmoney() throws Exception {
     BigDecimal money = new BigDecimal("0.02");
     BigDecimal compare = myService.addmoney(money);
     assertEquals(compare ,money);
    }



    @Test
    public void testGetInventory() throws Exception {
        List <Item> myTestList = myService.getInventory();
        int test = myTestList.size();       
        assertEquals(1,test);
        
    }


    @Test
    public void testSubtractFunds() throws Exception {
        BigDecimal money = new BigDecimal("0.01");
        BigDecimal moneyMore = new BigDecimal("100.00"); 
        BigDecimal compare = new BigDecimal("100.01");
        
        Item testItem = new Item();
        
        testItem.setItemIndexlocation("a1");
        testItem.setItemName("food");
        testItem.setNumbeOfItems(1);
        testItem.setItemCost(money);
      
        
        myService.addmoney(moneyMore);
        myService.subtractFunds(testItem);
        moneyMore = myService.addmoney(money);
        
        assertEquals(compare,moneyMore);
    }

   
    @Test
    public void testRetreiveItem() throws Exception {
        String userInput ="A100";
        Item myTestItem =  myService.retreiveItem(userInput);
        assertEquals(userInput, myTestItem.getItemIndexlocation());
    }

    @Test
    public void testChangeDispense() throws Exception {
            int quarter = 1600;
            Coins coin = myService.changeDispense();
            assertEquals(quarter, coin.getNumberOfQuarters());
    }

  
}
