/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.dao;

import com.mycompany.vendingmachine.dto.Item;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author michael
 */
public class VendingMachineDaoTest {

    VendingMachineDao myDao = new VendingMachineDaoStubImpl();
    
    
    public VendingMachineDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws VendingMachineDaoException {
        List<Item> inventory = myDao.allItems();
        for (Item items : inventory) {
            myDao.deleteInventory(items);
        }

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of retreiveItem method, of class VendingMachineDao.
     */
    @Test
    public void testRetreiveItem() throws Exception {

        List<Item> FromDao = myDao.allItems();
        List<Item> AlsoFromDao = myDao.allItems();

        assertEquals(FromDao, AlsoFromDao);
    }

    /**
     * Test of WriteInventory method, of class VendingMachineDao.
     */
    @Test
    public void testWriteInventory() throws Exception {
        List<Item> myTestList = new ArrayList<Item>();
        List<Item> fromDao = myDao.allItems();
        for (Item eachItem : fromDao) {
            myDao.updateInventory(eachItem);
            myTestList.add(eachItem);
        }
        int myTestlistSize = myTestList.size();
        int fromDaoSize = fromDao.size();

        assertEquals(myTestlistSize, fromDaoSize);
    }

    @Test
    public void testAddItem() throws Exception {
        BigDecimal cost = new BigDecimal("0.50");

        Item item = new Item();
        item.setItemIndexlocation("a1");
        item.setItemName("Food");
        item.setItemCost(cost);
        item.setNumbeOfItems(10);
        myDao.createInventory(item);
        List <Item> allItems =myDao.allItems();
        myDao.writeInventory(allItems);
   

    }

    @Test
    public void removeItem() throws Exception {
        BigDecimal cost = new BigDecimal("0.50");
        Item item = new Item();
        item.setItemIndexlocation("a100");
        item.setItemName("Food");
        item.setItemCost(cost);
        item.setNumbeOfItems(10);
        int found = 0;
        myDao.updateInventory(item);
        myDao.deleteInventory(item);

        List<Item> compareItems = myDao.allItems();
        for (Item items : compareItems) {
            if (items.getItemIndexlocation().equals("a100")) {
                found = 1;
            }
        }

        assertEquals(0, found);
    }
}
