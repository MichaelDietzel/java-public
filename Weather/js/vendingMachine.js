$(document).ready(function () {
  loadItems();	
  
});
 
function addDollar(){
 var money = $('#money-display').val()
	money = Number(money) + Number(1.00);
 var addedMoney = money.toFixed(2);
	document.getElementById('money-display').value = addedMoney;
	document.getElementById('change-display').value = 0;
}
function addQuarter(){
 var money = $('#money-display').val()
	money = Number(money) + Number(0.25);
 var addedMoney = money.toFixed(2);
	document.getElementById('money-display').value = addedMoney;
	document.getElementById('change-display').value = 0;
}
function addDime(){
 var money = $('#money-display').val()
		 money = Number(money) + Number(0.10);
 var addedMoney = money.toFixed(2);
	document.getElementById('money-display').value = addedMoney;
	document.getElementById('change-display').value = 0;
}
function addNickle(){
 var money = $('#money-display').val()
	 money = Number(money) + Number(0.05);
 var addedMoney = money.toFixed(2);
	document.getElementById('money-display').value = money;
	document.getElementById('change-display').value = 0;
}

function loadItems(){

	var contentRows = $('#contentRows');
    $.ajax ({
        type: 'GET',
        url: 'http://localhost:8080/items',
        success: function (data, status) {
            $.each(data, function (index, items) {
                var Itemid = items.id ;
                var name = items.name;
                var price = Number(items.price);
				var fixedPrice = price.toFixed(2);
				var quantity = items.quantity;

                var row = '<div id='+ Itemid +' class="col-sm-3" onclick="display(this.id)" style="background-color:RoyalBlue;color:White;height:200px;margin: 10px 10px 10px 10px;border-style: solid; border-color: DarkOrange;">';
                    row +=  '<p align="left" >' + Itemid + '</p> <br>' ;
                    row +=  '<p align="center" >' + name + '</p>'; 
                    row +=  '<p align="center" id=Cost-'+ Itemid +'>' + Number(fixedPrice) + '</p>' + '<br><br><br>';
                    row +=  '<p align="right" >Quantity:' + quantity + '</p>' +'<br>';
                    row += '</div>';
                contentRows.append(row);
            });
        },

		error: function () {
			var Messaged =  data.message;
			document.getElementById('message-display').text = Messaged;
        }
    });
}
function purchase(){
	var toGetQuantity = $('#cost-display').val();
	toGetQuantity = toGetQuantity.toString();
	var quantity = $('#' + toGetQuantity + '').text();
	var money = $('#money-display').val();
	var findid = $('#cost-display').val();
	var itemCost =$('#Cost-'+toGetQuantity + '').val();
	var id = Number(findid)
	
	
	$('#errorMessages').empty;
	
	

    $.ajax ({
        type: 'GET',
        url: 'http://localhost:8080/money/'+ money + '/item/' + id,
        success: function (data, status) {
			var jsonString = data;
			var quarter =  data.quarters;
			var dime = 	   data.dimes;
			var nickels =  data.nickels;
			var pennies =  data.pennies;
			var change = 	(Number(quarter) * 0.25) + (Number(dime) * 0.10) + (Number(nickels)*0.05) + Number(pennies);
			var change = change.toFixed(2);
			alert("The amount of change dispursed :" + change);
			document.getElementById('change-display').value = "Quarters : " + quarter.toString() + " Dimes :" + dime.toString() + " Nickles :" + nickels.toString() + " ";
			document.getElementById('message-display').value = "thank you";
			document.getElementById('money-display').value = 0;
			
		},
		error: function (data,status) {
			var MessageObject = jsonString;
			var Messaged = MessageObject.message;
			document.getElementById('message-display').value = Messaged;		
		}
	});
	$('#contentRows').empty();
	loadItems();
	
}
function display(Itemid){
	document.getElementById('cost-display').value = Itemid;
	
}
