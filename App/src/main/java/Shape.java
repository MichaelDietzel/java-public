/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author michael
 */


public abstract class Shape {
    
    public abstract void color();
    
    public abstract void perimeter();
   
    public abstract void area();
}


