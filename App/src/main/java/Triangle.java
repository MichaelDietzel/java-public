/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author michael
 */
public class Triangle extends Shape {

    @Override
    public void color() {
        System.out.println("The color is blue");
    }

    @Override
    public void perimeter() {
        System.out.println(" Side 1 + Side 2 + Side 3");
          }

    @Override
    public void area() {
        System.out.println("Area of a triangle = .5* (Base * Height)");
    }
    
}
