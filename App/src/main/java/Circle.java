/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author michael
 */
public class Circle extends Shape{

    @Override
    public void color() {
       System.out.println("The color is blue");
    }

    @Override
    public void perimeter() {
        System.out.println(" The permiter of a circle is the (2*3.14) Radius");
    }

    @Override
    public void area() {
        System.out.println("The Area of a circle is the 3.14 * R^2)");
    }
    
}
