DROP DATABASE IF EXISTS superHeroesTest;

CREATE DATABASE superHeroesTest;

/* foreign key Super Hero */
CREATE TABLE superHeroesTest.enhanced
(

 enhancedName varChar(20) not null,
 powerDescription longtext not null,
 enahncedAlignment varChar(20) not null,
 organizationName varChar(20) not null, 
 enhancedId int not null AUTO_INCREMENT,
 primary key(enhancedId)
);
/* foreign key MemberEnhancedName*/
CREATE TABLE superHeroesTest.sightings
( 

 enhancedSighted varChar(20) not null,
 dateStamp varChar(20) not null,
 enhancedId int not null,
 locationId int not null,
 incidentId int not null auto_increment,
 primary key(incidentId)
);


/* foreign key MemberEnhancedName*/
CREATE TABLE superHeroesTest.organization
(

 organizationName varChar(20) not null,
 organizationDescription   varChar(20) not null ,
 organizationAddressNumber varChar(20)not null,
 organizationAddressStreet varChar(20)not null, 
 organizationAddressCity   varChar(20)not null,
 organizaitonAddressZipcode varChar(20) not null, 
 organizationContactEmail  varChar(20) not null, 
 organizationId int not null auto_increment, 
 primary key(organizationId)
);

/* foreign key enhancedSighted */
CREATE TABLE superHeroesTest.location
(
 addressNumber  varChar(40)not null ,
 addressStreet  varChar(40)not null,
 addressCity    varChar(40)not null, 
 addressZipcode varChar(40)not null,
 latitude  varChar(40)not null,
 longitude varChar(40)not null,
 locationsDescription longtext not null,
 locationId int not null AUTO_INCREMENT,
 primary key(locationId)
);
