/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.dao;

import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.Location;
import com.mycompany.superherosighting.model.Organization;
import com.mycompany.superherosighting.model.Sightings;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class SuperHeroDaoImplTest {

    SuperHeroOrganizaitonDao orgDao;
    SuperHeroEnhancedDao enhDao;
    SuperHeroSightingsDao sigDao;
    SuperHeroLocationDao locDao;

    public SuperHeroDaoImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        enhDao = ctx.getBean("SuperHeroEnhancedDao", SuperHeroEnhancedDao.class);

        ApplicationContext ctxTwo = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        locDao = ctxTwo.getBean("SuperHeroLocationDao", SuperHeroLocationDao.class);

        ApplicationContext ctxThree = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        sigDao = ctxThree.getBean("SuperHeroSightingsDao", SuperHeroSightingsDao.class);

        ApplicationContext ctxFour = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        orgDao = ctxFour.getBean("SuperHeroOrganizationDao", SuperHeroOrganizaitonDao.class);

        List<Enhanced> enhanced = enhDao.getAllEnhanced();
        for (Enhanced currentEnhacned : enhanced) {
            enhDao.deleteEnhanced(currentEnhacned.getIdentifier());
        }

        List<Location> location = locDao.getAllLocations();
        for (Location currentLocaiton : location) {
            locDao.deleteLocation(currentLocaiton.getLocationID());
        }

        List<Organization> organization = orgDao.getAllOrganization();
        for (Organization currentOrganization : organization) {
            orgDao.deleteOrganization(currentOrganization.getOrganizationId());
        }
        List<Sightings> sighting = sigDao.getAllSightings();
        for (Sightings currentsighting : sighting) {
            sigDao.deleteSightings(currentsighting.getIncidentId());
        }
    }

    @Test
    public void getAllSightingsByLocation() {
        Location location = new Location();
        location.setAddressCity("City");
        location.setAddressNumber("1111");
        location.setAddressStreet("Street");
        location.setAddressZipcode("1111");
        location.setLatitude("11");
        location.setLocationDescription("place");
        location.setLongitude("11");

        locDao.addLocation(location);
        locDao.addLocation(location);
        List<Location> allLocations = locDao.getAllLocations();
        int size = allLocations.size();
        assertEquals(2, 2);

    }

    @Test
    public void getAllSightingsByEnhanced() {
        Enhanced thisEnhanced = new Enhanced();
        Location thisLocation = new Location();
        thisLocation.setLocationID(1);
        thisEnhanced.setIdentifier(1);
        Sightings sighting = new Sightings();
        sighting.setDateStamp("12-06-2007");
        sighting.setEnhancedSighted("Mike");
        sighting.setEnahncedComponent(thisEnhanced);
        sighting.setLocationComponet(thisLocation);

     
        thisLocation.setLocationID(1);
        thisEnhanced.setIdentifier(2);
        Sightings sighting2 = new Sightings();
        sighting2.setDateStamp("12-06-2007");
        sighting2.setEnhancedSighted("Mike");
        sighting2.setEnahncedComponent(thisEnhanced);
        sighting2.setLocationComponet(thisLocation);

        sigDao.addSightings(sighting);
        sigDao.addSightings(sighting2);
        List<Sightings> sightingList = sigDao.getAllSightingsByEnhanced(1);
        int sightingListsize = sightingList.size();
        assertEquals(sightingListsize, 0);
    }

    @Test
    public void getAllEnhancedInOrganizaiton() {
        Enhanced enhanced = new Enhanced();
        enhanced.setAllignment("good");
        enhanced.setName("Mike");
        enhanced.setPowers("Fly");
        Organization organizationname = new Organization();
        organizationname.setOrganizationName("1");
        enhanced.setOrganizationComponent(organizationname);

        Enhanced enhancedTwo = new Enhanced();
        enhancedTwo.setAllignment("good");
        enhancedTwo.setName("Mike");
        organizationname.setOrganizationName("2");
        enhancedTwo.setOrganizationComponent(organizationname);
        enhancedTwo.setPowers("Fly");

        enhDao.addEnhanced(enhanced);
        enhDao.addEnhanced(enhancedTwo);
    
        List<Enhanced> enhancedList = enhDao.getAllEnhancedInOrganizaiton("1");
        int enhancedListint = enhancedList.size();
        assertEquals(enhancedListint, 0);
    }

    @Test
    public void getSightingEnhancedLocationbyDate() {
        Enhanced thisEnhanced = new Enhanced();
        Location thisLocation = new Location();
        thisLocation.setLocationID(1);
        thisEnhanced.setIdentifier(1);
        Sightings sighting = new Sightings();
        sighting.setDateStamp("12-06-2007");
        sighting.setEnhancedSighted("Mike");
        sighting.setEnahncedComponent(thisEnhanced);
        sighting.setLocationComponet(thisLocation);
        Sightings secondSighting = new Sightings();
        secondSighting.setDateStamp("12-06-2007");
        secondSighting.setEnhancedSighted("Mike");
        secondSighting.setEnahncedComponent(thisEnhanced);
        secondSighting.setLocationComponet(thisLocation);
        Sightings ThirdSighting = new Sightings();
        ThirdSighting.setDateStamp("12-07-2007");
        ThirdSighting.setEnhancedSighted("Mike");
        ThirdSighting.setEnahncedComponent(thisEnhanced);
        ThirdSighting.setLocationComponet(thisLocation);

        sigDao.addSightings(sighting);
        sigDao.addSightings(secondSighting);
        sigDao.addSightings(ThirdSighting);

        List<Sightings> sightingList = sigDao.getSightingEnhancedLocationbyDate("12-06-2007");
        int sightingListint = sightingList.size();
        assertEquals(sightingListint, 2);
    }

    @Test
    public void addEnhanced() {
        Enhanced enhanced = new Enhanced();
        enhanced.setAllignment("good");
        enhanced.setName("Mike");
        enhanced.setPowers("Fly");
        Organization organizationId = new Organization();
        organizationId.setOrganizationName("1");
        enhanced.setOrganizationComponent(organizationId);

        Enhanced enhancedTwo = new Enhanced();
        enhancedTwo.setAllignment("good");
        enhancedTwo.setName("Not mike");
        enhancedTwo.setOrganizationComponent(organizationId);
        enhancedTwo.setPowers("not-fly");

        Enhanced enhancedThree = new Enhanced();
        enhancedThree.setAllignment("good");
        enhancedThree.setName("Mike");
        enhancedThree.setOrganizationComponent(organizationId);
        enhancedThree.setPowers("Fly");

        enhDao.addEnhanced(enhanced);
        enhDao.addEnhanced(enhancedTwo);
        enhDao.addEnhanced(enhancedThree);
        List<Enhanced> enhancedList = enhDao.getAllEnhanced();
        int enhancedListint = enhancedList.size();
        assertEquals(enhancedListint, 3);
    }

    @Test
    public void deleteEnhanced() {
        Enhanced enhanced = new Enhanced();
        enhanced.setAllignment("good");
        enhanced.setName("Mike");
        enhanced.setPowers("Fly");
        Organization organizationId = new Organization();
        organizationId.setOrganizationName("1");
        enhanced.setOrganizationComponent(organizationId);

        Enhanced enhancedTwo = new Enhanced();
        enhancedTwo.setAllignment("good");
        enhancedTwo.setName("Mike");
        enhancedTwo.setPowers("Fly");
        enhancedTwo.setOrganizationComponent(organizationId);

        Enhanced enhancedThree = new Enhanced();
        enhancedThree.setAllignment("good");
        enhancedThree.setName("Mike");
        enhancedThree.setPowers("Fly");
        enhancedThree.setOrganizationComponent(organizationId);

        enhDao.addEnhanced(enhanced);
        enhDao.addEnhanced(enhancedTwo);
        enhDao.addEnhanced(enhancedThree);

        List<Enhanced> enhancedList = enhDao.getAllEnhanced();
        for (Enhanced currentEnhacned : enhancedList) {
            enhDao.deleteEnhanced(currentEnhacned.getIdentifier());
        }
        List<Enhanced> enhancedListTwo = enhDao.getAllEnhanced();
        assertEquals(enhancedListTwo.size(), 0);
    }

    @Test
    public void updateEnhanced() {
        Enhanced enhanced = new Enhanced();
        enhanced.setAllignment("good");
        enhanced.setName("Mike");
        enhanced.setPowers("Fly");
        Organization organizationId = new Organization();
        organizationId.setOrganizationName("1");
        enhanced.setOrganizationComponent(organizationId);
        enhDao.addEnhanced(enhanced);
        enhanced.setName("Potato");
        enhDao.updateEnhanced(enhanced);
        List<Enhanced> list = enhDao.getAllEnhanced();
        Enhanced enhancedtwo = list.get(0);
        assertEquals(enhancedtwo.getName(), "Potato");
    }

    @Test
    public void getEnhancedId() {
        Enhanced enhanced = new Enhanced();
        enhanced.setAllignment("good");
        enhanced.setName("Mike");
        enhanced.setPowers("Fly");
        Organization organizationId = new Organization();
        organizationId.setOrganizationName("1");
        enhanced.setOrganizationComponent(organizationId);
        enhDao.addEnhanced(enhanced);
        List<Enhanced> list = enhDao.getAllEnhanced();
        Enhanced enhancedTwo = list.get(0);
        Enhanced enhacnedThree = enhDao.getEnhancedId(enhancedTwo.getIdentifier());
        int one = enhancedTwo.getIdentifier();
        int two = enhacnedThree.getIdentifier();
        assertEquals(one, two);
    }

    @Test
    public void getAllEnhanced() {
        Enhanced enhanced = new Enhanced();
        enhanced.setAllignment("good");
        enhanced.setName("Mike");
        enhanced.setPowers("Fly");
        Organization organizationId = new Organization();
        organizationId.setOrganizationName("1");
        enhanced.setOrganizationComponent(organizationId);
        Enhanced enhancedTwo = new Enhanced();
        enhancedTwo.setAllignment("good");
        enhancedTwo.setName("Not mike");
        enhancedTwo.setPowers("not-fly");
        enhancedTwo.setOrganizationComponent(organizationId);
        Enhanced enhancedThree = new Enhanced();
        enhancedThree.setAllignment("good");
        enhancedThree.setName("Mike");
        enhancedThree.setPowers("Fly");
        enhancedThree.setOrganizationComponent(organizationId);
        enhDao.addEnhanced(enhanced);
        enhDao.addEnhanced(enhancedTwo);
        enhDao.addEnhanced(enhancedThree);
        List<Enhanced> enhancedList = enhDao.getAllEnhanced();
        int enhancedListint = enhancedList.size();
        assertEquals(enhancedListint, 3);
    }

    @Test
    public void addLocation() {
        Location newLocation = new Location();
        newLocation.setAddressCity("1111");
        newLocation.setAddressNumber("1111");
        newLocation.setAddressStreet("1111");
        newLocation.setAddressZipcode("1111");
        newLocation.setLocationDescription("1111");
        newLocation.setLatitude("1111");
        newLocation.setLongitude("1111");

        locDao.addLocation(newLocation);
        List<Location> allLocaitons = locDao.getAllLocations();
        int locationSizeint = allLocaitons.size();
        assertEquals(locationSizeint, 1);

    }

    @Test
    public void deleteLocation() {

        Location newLocation = new Location();
        newLocation.setAddressCity("1111");
        newLocation.setAddressNumber("1111");
        newLocation.setAddressStreet("1111");
        newLocation.setAddressZipcode("1111");
        newLocation.setLocationDescription("1111");
        newLocation.setLatitude("1111");
        newLocation.setLongitude("1111");

        locDao.addLocation(newLocation);
        List<Location> allLocaitons = locDao.getAllLocations();
        Location thisLocation = allLocaitons.get(0);
        int locationSizeint = thisLocation.getLocationID();
        locDao.deleteLocation(locationSizeint);
        List<Location> allLocaitonsTwo = locDao.getAllLocations();
        locationSizeint = allLocaitonsTwo.size();
        assertEquals(locationSizeint, 0);
    }

    @Test
    public void updateLocation() {
        Location newLocation = new Location();
        newLocation.setAddressCity("1111");
        newLocation.setAddressNumber("1111");
        newLocation.setAddressStreet("1111");
        newLocation.setAddressZipcode("1111");
        newLocation.setLocationDescription("1111");
        newLocation.setLatitude("1111");
        newLocation.setLongitude("1111");

        locDao.addLocation(newLocation);
        List<Location> allLocaitons = locDao.getAllLocations();
        Location thisLocation = allLocaitons.get(0);
        newLocation.setLocationID(thisLocation.getLocationID());
        newLocation.setAddressCity("2222");
        locDao.updateLocation(newLocation);
        List<Location> ThisLocationTwo = locDao.getAllLocations();
        Location thisLocationAgain = ThisLocationTwo.get(0);
        String stringOutput = thisLocationAgain.getAddressCity();

        assertEquals(stringOutput, "2222");

    }

    @Test
    public void getLocationIncidentId() {
        Location newLocation = new Location();
        newLocation.setAddressCity("1111");
        newLocation.setAddressNumber("1111");
        newLocation.setAddressStreet("1111");
        newLocation.setAddressZipcode("1111");
        newLocation.setLocationDescription("1111");
        newLocation.setLatitude("1111");
        newLocation.setLongitude("1111");

        locDao.addLocation(newLocation);
        List<Location> allLocaitons = locDao.getAllLocations();
        Location thisLocation = allLocaitons.get(0);
        int locationSizeint = thisLocation.getLocationID();
        Location two = locDao.getLocationIncidentId(locationSizeint);
        String pullInfo = two.getAddressCity();
        assertEquals(pullInfo, "1111");
    }

    @Test
    public void getAllLocations() {
        Location newLocation = new Location();
        newLocation.setAddressCity("1111");
        newLocation.setAddressNumber("1111");
        newLocation.setAddressStreet("1111");
        newLocation.setAddressZipcode("1111");
        newLocation.setLocationDescription("1111");
        newLocation.setLatitude("1111");
        newLocation.setLongitude("1111");

        locDao.addLocation(newLocation);
        List<Location> allLocaitons = locDao.getAllLocations();
        Location thisLocation = allLocaitons.get(0);
        int locationSizeint = thisLocation.getLocationID();
        locDao.getLocationIncidentId(locationSizeint);
        List<Location> allLocaitonsTwo = locDao.getAllLocations();
        locationSizeint = allLocaitonsTwo.size();
        assertEquals(locationSizeint, 1);
    }

    @Test
    public void getOrganization() {
        Organization organizaiton = new Organization();
        organizaiton.setOrganizaitonAddressZipcode("1111");
        organizaiton.setOrganizationAddressCity("1111");
        organizaiton.setOrganizationAddressNumber("11111");
        organizaiton.setOrganizationAddressStreet("1111");
        organizaiton.setOrganizationContactEmail("11111");
        organizaiton.setOrganizationDescription("11111");
        organizaiton.setOrganizationName("11111");
        orgDao.addOrganization(organizaiton);
        List<Organization> list = orgDao.getAllOrganization();
        Organization OrganizationTwo = list.get(0);
        Organization OrganizationThree = orgDao.getOrganizationId(OrganizationTwo.getOrganizationId());
        int one = organizaiton.getOrganizationId();
        int two = OrganizationThree.getOrganizationId();
        assertEquals(one, one);

    }

    @Test
    public void getAllOrganization() {
        Organization organizaiton = new Organization();
        organizaiton.setOrganizaitonAddressZipcode("1111");
        organizaiton.setOrganizationAddressCity("1111");
        organizaiton.setOrganizationAddressNumber("11111");
        organizaiton.setOrganizationAddressStreet("1111");
        organizaiton.setOrganizationContactEmail("11111");
        organizaiton.setOrganizationDescription("11111");
        organizaiton.setOrganizationName("11111");
        orgDao.addOrganization(organizaiton);

        orgDao.addOrganization(organizaiton);

        orgDao.addOrganization(organizaiton);

        List<Organization> organizationList = orgDao.getAllOrganization();
        int enhancedListint = organizationList.size();
        assertEquals(enhancedListint, 3);
    }

    @Test
    public void addOrganization() {
        Organization organizaiton = new Organization();
        organizaiton.setOrganizaitonAddressZipcode("1111");
        organizaiton.setOrganizationAddressCity("1111");
        organizaiton.setOrganizationAddressNumber("11111");
        organizaiton.setOrganizationAddressStreet("1111");
        organizaiton.setOrganizationContactEmail("11111");
        organizaiton.setOrganizationDescription("11111");
        organizaiton.setOrganizationName("11111");
        orgDao.addOrganization(organizaiton);

        orgDao.addOrganization(organizaiton);

        orgDao.addOrganization(organizaiton);

        List<Organization> organizationList = orgDao.getAllOrganization();
        int enhancedListint = organizationList.size();
        assertEquals(enhancedListint, 3);
    }

    @Test
    public void deleteOrganization() {
        Organization organizaiton = new Organization();
        organizaiton.setOrganizaitonAddressZipcode("1111");
        organizaiton.setOrganizationAddressCity("1111");
        organizaiton.setOrganizationAddressNumber("11111");
        organizaiton.setOrganizationAddressStreet("1111");
        organizaiton.setOrganizationContactEmail("11111");
        organizaiton.setOrganizationDescription("11111");
        organizaiton.setOrganizationName("11111");
        orgDao.addOrganization(organizaiton);

        orgDao.addOrganization(organizaiton);

        orgDao.addOrganization(organizaiton);
        List<Organization> list = orgDao.getAllOrganization();
        Organization newOrg = list.get(0);
        int identifire = newOrg.getOrganizationId();
        orgDao.deleteOrganization(identifire);

        List<Organization> organizationList = orgDao.getAllOrganization();
        int enhancedListint = organizationList.size();
        assertEquals(enhancedListint, 2);

    }

    @Test
    public void updateOrganization() {
        Organization organizaiton = new Organization();
        organizaiton.setOrganizaitonAddressZipcode("1111");
        organizaiton.setOrganizationAddressCity("1111");
        organizaiton.setOrganizationAddressNumber("11111");
        organizaiton.setOrganizationAddressStreet("1111");
        organizaiton.setOrganizationContactEmail("11111");
        organizaiton.setOrganizationDescription("11111");
        organizaiton.setOrganizationName("11111");
        orgDao.addOrganization(organizaiton);
        List<Organization> list = orgDao.getAllOrganization();
        Organization thisOrganizaiton = list.get(0);
        thisOrganizaiton.setOrganizaitonAddressZipcode("2222");
        orgDao.updateOrganization(thisOrganizaiton);
        assertEquals(thisOrganizaiton.getOrganizaitonAddressZipcode(), "2222");

    }

    @Test
    public void getOrganizationId() {
        Organization organizaiton = new Organization();
        organizaiton.setOrganizaitonAddressZipcode("1111");
        organizaiton.setOrganizationAddressCity("1111");
        organizaiton.setOrganizationAddressNumber("11111");
        organizaiton.setOrganizationAddressStreet("1111");
        organizaiton.setOrganizationContactEmail("11111");
        organizaiton.setOrganizationDescription("11111");
        organizaiton.setOrganizationName("11111");
        orgDao.addOrganization(organizaiton);
        List<Organization> list = orgDao.getAllOrganization();
        Organization thisOrganizaiton = list.get(0);
        thisOrganizaiton.setOrganizaitonAddressZipcode("2222");
        orgDao.updateOrganization(thisOrganizaiton);
        assertEquals(thisOrganizaiton.getOrganizationId(), thisOrganizaiton.getOrganizationId());
    }

    @Test
    public void getAllSightings() {
        Enhanced thisEnhanced = new Enhanced();
        Location thisLocation = new Location();
        thisLocation.setLocationID(1);
        thisEnhanced.setIdentifier(1);
        Sightings sighting = new Sightings();
        sighting.setDateStamp("12-06-2007");
        sighting.setEnhancedSighted("Mike");
        sighting.setEnahncedComponent(thisEnhanced);
        sighting.setLocationComponet(thisLocation);

        sigDao.addSightings(sighting);
        sigDao.addSightings(sighting);
        sigDao.addSightings(sighting);
        List<Sightings> size = sigDao.getAllSightings();
        int SizeInt = size.size();
        assertEquals(1, 1);
    }

    @Test
    public void addSightings() {
        Enhanced thisEnhanced = new Enhanced();
        Location thisLocation = new Location();
        thisLocation.setLocationID(1);
        thisEnhanced.setIdentifier(1);
        Sightings sighting = new Sightings();
        sighting.setDateStamp("12-06-2007");
        sighting.setEnhancedSighted("Mike");
        sighting.setEnahncedComponent(thisEnhanced);
        sighting.setLocationComponet(thisLocation);
        sighting.setEnhancedSighted("person");
        sigDao.addSightings(sighting);
        sigDao.addSightings(sighting);
        sigDao.addSightings(sighting);
        List<Sightings> size = sigDao.getAllSightings();
        int SizeInt = size.size();
        assertEquals(SizeInt, 3);
    }

    @Test
    public void deleteSightings() {
        Enhanced thisEnhanced = new Enhanced();
        Location thisLocation = new Location();
        thisLocation.setLocationID(1);
        thisEnhanced.setIdentifier(1);
        Sightings sighting = new Sightings();
        sighting.setDateStamp("12-06-2007");
        sighting.setEnhancedSighted("Mike");
        sighting.setEnahncedComponent(thisEnhanced);
        sighting.setLocationComponet(thisLocation);
        sigDao.addSightings(sighting);
        sigDao.addSightings(sighting);
        sigDao.addSightings(sighting);
        List<Sightings> size = sigDao.getAllSightings();
        Sightings seeME = size.get(0);
        sigDao.deleteSightings(seeME.getIncidentId());
        List<Sightings> sizeTwo = sigDao.getAllSightings();
        assertEquals(sizeTwo.size(), 2);
    }

    @Test
    public void updateSightings() {
        Enhanced thisEnhanced = new Enhanced();
        Location thisLocation = new Location();
        thisLocation.setLocationID(1);
        thisEnhanced.setIdentifier(1);
        Sightings sighting = new Sightings();
        sighting.setDateStamp("12-06-2007");
        sighting.setEnhancedSighted("Mike");
        sighting.setEnahncedComponent(thisEnhanced);
        sighting.setLocationComponet(thisLocation);

        sigDao.addSightings(sighting);
        sigDao.addSightings(sighting);
        sigDao.addSightings(sighting);
        List<Sightings> size = sigDao.getAllSightings();
        Sightings seeME = size.get(0);

        seeME.setEnhancedSighted("green");
        sigDao.updateSightings(seeME);
        List<Sightings> sizeTwo = sigDao.getAllSightings();
        Sightings sawn = sigDao.getSightingsById(seeME.getIncidentId());

        assertEquals(sawn.getEnhancedSighted(), "green");
    }

    @Test
    public void getSightingsById() {
        Enhanced thisEnhanced = new Enhanced();
        Location thisLocation = new Location();
        thisLocation.setLocationID(1);
        thisEnhanced.setIdentifier(1);
        Sightings sighting = new Sightings();
        sighting.setDateStamp("12-06-2007");
        sighting.setEnhancedSighted("Mike");
        sighting.setEnahncedComponent(thisEnhanced);
        sighting.setLocationComponet(thisLocation);
        sigDao.addSightings(sighting);
        sigDao.addSightings(sighting);
        sigDao.addSightings(sighting);
        List<Sightings> size = sigDao.getAllSightings();
        Sightings seeME = size.get(0);

        seeME.setEnhancedSighted("green");
        sigDao.updateSightings(seeME);
        List<Sightings> sizeTwo = sigDao.getAllSightings();
        Sightings sawn = sigDao.getSightingsById(seeME.getIncidentId());

        assertEquals(sawn.getEnhancedSighted(), "green");
    }
}
