/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.service;

import com.mycompany.superherosighting.dao.SuperHeroEnhancedDao;
import com.mycompany.superherosighting.dao.SuperHeroLocationDao;
import com.mycompany.superherosighting.dao.SuperHeroOrganizaitonDao;
import com.mycompany.superherosighting.dao.SuperHeroSightingsDao;
import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.Location;
import com.mycompany.superherosighting.model.Organization;
import com.mycompany.superherosighting.model.Sightings;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Mike
 */
public class SuperHeroServiceLayerImplTest {
   SuperHeroOrganizaitonDao orgDao;
    SuperHeroEnhancedDao enhDao;
    SuperHeroSightingsDao sigDao;
    SuperHeroLocationDao locDao;
    /**
     * SuperHeroOrganizaitonDao orgService; SuperHeroEnhancedDao enhService;
     * SuperHeroServiceSightingsLayer sigService; SuperHeroLocationDao
     * locService;*
     */
    ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
    SuperHeroServiceEnhancedLayer enhService = ctx.getBean("SuperHeroServiceEnhancedLayer", SuperHeroServiceEnhancedLayer.class);

    ApplicationContext ctxTwo = new ClassPathXmlApplicationContext("test-applicationContext.xml");
    SuperHeroServiceSightingsLayer sigService = ctxTwo.getBean("SuperHeroServiceSightingsLayer", SuperHeroServiceSightingsLayer.class);

    ApplicationContext ctxThree = new ClassPathXmlApplicationContext("test-applicationContext.xml");
    SuperHeroServiceLocationLayer locService = ctxTwo.getBean("SuperHeroServiceLocationLayer", SuperHeroServiceLocationLayer.class);

    ApplicationContext ctxFour = new ClassPathXmlApplicationContext("test-applicationContext.xml");
    SuperHeroServiceOrganizationLayer orgService = ctxTwo.getBean("SuperHeroServiceOrganizationLayer", SuperHeroServiceOrganizationLayer.class);

 

    public SuperHeroServiceLayerImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        enhDao = ctx.getBean("SuperHeroEnhancedDao", SuperHeroEnhancedDao.class);

        ApplicationContext ctxTwo = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        locDao = ctxTwo.getBean("SuperHeroLocationDao", SuperHeroLocationDao.class);

        ApplicationContext ctxThree = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        sigDao = ctxThree.getBean("SuperHeroSightingsDao", SuperHeroSightingsDao.class);

        ApplicationContext ctxFour = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        orgDao = ctxFour.getBean("SuperHeroOrganizationDao", SuperHeroOrganizaitonDao.class);
    }

    @After
    public void tearDown() {

    }

    @Test
    public void validateSightings() {

    }

    @Test
    public void tenSightings() {

    }

    @Test
    public void deleteEnhanced() {
        Enhanced enhanced = new Enhanced();
        enhanced.setAllignment("good");
        enhanced.setIdentifier(1);
        enhanced.setName("mike");
        enhanced.setPowers("Mike");
        Organization organization = new Organization();
        organization.setOrganizationName("mike");

     
    }

    @Test
    public void allSightingsDetailsOnDate() {
        Enhanced enhanced = new Enhanced();
        enhanced.setAllignment("good");
        enhanced.setIdentifier(1);
        enhanced.setName("mike");
        enhanced.setPowers("Mike");
        Organization organization = new Organization();
        organization.setOrganizationName("mike");
        enhanced.setOrganizationComponent(organization);
        Sightings sighting = new Sightings();
        sighting.setDateStamp("2");
        sighting.setEnhancedSighted("mike");
        sighting.setEnahncedComponent(enhanced);
        Location location = new Location();
        location.setLocationID(1);
        sighting.setLocationComponet(location);
        sigDao.addSightings(sighting);

    }

    @Test
    public void sightingsAtAnyLocation() {

    }

}
