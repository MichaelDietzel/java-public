/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.dao;

import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.Organization;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Mike
 */
public class SuperHeroEnhancedDaoImpl implements SuperHeroEnhancedDao {

    private static final String SQL_INSERT_ENHANCED
            = "insert into enhanced"
            + "( enhancedName, powerDescription, enahncedAlignment, organizationName)"
            + "values (?, ?, ?, ?)";
    private static final String SQL_DELETE_ENHANCED
            = "delete from enhanced where enhancedId = ?";
    private static final String SQL_SELECT_ENHANCED
            = "select * from enhanced where enhancedId = ?";
    private static final String SQL_UPDATE_ENHANCED
            = "update enhanced set "
            + "enhancedName= ?, powerDescription = ?, enahncedAlignment = ?, organizationName = ? "
            + "where enhancedId = ?";
    private static final String SQL_SELECT_ALL_ENHANCED
            = "select * from ENHANCED";
    private static final String SQL_SELECT_ENHANCED_BY_ORGANIZATION_NAME
            = "select * from ENHANCED where organizationName = ?";

    private static final class EnhancedMapper implements RowMapper<Enhanced> {

        public Enhanced mapRow(ResultSet rs, int rowNum) throws SQLException {
            Enhanced EnhancedPerson = new Enhanced();

            EnhancedPerson.setName(rs.getString("enhancedName"));
            EnhancedPerson.setPowers(rs.getString("powerDescription"));
            EnhancedPerson.setAllignment(rs.getString("enahncedAlignment"));
            Organization organizationForMapper = new Organization();
            organizationForMapper.setOrganizationName(rs.getString("organizationName"));
            EnhancedPerson.setOrganizationComponent(organizationForMapper);
            EnhancedPerson.setIdentifier(rs.getInt("enhancedId"));
            return EnhancedPerson;

        }
    }

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Enhanced addEnhanced(Enhanced enhancedIndividual) {
        jdbcTemplate.update(SQL_INSERT_ENHANCED,
                enhancedIndividual.getName(),
                enhancedIndividual.getPowers(),
                enhancedIndividual.getAllignment(),
                enhancedIndividual.getOrganizationComponent().getOrganizationName());
        int newUniqueId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                Integer.class);
        enhancedIndividual.setIdentifier(newUniqueId);

        return enhancedIndividual;
    }

    @Override
    public void deleteEnhanced(int UniqueId) {
        jdbcTemplate.update(SQL_DELETE_ENHANCED, UniqueId);
    }

    @Override
    public void updateEnhanced(Enhanced enhancedIndividual) {
        jdbcTemplate.update(SQL_UPDATE_ENHANCED,
                enhancedIndividual.getName(),
                enhancedIndividual.getPowers(),
                enhancedIndividual.getAllignment(),
                enhancedIndividual.getOrganizationComponent().getOrganizationName(),
                enhancedIndividual.getIdentifier());
    }

    @Override
    public Enhanced getEnhancedId(int enhancedId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_ENHANCED,
                    new EnhancedMapper(), enhancedId);
        } catch (EmptyResultDataAccessException ex) {

            return null;
        }

    }

    @Override
    public List<Enhanced> getAllEnhanced() {
        return jdbcTemplate.query(SQL_SELECT_ALL_ENHANCED,
                new EnhancedMapper());
    }

   

    @Override
    public List<Enhanced> getAllEnhancedInOrganizaiton(String identifier) {

        return jdbcTemplate.query(SQL_SELECT_ENHANCED_BY_ORGANIZATION_NAME,
                new SuperHeroEnhancedDaoImpl.EnhancedMapper(),
                identifier);
    }

}
