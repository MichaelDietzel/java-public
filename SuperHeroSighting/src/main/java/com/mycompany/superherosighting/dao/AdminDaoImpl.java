/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.dao;

/**
 *
 * @author Mike
 */
import com.mycompany.superherosighting.model.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Transactional;

public class AdminDaoImpl implements AdminDao {

    private static final String SQL_UPDATE_USER
            = "update users set "
            + "username= ?, password = ?,enabled = ?"
            + "where user_id = ?";
    private static final String SQL_UPDATE_AUTHORITY
            = "update authorities set "
            + "authority= ?"
            + "where username = ?";

    private static final String SQL_Disable_USER
            = "update users set "
            + "username= ?, password = ?,enabled = ?"
            + "where user_id = ?";

    private static final String SQL_INSERT_USER
            = "insert into users (username, password, enabled) values (?, ?, 1)";
    private static final String SQL_INSERT_AUTHORITY
            = "insert into authorities (username, authority) values (?, ?)";
    private static final String SQL_DELETE_USER
            = "delete from users where username = ?";
    private static final String SQL_DELETE_AUTHORITIES
            = "delete from authorities where username = ?";
    private static final String SQL_GET_ALL_USERS
            = "select * from users";
    private static final String SQL_GET_ALL_ENABLED
            = "select * from users where enabled = 1";

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional
    public User addUser(User newUser) {
        jdbcTemplate.update(SQL_INSERT_USER,
                newUser.getUsername(),
                newUser.getPassword());
        newUser.setId(jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                Integer.class));

        // now insert user's roles
        ArrayList<String> authorities = newUser.getAuthorities();
        for (String authority : authorities) {
            jdbcTemplate.update(SQL_INSERT_AUTHORITY,
                    newUser.getUsername(),
                    authority);
        }

        return newUser;
    }

    @Override
    public void deleteUser(String username) {
        jdbcTemplate.update(SQL_DELETE_AUTHORITIES, username);
        jdbcTemplate.update(SQL_DELETE_USER, username);
    }

    @Override
    public List<User> getAllUsers() {
        return jdbcTemplate.query(SQL_GET_ALL_USERS, new UserMapper());
    }

    @Override
    public List<User> getAllEnabledUsers() {
        return jdbcTemplate.query(SQL_GET_ALL_ENABLED, new UserMapper());
    }

    @Override
    @Transactional
    public void updateUser(User user) {
        ArrayList<String> authorities = user.getAuthorities();
        for (String authority : authorities) {
            jdbcTemplate.update(SQL_UPDATE_AUTHORITY,
                    user.getUsername(),
                    authority);
        }
            jdbcTemplate.update(SQL_UPDATE_USER,
                    user.getUsername(),
                    user.getPassword(),
                    user.getEnabled(),
                    user.getId());
    }

    private static final class UserMapper implements RowMapper<User> {

        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            user.setId(rs.getInt("user_id"));
            user.setUsername(rs.getString("username"));
            user.setPassword(rs.getString("password"));
            user.setEnabled(rs.getString("enabled"));
            return user;
        }
    }

    public void DisableUser(User user) {
        jdbcTemplate.update(SQL_Disable_USER,
                user.getUsername(),
                user.getPassword(),
                user.getEnabled(),
                user.getId());

    }
}
