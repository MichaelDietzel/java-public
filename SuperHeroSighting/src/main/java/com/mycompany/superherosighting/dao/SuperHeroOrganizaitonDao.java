/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.dao;

import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.Organization;
import java.util.List;

/**
 *
 * @author Mike
 */
public interface SuperHeroOrganizaitonDao {
    


    public List<Organization> getOrganization();

    public List<Organization> getAllOrganization();

    public Organization addOrganization(Organization organization);

    public void deleteOrganization(int OrganizationId);

    public void updateOrganization(Organization organization);

    public Organization getOrganizationId(int OrganizationId);
    
    public <List> Organization getOrganizaiotnbyName(String organizaitonName);


}
