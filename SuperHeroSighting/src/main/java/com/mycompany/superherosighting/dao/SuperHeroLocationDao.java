/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.dao;

import com.mycompany.superherosighting.model.Location;
import java.util.List;

/**
 *
 * @author Mike
 */
public interface SuperHeroLocationDao {

    public Location addLocation(Location location);

    public void deleteLocation(int locationId);

    public void updateLocation(Location location);

    public Location getLocationIncidentId(int incidentId);

    public List<Location> getAllLocations();

    public List<Location> getlocationIncidentByZipcode(int zipcode);

    public List<Location> getlocationIncidentByEnhanced(int enhancedId);
}
