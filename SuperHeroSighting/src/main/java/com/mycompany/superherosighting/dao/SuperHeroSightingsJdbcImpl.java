/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.dao;

import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.Location;
import com.mycompany.superherosighting.model.Sightings;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Mike
 */
public class SuperHeroSightingsJdbcImpl implements SuperHeroSightingsDao {

    private static final String SQL_INSERT_SIGHTINGS
            = "insert into SIGHTINGS"
            + "( enhancedSighted, dateStamp, enhancedId,  locationId)"
            + "values (?, ?, ?, ?)";
    private static final String SQL_DELETE_SIGHTINGS
            = "delete from SIGHTINGS where incidentId = ?";
    private static final String SQL_SELECT_SIGHTINGS_BY_ID
            = "select * from SIGHTINGS where incidentId = ?";
    private static final String SQL_UPDATE_SIGHTINGS
            = "update SIGHTINGS set "
            + "enhancedSighted = ?, dateStamp = ?, enhancedId = ?, locationId = ? "
            + "where incidentId = ?";
    private static final String SQL_SELECT_ALL_SIGHTINGS
            = "select * from SIGHTINGS";
    private static final String SQL_SELECT_SIGHTINGS_BY_LOCATIONID
            = "select * from SIGHTINGS where locationId = ?";
    private static final String SQL_SELECT_SIGHTINGS_BY_ENHANCEDID
            = "select * from SIGHTINGS where enhancedId =? ";
    private static final String SQL_SELECT_DATESTAMP
            = "select * from SIGHTINGS where dateStamp = ?";
    private static final String SQL_SELECT_
            = "select b.book_id, b.isbn, b.title, b.publisher_id, b.price, "
            + "b.publish_date from books b join books_authors ba on author_id "
            + "where b.book_id = ba.book_id "
            + "and ba.author_id  =  ?;";

    private static final class SightingMapper implements RowMapper<Sightings> {

        public Sightings mapRow(ResultSet rs, int rowNum) throws SQLException {
            Sightings sightingItem = new Sightings();
            sightingItem.setEnhancedSighted(rs.getString("enhancedSighted"));
            sightingItem.setDateStamp(rs.getString("dateStamp"));
            Enhanced enhancedForComponent = new Enhanced();
            enhancedForComponent.setIdentifier(rs.getInt("enhancedId"));
            sightingItem.setEnahncedComponent(enhancedForComponent);
            Location locationForComponent = new Location();
            locationForComponent.setLocationID(rs.getInt("locationId"));
            sightingItem.setLocationComponet(locationForComponent);

            sightingItem.setIncidentId(rs.getInt("incidentId"));
            return sightingItem;
        }
    }
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Sightings> getAllSightingsByLocation(int identifier) {
        return jdbcTemplate.query(SQL_SELECT_SIGHTINGS_BY_LOCATIONID,
                new SuperHeroSightingsJdbcImpl.SightingMapper(),
                identifier);
    }

    @Override
    public List<Sightings> getAllSightingsByEnhanced(int identifier) {
        return jdbcTemplate.query(SQL_SELECT_SIGHTINGS_BY_ENHANCEDID,
                new SuperHeroSightingsJdbcImpl.SightingMapper(), identifier);
    }

    @Override
    public List<Sightings> getSightingEnhancedLocationbyDate(String Date) {

        List<Sightings> sightingsOnDate = jdbcTemplate.query(SQL_SELECT_DATESTAMP,
                new SuperHeroSightingsJdbcImpl.SightingMapper(),
                Date);

        return sightingsOnDate;
    }

    @Override
    public List<Sightings> getAllSightings() {
        return jdbcTemplate.query(SQL_SELECT_ALL_SIGHTINGS,
                new SuperHeroSightingsJdbcImpl.SightingMapper());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Sightings addSightings(Sightings sightings) {
        jdbcTemplate.update(SQL_INSERT_SIGHTINGS,
                sightings.getEnhancedSighted(),
                sightings.getDateStamp(),
                sightings.getEnahncedComponent().getIdentifier(),
                sightings.getLocationComponet().getLocationID());
        int incidentId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);
        sightings.setIncidentId(incidentId);
        return sightings;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteSightings(int sightingsId
    ) {
        jdbcTemplate.update(SQL_DELETE_SIGHTINGS, sightingsId);
    }

    @Override
    public void updateSightings(Sightings sightings
    ) {
        jdbcTemplate.update(SQL_UPDATE_SIGHTINGS,
                sightings.getEnhancedSighted(),
                sightings.getDateStamp(),
                sightings.getEnahncedComponent().getIdentifier(),
                sightings.getLocationComponet().getLocationID(),
                sightings.getIncidentId());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Sightings getSightingsById(int SightingsId
    ) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_SIGHTINGS_BY_ID,
                    new SuperHeroSightingsJdbcImpl.SightingMapper(),
                    SightingsId);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }

    }
}
