/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.dao;

import com.mycompany.superherosighting.model.User;
import java.util.List;

/**
 *
 * @author Mike
 */
public interface AdminDao {
    
    public User addUser(User newUser);
    public void deleteUser(String username);
    public List<User> getAllUsers();
    public List<User> getAllEnabledUsers();
    public void updateUser(User user);
    public void DisableUser(User user); 
    
    
}
