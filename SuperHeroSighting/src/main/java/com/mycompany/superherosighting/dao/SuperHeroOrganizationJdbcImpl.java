/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.dao;

import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.Organization;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author Mike
 */
public class SuperHeroOrganizationJdbcImpl implements SuperHeroOrganizaitonDao {

    private static final String SQL_INSERT_ORGANIZATION
            = "insert into organization"
            + "(organizationName, organizationDescription, organizationAddressNumber, organizationAddressStreet, organizationAddressCity, organizaitonAddressZipcode, organizationContactEmail)"
            + "values ( ?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_DELETE_ORGANIZATION
            = "delete from organization where organizationId = ?";
    private static final String SQL_SELECT_ORGANIZATION
            = "select * from organization where organizationId = ?";
    private static final String SQL_UPDATE_ORGANIZATION
            = "update organization set "
            + "organizationName = ?, organizationDescription = ?,organizationAddressNumber = ?, organizationAddressStreet = ?, organizationAddressCity = ?, organizaitonAddressZipcode = ?, organizationContactEmail = ? "
            + "where organizationId = ?";
    private static final String SQL_SELECT_ALL_ORGANIZATION
            = "select * from organization";
    private static final String SQL_SELECT_ALL_ORGANIZATIONNAME
            = "select * from organization where organizationName = ?";

    private static final class OrganizationMapper implements RowMapper<Organization> {

        public Organization mapRow(ResultSet rs, int rowNum) throws SQLException {
            Organization organizationItem = new Organization();

            organizationItem.setOrganizationName(rs.getString("organizationName"));
            organizationItem.setOrganizationDescription(rs.getString("organizationDescription"));
            organizationItem.setOrganizationAddressNumber(rs.getString("organizationAddressNumber"));
            organizationItem.setOrganizationAddressStreet(rs.getString("organizationAddressStreet"));
            organizationItem.setOrganizationAddressCity(rs.getString("organizationAddressCity"));
            organizationItem.setOrganizaitonAddressZipcode(rs.getString("organizaitonAddressZipcode"));
            organizationItem.setOrganizationContactEmail(rs.getString("organizationContactEmail"));
            organizationItem.setOrganizationId(rs.getInt("organizationId"));
            return organizationItem;

        }
    }

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public <List> Organization getOrganizaiotnbyName(String organizaitonName) {
        return jdbcTemplate.queryForObject(SQL_SELECT_ALL_ORGANIZATIONNAME,
                new SuperHeroOrganizationJdbcImpl.OrganizationMapper(),organizaitonName);
    }

    @Override
    public List<Organization> getOrganization() {
        return jdbcTemplate.query(SQL_SELECT_ORGANIZATION,
                new SuperHeroOrganizationJdbcImpl.OrganizationMapper());
    }

    @Override
    public Organization addOrganization(Organization organization) {
        jdbcTemplate.update(SQL_INSERT_ORGANIZATION,
                organization.getOrganizationName(),
                organization.getOrganizationDescription(),
                organization.getOrganizationAddressNumber(),
                organization.getOrganizationAddressStreet(),
                organization.getOrganizationAddressCity(),
                organization.getOrganizaitonAddressZipcode(),
                organization.getOrganizationContactEmail());
        int newOrganizationAndIndividualID = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                Integer.class);
        organization.setOrganizationId(newOrganizationAndIndividualID);

        return organization;
    }

    @Override
    public void deleteOrganization(int OrganizationId) {
        jdbcTemplate.update(SQL_DELETE_ORGANIZATION, OrganizationId);
    }

    @Override
    public void updateOrganization(Organization organization) {
        jdbcTemplate.update(SQL_UPDATE_ORGANIZATION,
                organization.getOrganizationName(),
                organization.getOrganizationDescription(),
                organization.getOrganizationAddressNumber(),
                organization.getOrganizationAddressStreet(),
                organization.getOrganizationAddressCity(),
                organization.getOrganizaitonAddressZipcode(),
                organization.getOrganizationContactEmail(),
                organization.getOrganizationId());

    }

    @Override
    public Organization getOrganizationId(int OrganizationId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_ORGANIZATION,
                    new SuperHeroOrganizationJdbcImpl.OrganizationMapper(), OrganizationId);
        } catch (EmptyResultDataAccessException ex) {

            return null;
        }
    }

    @Override
    public List<Organization> getAllOrganization() {
        return jdbcTemplate.query(SQL_SELECT_ALL_ORGANIZATION,
                new SuperHeroOrganizationJdbcImpl.OrganizationMapper());
    }
}
