/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.dao;

import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.Sightings;
import java.util.List;

/**
 *
 * @author Mike
 */
public interface SuperHeroEnhancedDao {
    
    
    public Enhanced addEnhanced(Enhanced enhanced);

    public void deleteEnhanced(int EnhancedId);

    public void updateEnhanced(Enhanced enhanced);

    public Enhanced getEnhancedId(int enhancedid);

    public List<Enhanced> getAllEnhanced();
    
    public List<Enhanced> getAllEnhancedInOrganizaiton(String identifier);

}
