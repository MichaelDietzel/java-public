/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.dao;

import com.mycompany.superherosighting.model.Location;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Mike
 */
public class SuperHeroLocationJdbcImpl implements SuperHeroLocationDao {

    private static final String SQL_INSERT_LOCATION
            = "insert into location"
            + "(addressNumber, addressStreet, addressCity, addressZipcode, latitude, longitude, locationsDescription)"
            + "values (?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_DELETE_LOCATION
            = "delete from location where locationID = ?";
    private static final String SQL_SELECT_LOCATION
            = "select * from location where locationID = ?";
    private static final String SQL_UPDATE_LOCATION
            = "update location set "
            + "addressNumber = ?, addressStreet = ?, addressCity = ?,addressZipcode = ?,latitude = ?, longitude = ?, locationsDescription = ? "
            + "where locationId = ?";
    private static final String SQL_SELECT_ALL_LOCATION
            = "select * from location";
    private static final String SQL_SELECT_LOCATION_ZIPCODE
            = "select * from location where addressZipcode = ?";
    private static final String SQL_SELECT_LOCATION_BY_ENHANCED
            = "select * from location where enhancedSighted = ?";

    private static final class LocationMapper implements RowMapper<Location> {

        public Location mapRow(ResultSet rs, int rowNum) throws SQLException {
            Location locationIncident = new Location();
            locationIncident.setAddressNumber(rs.getString("addressNumber"));
            locationIncident.setAddressStreet(rs.getString("addressStreet"));
            locationIncident.setAddressCity(rs.getString("addressCity"));
            locationIncident.setAddressZipcode(rs.getString("addressZipcode"));
            locationIncident.setLatitude(rs.getString("latitude"));
            locationIncident.setLongitude(rs.getString("longitude"));
            locationIncident.setLocationDescription(rs.getString("locationsDescription"));
            locationIncident.setLocationID(rs.getInt("locationId"));
            return locationIncident;
        }
    }

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Location addLocation(Location location) {
        jdbcTemplate.update(SQL_INSERT_LOCATION,
                location.getAddressNumber(),
                location.getAddressStreet(),
                location.getAddressCity(),
                location.getAddressZipcode(),
                location.getLatitude(),
                location.getLongitude(),
                location.getLocationDescription());
        int incidentId
                = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);

        location.setLocationID(incidentId);
        return location;
    }

    @Override
    public void deleteLocation(int locationId) {
        jdbcTemplate.update(SQL_DELETE_LOCATION, locationId);
    }

    @Override
    public void updateLocation(Location location) {
        jdbcTemplate.update(SQL_UPDATE_LOCATION,
                location.getAddressNumber(),
                location.getAddressStreet(),
                location.getAddressCity(),
                location.getAddressZipcode(),
                location.getLatitude(),
                location.getLongitude(),
                location.getLocationDescription(),
                location.getLocationID());
    }

    @Override
    public Location getLocationIncidentId(int incidentId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_LOCATION,
                    new LocationMapper(),
                    incidentId);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Location> getAllLocations() {
        return jdbcTemplate.query(SQL_SELECT_ALL_LOCATION,
                new LocationMapper());
    }

    @Override
    public List<Location> getlocationIncidentByZipcode(int zipcode) {
        List<Location> LocationList
                = jdbcTemplate.query(SQL_SELECT_LOCATION_ZIPCODE,
                        new LocationMapper(),
                        zipcode);
        return LocationList;
    }

    @Override
    public List<Location> getlocationIncidentByEnhanced(int enhancedId) {
        List<Location> LocationList
                = jdbcTemplate.query(SQL_SELECT_LOCATION_BY_ENHANCED,
                        new LocationMapper(),
                        enhancedId);

        return LocationList;
    }
    
  
}
