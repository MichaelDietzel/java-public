/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.dao;

import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.Location;
import com.mycompany.superherosighting.model.Organization;
import com.mycompany.superherosighting.model.Sightings;
import java.util.List;

/**
 *
 * @author Mike
 */
public interface SuperHeroSightingsDao {
    
    public List<Sightings> getAllSightingsByLocation(int identifier);

    public List<Sightings> getAllSightingsByEnhanced(int identifier);

    public List<Sightings> getSightingEnhancedLocationbyDate(String Date);
    
    public List<Sightings> getAllSightings();

    public Sightings addSightings(Sightings sightings);

    public void deleteSightings(int SightingsId);

    public void updateSightings(Sightings sightings);

    public Sightings getSightingsById(int SightingsId);

}
