/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.model;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

/**
 *
 * @author Mike
 */
public class Enhanced {

    int identifier;
    @Length(max = 20, message = "name must be no more than 20 characters in length.")
    String name;
    @Length(max = 20, message = "powers must be no more than 20 characters in length.")
    String powers;
    @Length(max = 20, message = "allignment must be no more than 20 characters in length.")
    String allignment;
    
    Organization organizationComponent;

    /**
     * need to add an Organization id you puts *
     */
    public Enhanced() {

    }

    public int getIdentifier() {
        return identifier;
    }

    public void setIdentifier(int identifier) {
        this.identifier = identifier;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPowers() {
        return powers;
    }

    public void setPowers(String powers) {
        this.powers = powers;
    }

    public String getAllignment() {
        return allignment;
    }

    public void setAllignment(String allignment) {
        this.allignment = allignment;
    }

    public Organization getOrganizationComponent() {
        return organizationComponent;
    }

    public void setOrganizationComponent(Organization organization) {
        this.organizationComponent = organization;
    }

    public Enhanced getEnhancedId(int enhancedId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void deleteEnhanced(int enhancedId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }



}
