/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.model;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

/**
 *
 * @author Mike
 */
public class Organization {

    
    @Length(max = 40, message = "Organization Name must be no more than 40 characters in length.")
    String organizationName;
    
    @Length(max = 40, message = "Organization Description must be no more than 40 characters in length.")
    String organizationDescription;
    
    @Length(max = 40, message = "Organization Address Number must be no more than 40 characters in length.")
    String organizationAddressNumber;
    
    @Length(max = 40, message = "organization Address Street must be no more than 40 characters in length.")
    String organizationAddressStreet;
    
    @Length(max = 40, message = "organization Address City must be no more than 40 characters in length.")
    String organizationAddressCity;
    
    @Length(max = 40, message = "organizaiton Address Zipcode must be no more than 40 characters in length.")
    String organizaitonAddressZipcode;
    
    @Email (message = "organization Contact Email must contain an @ symbol")
    @Length(max = 40, message = "organization Contact Email must be no more than 40 characters in length. And Contain an @ symbol")
    String organizationContactEmail;
    
    int organizationId;
    public Organization() {
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }


    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationDescription() {
        return organizationDescription;
    }

    public void setOrganizationDescription(String organizationDescription) {
        this.organizationDescription = organizationDescription;
    }


    public String getOrganizationAddressStreet() {
        return organizationAddressStreet;
    }

    public void setOrganizationAddressStreet(String organizationAddressStreet) {
        this.organizationAddressStreet = organizationAddressStreet;
    }

    public String getOrganizationAddressCity() {
        return organizationAddressCity;
    }

    public void setOrganizationAddressCity(String organizationAddressCity) {
        this.organizationAddressCity = organizationAddressCity;
    }

    public String getOrganizationAddressNumber() {
        return organizationAddressNumber;
    }

    public void setOrganizationAddressNumber(String organizationAddressNumber) {
        this.organizationAddressNumber = organizationAddressNumber;
    }

    public String getOrganizaitonAddressZipcode() {
        return organizaitonAddressZipcode;
    }

    public void setOrganizaitonAddressZipcode(String organizaitonAddressZipcode) {
        this.organizaitonAddressZipcode = organizaitonAddressZipcode;
    }

    public String getOrganizationContactEmail() {
        return organizationContactEmail;
    }

    public void setOrganizationContactEmail(String organizationContactEmail) {
        this.organizationContactEmail = organizationContactEmail;
    }
    
    
}