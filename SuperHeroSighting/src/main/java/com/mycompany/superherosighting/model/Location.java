/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.model;

import java.util.Date;
import org.hibernate.validator.constraints.Length;

/**
 *
 * @author Mike
 */
public class Location {
                    
    @Length(max = 40, message = "Address Number must be no more than 20 characters in length.")
    String addressNumber;
    @Length(max = 40, message = "Address Street must be no more than 20 characters in length.")
    String addressStreet;
    @Length(max = 40, message = "Address City must be no more than 20 characters in length.")
    String addressCity;
    @Length(max = 40, message = "Address Zip code must be no more than 20 characters in length.")
    String addressZipcode;
    @Length(max = 40, message = "Latitude must be no more than 20 characters in length.")
    String latitude;
    @Length(max = 40, message = "Longitude must be no more than 20 characters in length.")
    String longitude;
    @Length(max = 40, message = "Location Description must be no more than 20 characters in length.")
    String locationDescription;
    
    int locationID;
   public Location(){
        
    }

    public String getAddressNumber() {
        return addressNumber;
    }

    public void setAddressNumber(String addressNumber) {
        this.addressNumber = addressNumber;
    }

    public String getAddressZipcode() {
        return addressZipcode;
    }

    public void setAddressZipcode(String addressZipcode) {
        this.addressZipcode = addressZipcode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
   
    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public int getLocationID() {
        return locationID;
    }

    public void setLocationID(int locationID) {
        this.locationID = locationID;
    }



    public String getAddressStreet() {
        return addressStreet;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }




    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }
    }
