/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.model;

import java.time.LocalDate;
import java.util.Date;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

/**
 *
 * @author Mike
 */
public class Sightings {
    @Length(max = 20, message = "Email must be no more than 20 characters in length.")
    String enhancedSighted;
    @Length(max = 20, message = "Email must be no more than 20 characters in length.")
    String dateStamp;

    int incidentId;
    
    Enhanced enahncedComponent;
    
    Location locationComponet;

    public String getEnhancedSighted() {
        return enhancedSighted;
    }

    public void setEnhancedSighted(String enhancedSighted) {
        this.enhancedSighted = enhancedSighted;
    }

    public String getDateStamp() {
        return dateStamp;
    }

    public void setDateStamp(String dateStamp) {
        this.dateStamp = dateStamp;
    }

    public int getIncidentId() {
        return incidentId;
    }

    public void setIncidentId(int incidentId) {
        this.incidentId = incidentId;
    }

    public Enhanced getEnahncedComponent() {
        return enahncedComponent;
    }

    public void setEnahncedComponent(Enhanced enahncedComponent) {
        this.enahncedComponent = enahncedComponent;
    }

    public Location getLocationComponet() {
        return locationComponet;
    }

    public void setLocationComponet(Location locationComponet) {
        this.locationComponet = locationComponet;
    }

}
