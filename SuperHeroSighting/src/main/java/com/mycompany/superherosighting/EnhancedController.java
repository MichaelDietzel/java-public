/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting;

import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.Location;
import com.mycompany.superherosighting.model.Organization;
import com.mycompany.superherosighting.model.Sightings;
import com.mycompany.superherosighting.service.SuperHeroServiceEnhancedLayer;
import com.mycompany.superherosighting.service.SuperHeroServiceLocationLayer;
import com.mycompany.superherosighting.service.SuperHeroServiceOrganizationLayer;
import com.mycompany.superherosighting.service.SuperHeroServiceSightingsLayer;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Mike
 */
@Controller
public class EnhancedController {

    @Inject
    private SuperHeroServiceEnhancedLayer enhancedService;

    public EnhancedController() {
    }

    String jspPage = "index";
    String message = "The program is ready for use";
   

    @RequestMapping(value = "/dboGetallEnhanced", method = RequestMethod.POST)
    public String dboGetallEnhanced(HttpServletRequest request, Model model) {
        List<Enhanced> allEnhancedInDBO = enhancedService.getAllEnhanced();

        Enhanced enhanced = new Enhanced();
        model.addAttribute("enhanced", enhanced);
        model.addAttribute("allEnhancedInDBO", allEnhancedInDBO);
        return "EnhancedScreen";
    }

    /**
     * main enhanced servlets*
     */
    @RequestMapping(value = "/addEnhanced", method = RequestMethod.POST)
    public String EnhancedValidator(@Valid @ModelAttribute("enhanced") Enhanced enhanced, BindingResult result, HttpServletRequest request, Model model) {
        if (result.hasErrors()) {
            message = "error in your enhanced input information";
            model.addAttribute("message", message);
            return "EnhancedScreen";
        }
        enhancedService.addEnhanced(enhanced);
        message = "Your Item has been added to the database";

        Enhanced enhancedBlank = new Enhanced();
        model.addAttribute("enhanced", enhancedBlank);
        return "EnhancedScreen";
    }

    @RequestMapping(value = "/displayEnhancedForm", method = RequestMethod.GET)
    public String displayEnhancedForm(@Valid HttpServletRequest request, Model model) {
        String EnhancedIdParameter = request.getParameter("identifier");
        int enhancedId = Integer.parseInt(EnhancedIdParameter);
        Enhanced enhanced = enhancedService.getEnhancedId(enhancedId);
        model.addAttribute("enhanced", enhanced);
        message = "Your Item is Visible in the input box below";
        model.addAttribute("message", message);
        return "EnhancedScreen";
    }

    @RequestMapping(value = "/DeleteEnhancedForm", method = RequestMethod.GET)
    public String deleteEnhanced(@Valid @ModelAttribute("enhanced") Enhanced enhanced, BindingResult result, HttpServletRequest request, Model model) {
        String EnhancedIdParameter = request.getParameter("identifier");
        int enhancedId = Integer.parseInt(EnhancedIdParameter);
        enhanced = enhancedService.getEnhancedId(enhancedId);
        message = enhancedService.deleteEnhancedlogic(enhanced);

        Enhanced enhancedBlank = new Enhanced();
        model.addAttribute("enhanced", enhancedBlank);
        return "EnhancedScreen";
    }

    @RequestMapping(value = "/UpdateEnhancedForm", method = RequestMethod.GET)
    public String updateEnhanced(HttpServletRequest request, Model model) {
        String EnhancedIdParameter = request.getParameter("identifier");
        int enhancedId = Integer.parseInt(EnhancedIdParameter);
        Enhanced enhanced = enhancedService.getEnhancedId(enhancedId);
        model.addAttribute("enhanced", enhanced);
        return "UpdateEnhanced";
    }

    @RequestMapping(value = "/updatedEnhanced", method = RequestMethod.GET)
    public String updatedEnhanced(@Valid @ModelAttribute("enhanced") Enhanced enhanced, BindingResult result, HttpServletRequest request, Model model) {

        Enhanced enhancedBlank = new Enhanced();

        if (result.hasErrors()) {
            return "UpdateEnhanced";
        }
        enhancedService.updateEnhaned(enhanced);
        message = "Your Enhanced has been updated";
        model.addAttribute("message", message);
        model.addAttribute("enhanced", enhancedBlank);
        return "EnhancedScreen";
    }

    /**
     * Special queries *
     */
    @RequestMapping(value = "/AllEnhancedInOrganizaitonForm", method = RequestMethod.GET)
    public String AllEnhancedInOrganizaitonForm(@Valid @ModelAttribute("enhanced") Enhanced enhanced, BindingResult result, HttpServletRequest request, Model model) {
        if (result.hasErrors()) {

            message = "There was a problem with the information you entered";
            model.addAttribute("message", message);
            return "EnhancedScreen";
        }

        String enhancedIdParameter = request.getParameter("organizationComponent.organizationName");
        Organization organizaitonToSet = new Organization();
        organizaitonToSet.setOrganizationName(enhancedIdParameter);
        Enhanced enhancedWithSet = new Enhanced();
        enhancedWithSet.setOrganizationComponent(organizaitonToSet);
        List<Enhanced> allEnhancedInDBO = enhancedService.getAllEnhancedInOrg(enhancedWithSet);
        Enhanced enhancedBlank = new Enhanced();
        message = "The information you requested is displayed below";
        model.addAttribute("message", message);
        model.addAttribute("allEnhancedInDBO", allEnhancedInDBO);
        model.addAttribute("enhanced", enhancedBlank);

        return "EnhancedScreen";

    }

    @RequestMapping(value = "/AllEnhancedInOrganizaitonList", method = RequestMethod.GET)
    public String AllEnhancedInOrganizaitonList(@ModelAttribute("enhanced") Enhanced enhanced, BindingResult result, HttpServletRequest request, Model model) {
        String enhancedIdParameter = request.getParameter("organizationComponent.organizationName");
        Organization organizaitonToSet = new Organization();
        organizaitonToSet.setOrganizationName(enhancedIdParameter);
        Enhanced enhancedWithSet = new Enhanced();
        enhancedWithSet.setOrganizationComponent(organizaitonToSet);
        List<Enhanced> allEnhancedInDBO = enhancedService.getAllEnhancedInOrg(enhancedWithSet);
        Enhanced enhancedBlank = new Enhanced();
        message = "The information you requested is displayed below";
        model.addAttribute("message", message);
        model.addAttribute("allEnhancedInDBO", allEnhancedInDBO);
        model.addAttribute("enhanced", enhancedBlank);

        return "EnhancedScreen";

    }

    @RequestMapping(value = "/AllOrganizationEnhancedBelongsToForm", method = RequestMethod.GET)
    public String AllOrganizationEnhancedBelongsToForm(@Valid @ModelAttribute("enhanced") Enhanced enhanced, BindingResult result, HttpServletRequest request, Model model) {

        if (result.hasErrors()) {

            message = "There was a problem with the information you entered";
            model.addAttribute("message", message);
            return "EnhancedScreen";
        }
        String enhancedIdParameter = request.getParameter("identifier");
        int enhancedId = Integer.parseInt(enhancedIdParameter);
        Enhanced enhancedToService = enhancedService.getEnhancedId(enhancedId);
        List<Organization> allOrganizationsInDBO = enhancedService.getAllOrganizationsEnhancedIsIn(enhancedToService);

        message = "The information you requested is displayed below";
        model.addAttribute("message", message);

        Organization organizationBlank = new Organization();
        model.addAttribute("allOrganizationsInDBO", allOrganizationsInDBO);
        model.addAttribute("organization", organizationBlank);

        return "OrganizationScreen";
    }

    @RequestMapping(value = "/AllOrganizationEnhancedBelongsToList", method = RequestMethod.GET)
    public String AllOrganizationEnhancedBelongsToList(@ModelAttribute("enhanced") Enhanced enhanced, BindingResult result, HttpServletRequest request, Model model) {


        String enhancedIdParameter = request.getParameter("identifier");
        int enhancedId = Integer.parseInt(enhancedIdParameter);
        Enhanced enhancedToService = enhancedService.getEnhancedId(enhancedId);
        List<Organization> allOrganizationsInDBO = enhancedService.getAllOrganizationsEnhancedIsIn(enhancedToService);

        message = "The information you requested is displayed below";
        model.addAttribute("message", message);

        Organization organizationBlank = new Organization();
        model.addAttribute("allOrganizationsInDBO", allOrganizationsInDBO);
        model.addAttribute("organization", organizationBlank);

        return "OrganizationScreen";
    }
}
