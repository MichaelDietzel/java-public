/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.service;

import com.mycompany.superherosighting.dao.SuperHeroEnhancedDao;
import com.mycompany.superherosighting.dao.SuperHeroLocationDao;
import com.mycompany.superherosighting.dao.SuperHeroOrganizaitonDao;
import com.mycompany.superherosighting.dao.SuperHeroSightingsDao;
import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.Location;
import com.mycompany.superherosighting.model.Organization;
import com.mycompany.superherosighting.model.Sightings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;
import static org.hibernate.validator.internal.util.CollectionHelper.newArrayList;

/**
 *
 * @author Mike
 */
public class SuperHeroServiceSightingsImpl implements SuperHeroServiceSightingsLayer {

    @Inject
    private SuperHeroSightingsDao sightingsDao;
    @Inject
    private SuperHeroLocationDao locationDao;
    @Inject
    private SuperHeroEnhancedDao enhancedDao;

    @Override
    public List<Location> sightingsAtAnyLocation(Sightings sightings) {
        List<Location> locationsSeen = new ArrayList();

        List<Sightings> sightingsForLoop = sightingsDao.getAllSightingsByEnhanced(sightings.getEnahncedComponent().getIdentifier());
        for (Sightings thisSighting : sightingsForLoop) {
            Location locationToadd = locationDao.getLocationIncidentId(thisSighting.getLocationComponet().getLocationID());
            locationsSeen.add(locationToadd);
        }
        return locationsSeen;
    }

    @Override
    public List<Sightings> get10Sightings() {
        List<Sightings> allSightings = newArrayList();
        try {
            allSightings = sightingsDao.getAllSightings();
        } catch (NullPointerException ex) {
            Sightings newSighting = new Sightings();
            newSighting.setDateStamp("");
            newSighting.setEnhancedSighted("No sighitngs found");
            newSighting.getEnahncedComponent().setIdentifier(0);
            newSighting.setIncidentId(0);
            newSighting.getLocationComponet().setLocationID(0);
            allSightings.add(newSighting);
            return allSightings;
        }

        List<Sightings> tenSightings = new ArrayList();
        int countOfSightings = allSightings.size();

        for (Sightings sighting : allSightings) {
            countOfSightings = countOfSightings - 1;
            tenSightings.add(allSightings.get(countOfSightings));
            if (tenSightings.size() >= 10) {
                break;
            }
        }
        return tenSightings;
    }

    @Override
    public List<Sightings> allSightingsDetailsOnDate(Sightings sightings) {
        List<Sightings> allSightingsDetailOnDateForController = sightingsDao.getSightingEnhancedLocationbyDate(sightings.getDateStamp());
        for (Sightings thisSighting : allSightingsDetailOnDateForController) {
            thisSighting.setEnahncedComponent(enhancedDao.getEnhancedId(thisSighting.getEnahncedComponent().getIdentifier()));
            thisSighting.setLocationComponet(locationDao.getLocationIncidentId(thisSighting.getLocationComponet().getLocationID()));
        }
        return allSightingsDetailOnDateForController;
    }

    @Override
    public List<Sightings> getAllSightings() {
        List<Sightings> allSightings = sightingsDao.getAllSightings();
        return allSightings;
    }

    @Override
    public void addSightings(Sightings sighting) {
        sightingsDao.addSightings(sighting);
    }

    @Override
    public Sightings getSightingsById(int sightingsId) {
        Sightings sightings = sightingsDao.getSightingsById(sightingsId);
        return sightings;
    }

    @Override
    public void deleteSightings(int sightingsId) {
        sightingsDao.deleteSightings(sightingsId);
    }

    @Override
    public void updateSightings(Sightings sightings) {
        sightingsDao.updateSightings(sightings);
    }

    @Override
    public List<Enhanced> allEnhancedAtLocation(Sightings sighting) {
        List<Enhanced> enhancedToDisplay = new ArrayList();
        List<Sightings> listToLoop = sightingsDao.getAllSightingsByLocation(sighting.getLocationComponet().getLocationID());
        for (Sightings thisSighting : listToLoop) {
            Enhanced enhancedForList = enhancedDao.getEnhancedId(thisSighting.getEnahncedComponent().getIdentifier());
            enhancedToDisplay.add(enhancedForList);
        }
        enhancedToDisplay.removeAll(Collections.singleton(null));
        return enhancedToDisplay;
    }

}
