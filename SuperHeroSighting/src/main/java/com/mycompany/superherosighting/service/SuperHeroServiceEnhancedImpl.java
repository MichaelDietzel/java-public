/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.service;

import com.mycompany.superherosighting.dao.SuperHeroEnhancedDao;
import com.mycompany.superherosighting.dao.SuperHeroOrganizaitonDao;
import com.mycompany.superherosighting.dao.SuperHeroSightingsDao;
import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.Organization;
import com.mycompany.superherosighting.model.Sightings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import org.springframework.dao.EmptyResultDataAccessException;

/**
 *
 * @author Mike
 */
public class SuperHeroServiceEnhancedImpl implements SuperHeroServiceEnhancedLayer {



    @Inject
    private SuperHeroEnhancedDao enhancedDao;
    @Inject
    private SuperHeroSightingsDao SightingDao;
    @Inject
    private SuperHeroOrganizaitonDao OrganizationDao;

    @Override
    public String deleteEnhancedlogic(Enhanced enhancedToService) {
        int SizeOfRemoval = 0;
        List<Sightings> allSightings = SightingDao.getAllSightings();
        for (Sightings thisSighting : allSightings) {
            if (thisSighting.getEnahncedComponent().getIdentifier() == enhancedToService.getIdentifier()) {
                SightingDao.deleteSightings(enhancedToService.getIdentifier());
                SizeOfRemoval = SizeOfRemoval++;
            }
        }
        enhancedDao.deleteEnhanced(enhancedToService.getIdentifier());
        String message = "Enhanced has been removed from the database along with " + SizeOfRemoval + " Coresponding Sightings";
        return message;
    }

    @Override
    public List<Enhanced> getAllEnhanced() {
        List<Enhanced> AllEnhanced = enhancedDao.getAllEnhanced();
        return AllEnhanced;
    }

    @Override
    public void addEnhanced(Enhanced enhancedToService) {
        enhancedDao.addEnhanced(enhancedToService);
    }

    @Override
    public Enhanced getEnhancedId(int enhancedId) {
        Enhanced enhanced = new Enhanced();
        return enhanced = enhancedDao.getEnhancedId(enhancedId);
    }

    @Override
    public void updateEnhaned(Enhanced enhanced) {
        enhancedDao.updateEnhanced(enhanced);
    }

    @Override
    public List<Enhanced> getAllEnhancedInOrg(Enhanced enhanced) {

        List<Enhanced> allEnhancedInOrg = enhancedDao.getAllEnhancedInOrganizaiton(enhanced.getOrganizationComponent().getOrganizationName());
        return allEnhancedInOrg;
    }

    @Override
    public List<Organization> getAllOrganizationsEnhancedIsIn(Enhanced enhanced) {
        List<Enhanced> allEnhancedInOrg = enhancedDao.getAllEnhanced();
        List<Organization> allOrganizaitonsEnhancedBelongsTo = new ArrayList();
        try {

            for (Enhanced thisEnhanced : allEnhancedInOrg) {
                if (thisEnhanced.getOrganizationComponent().getOrganizationName().equals(enhanced.getOrganizationComponent().getOrganizationName())) {
                   
                    Organization organizaiton = OrganizationDao.getOrganizaiotnbyName(enhanced.getOrganizationComponent().getOrganizationName());
                    allOrganizaitonsEnhancedBelongsTo.add(organizaiton);
                }
            }
        } catch (EmptyResultDataAccessException ex) {
            return allOrganizaitonsEnhancedBelongsTo;
        }
        allOrganizaitonsEnhancedBelongsTo.removeAll(Collections.singleton(null));
        return allOrganizaitonsEnhancedBelongsTo;

    }
}
