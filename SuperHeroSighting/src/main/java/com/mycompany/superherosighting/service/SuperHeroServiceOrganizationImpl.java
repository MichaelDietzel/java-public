/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.service;

import com.mycompany.superherosighting.dao.SuperHeroEnhancedDao;
import com.mycompany.superherosighting.dao.SuperHeroLocationDao;
import com.mycompany.superherosighting.dao.SuperHeroOrganizaitonDao;
import com.mycompany.superherosighting.dao.SuperHeroSightingsDao;
import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.Organization;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author Mike
 */
public class SuperHeroServiceOrganizationImpl implements SuperHeroServiceOrganizationLayer {

    @Inject
    private SuperHeroOrganizaitonDao organizationDao;

    @Override
    public List<Organization> getAllOrganizations() {
        List<Organization> allOrganizations = organizationDao.getAllOrganization();
        return allOrganizations;
    }

    @Override
    public void deleteOrganization(int organizationId) {
        organizationDao.deleteOrganization(organizationId);
    }

    @Override
    public Organization getOrganizationId(int organizationId) {
        Organization organization = organizationDao.getOrganizationId(organizationId);
        return organization;
    }

    @Override
    public void addOrganization(Organization organization) {
        organizationDao.addOrganization(organization);
    }

    @Override
    public void updateOrganization(Organization organization) {
        organizationDao.updateOrganization(organization);
    }
    /**
     * no implementations need at this time saved to maintain wiring incase
     * future changes requested*
     */

}
