/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.service;

import com.mycompany.superherosighting.model.Location;
import com.mycompany.superherosighting.model.Sightings;
import java.util.List;

/**
 *
 * @author Mike
 */
public interface SuperHeroServiceLocationLayer {

    public List<Sightings> getAllSightingsAtLocation(int locationID);

    public List<Location> getAllLocations();

    public void addLocation(Location location);

    public Location getLocationIncidentId(int locationId);

    public void deleteLocation(int organizationId);

    public void updateLocatation(Location location);
/** no implementations need at this time saved to maintain wiring incase future changes requested**/
 
}
