/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.service;

import com.mycompany.superherosighting.dao.SuperHeroEnhancedDao;
import com.mycompany.superherosighting.dao.SuperHeroLocationDao;
import com.mycompany.superherosighting.dao.SuperHeroOrganizaitonDao;
import com.mycompany.superherosighting.dao.SuperHeroSightingsDao;
import com.mycompany.superherosighting.model.Location;
import com.mycompany.superherosighting.model.Sightings;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author Mike
 */
public class SuperHeroServiceLocationImpl implements SuperHeroServiceLocationLayer {


    @Inject
    private SuperHeroSightingsDao sightingsDao;
     @Inject
    private SuperHeroLocationDao locationDao;

    @Override
    public List<Sightings> getAllSightingsAtLocation(int locationID) {
        List<Sightings> AllSightingsAtLocation = sightingsDao.getAllSightingsByLocation(locationID);
        return AllSightingsAtLocation;
    }

    @Override
    public List<Location> getAllLocations() {
     List<Location> allLocations =  locationDao.getAllLocations();
     return allLocations;
    }

    @Override
    public void addLocation(Location location) {
       locationDao.addLocation(location);
    }

    @Override
    public Location getLocationIncidentId(int locationId) {
       Location location = locationDao.getLocationIncidentId(locationId);
       return location;
    }

    @Override
    public void deleteLocation(int locationId) {
      locationDao.deleteLocation(locationId);
    }

    @Override
    public void updateLocatation(Location location) {
        locationDao.updateLocation(location);
    }

}
