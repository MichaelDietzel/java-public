/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.service;

import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.Organization;
import java.util.List;

/**
 *
 * @author Mike
 */
public interface SuperHeroServiceEnhancedLayer {

    public String deleteEnhancedlogic(Enhanced enhanced);

    public List<Enhanced> getAllEnhanced();

    public void addEnhanced(Enhanced enhancedToService);

    public Enhanced getEnhancedId (int enhancedId);

    public void updateEnhaned(Enhanced enhanced);

    public List<Enhanced> getAllEnhancedInOrg(Enhanced enhanced);

    public List<Organization> getAllOrganizationsEnhancedIsIn(Enhanced enhanced);

    
}
