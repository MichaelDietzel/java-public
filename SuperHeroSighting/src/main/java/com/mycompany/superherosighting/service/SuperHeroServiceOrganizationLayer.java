/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.service;

import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.Organization;
import java.util.List;

/**
 *
 * @author Mike
 */
public interface SuperHeroServiceOrganizationLayer {

    public List<Organization> getAllOrganizations();

    public void deleteOrganization(int organizationId);

    public Organization getOrganizationId(int organizationId);

    public void addOrganization(Organization organization);

    public void updateOrganization(Organization organization);



}
