/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.service;

import com.mycompany.superherosighting.model.User;
import java.util.List;

/**
 *
 * @author Mike
 */
public interface AdminServiceLayer {

    public List<User> getAllUsers();

    public void addUser(User newUser);

    public void deleteUser(String username);

    public User getUserToUpdate(String userNameToLookUp);

    public void updateUser(User user);

    public void disableEnableUser(User user);
    
}
