/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.service;

import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.Location;
import com.mycompany.superherosighting.model.Sightings;
import java.util.List;

/**
 *
 * @author Mike
 */
public interface SuperHeroServiceSightingsLayer {


    public List<Sightings> get10Sightings();

    public List<Location> sightingsAtAnyLocation(Sightings sightings);

    public List<Sightings> allSightingsDetailsOnDate(Sightings sightings);

    public List<Sightings> getAllSightings();

    public void addSightings(Sightings sighting);

    public Sightings getSightingsById(int sightingsId);

    public void deleteSightings(int sightingsId);

    public void updateSightings(Sightings sightings);

   public List<Enhanced> allEnhancedAtLocation(Sightings sighting);

   
}
