/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting.service;

import com.mycompany.superherosighting.dao.AdminDao;
import com.mycompany.superherosighting.dao.SuperHeroEnhancedDao;
import com.mycompany.superherosighting.model.User;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author Mike
 */
public class AdminServiceImpl implements AdminServiceLayer {

    @Inject
    private AdminDao adminDao;

    @Override
    public List<User> getAllUsers() {
            
        List<User> allUsers = adminDao.getAllUsers();
        
        return allUsers;
    }

    @Override
    public void addUser(User newUser) {
       adminDao.addUser(newUser);
    }

    @Override
    public void deleteUser(String username) {
      adminDao.deleteUser(username);
    }

    @Override
    public User getUserToUpdate(String userNameToLookUp) {
       List<User> allUser = adminDao.getAllUsers();
        User userToRetun = new User();
               for(User thisUser:allUser ){
                   if (thisUser.getUsername().equals(userNameToLookUp)){
                       userToRetun = thisUser;
                   }
               }
               return userToRetun;
    }

    @Override
    public void updateUser(User user) {
       adminDao.updateUser(user);
    }

    @Override
    public void disableEnableUser(User user) {
       adminDao.DisableUser(user);
    }

}
