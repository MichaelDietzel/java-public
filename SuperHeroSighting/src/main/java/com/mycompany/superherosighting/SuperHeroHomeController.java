package com.mycompany.superherosighting;

import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.Location;
import com.mycompany.superherosighting.model.Organization;
import com.mycompany.superherosighting.model.Sightings;
import com.mycompany.superherosighting.service.SuperHeroServiceEnhancedLayer;
import com.mycompany.superherosighting.service.SuperHeroServiceLocationLayer;
import com.mycompany.superherosighting.service.SuperHeroServiceOrganizationLayer;
import com.mycompany.superherosighting.service.SuperHeroServiceSightingsLayer;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SuperHeroHomeController {

    @Inject
    private SuperHeroServiceLocationLayer locationService;
    @Inject
    private SuperHeroServiceEnhancedLayer enhancedService;
    @Inject
    private SuperHeroServiceOrganizationLayer organizationService;
    @Inject
    private SuperHeroServiceSightingsLayer sightingsService;

    public SuperHeroHomeController() {
    }
    String jspPage = "index";
    String message = "The program is ready for use";
    Enhanced enhancedForDisplay = new Enhanced();


    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String displayAllItems(HttpServletRequest request, Model model) {

        Enhanced enhanced = new Enhanced();
        Sightings sightings = new Sightings();
        Organization organization = new Organization();
        Location location = new Location();

        model.addAttribute("sightings", sightings);
        model.addAttribute("enhanced", enhanced);
        model.addAttribute("organization", organization);
        model.addAttribute("location", location);

        List<Sightings> tenSightings = sightingsService.get10Sightings();
        model.addAttribute("tenSightings", tenSightings);

        return jspPage;
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String displayAdminPage() {
        return "admin";
    }

    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String login(HttpServletRequest request, Model model) {

        return "login";
    }

    @RequestMapping(value = {"/index"}, method = RequestMethod.GET)
    public String gotoIndexScreen(HttpServletRequest request, Model model) {
        List<Sightings> tenSightings = sightingsService.get10Sightings();
        model.addAttribute("tenSightings", tenSightings);
        return "index";
    }

    @RequestMapping(value = {"/EnhancedScreen"}, method = RequestMethod.GET)
    public String gotoEnhancedScreen(HttpServletRequest request, Model model) {
        Enhanced enhanced = new Enhanced();
        model.addAttribute("enhanced", enhanced);
        return "EnhancedScreen";
    }

    @RequestMapping(value = {"/LocationScreen"}, method = RequestMethod.GET)
    public String gotoLocationScreen(HttpServletRequest request, Model model) {
        Location location = new Location();
        model.addAttribute("location", location);
        return "LocationScreen";
    }

    @RequestMapping(value = {"/OrganizationScreen"}, method = RequestMethod.GET)
    public String gotoOrganizationScreen(HttpServletRequest request, Model model) {
        Organization organization = new Organization();
        model.addAttribute("organization", organization);
        return "OrganizationScreen";
    }

    @RequestMapping(value = {"/SightingsScreen"}, method = RequestMethod.GET)
    public String gotoSightingsScreen(HttpServletRequest request, Model model) {
        Sightings sightings = new Sightings();
        model.addAttribute("sightings", sightings);
        return "SightingsScreen";
    }

    @RequestMapping(value = {"/AdminScreen"}, method = RequestMethod.GET)
    public String gotoAdminScreen(HttpServletRequest request, Model model) {
        Sightings sightings = new Sightings();
        model.addAttribute("sightings", sightings);
        return "AdminScreen";
    }

    @RequestMapping(value = "/back", method = RequestMethod.POST)
    public String back(HttpServletRequest request, Model model) {
        return "redirect:/index";
    }

}
