/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting;

import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.Location;
import com.mycompany.superherosighting.model.Organization;
import com.mycompany.superherosighting.model.Sightings;
import com.mycompany.superherosighting.service.SuperHeroServiceEnhancedLayer;
import com.mycompany.superherosighting.service.SuperHeroServiceLocationLayer;
import com.mycompany.superherosighting.service.SuperHeroServiceOrganizationLayer;
import com.mycompany.superherosighting.service.SuperHeroServiceSightingsLayer;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Mike
 */
@Controller
public class SightingController {

    @Inject
    private SuperHeroServiceSightingsLayer sightingsService;

    public SightingController() {
    }

    String jspPage = "index";
    String message = "The program is ready for use";
    Enhanced enhancedForDisplay = new Enhanced();

    Organization organizationForDispaly = new Organization();
    Sightings sightingsForDisplay = new Sightings();
    Location locationForDisplay = new Location();

    List<Sightings> allSightingsAtLocation = new ArrayList();
    List<Enhanced> allEnhancedInOrg = new ArrayList();
    List<Sightings> SightingLocations = new ArrayList();

    @RequestMapping(value = "/dboGetallSighting", method = RequestMethod.POST)
    public String dboGetallSightings(HttpServletRequest request, Model model) {
        List<Sightings> allSightingsInDBO = sightingsService.getAllSightings();
        Sightings sightings = new Sightings();
        model.addAttribute("sightings", sightings);
        model.addAttribute("allSightingsInDBO", allSightingsInDBO);
        return "SightingsScreen";

    }

    @RequestMapping(value = "/addSightings", method = RequestMethod.POST)
    public String SightingHandler(@Valid @ModelAttribute("sightings") Sightings sighting, BindingResult result, HttpServletRequest request, Model model) {
        Sightings sightingsBlank = new Sightings();

        if (result.hasErrors()) {
            message = "error in your Sightings input information";
            model.addAttribute("message", message);
            return "SightingsScreen";
        }
        sightingsService.addSightings(sighting);
        message = "Your Item has been added to the database";
        model.addAttribute("message", message);
        model.addAttribute("sightings", sightingsBlank);
        return "SightingsScreen";

    }

    @RequestMapping(value = "/displaySightingsForm", method = RequestMethod.GET)
    public String displaySightingsForm(@Valid HttpServletRequest request, Model model) {
        String sightingsIdParameter = request.getParameter("identifier");
        int sightingsId = Integer.parseInt(sightingsIdParameter);
        Sightings sightings = sightingsService.getSightingsById(sightingsId);
        model.addAttribute("sightings", sightings);
        message = "Your Item is Displayed in the input boxes";
        model.addAttribute("message", message);
        return "SightingsScreen";
    }

    @RequestMapping(value = "/DeleteSightingsForm", method = RequestMethod.GET)
    public String deleteLocation(@Valid @ModelAttribute("sightings") Sightings sightings, BindingResult result, HttpServletRequest request, Model model) {
        String sightingsIdParameter = request.getParameter("identifier");
        int sightingsId = Integer.parseInt(sightingsIdParameter);
        sightingsService.deleteSightings(sightingsId);;
        model.addAttribute("sightings", sightings);
        message = "Your Sighting has been removed from the Sightings Form";
        model.addAttribute("message", message);
        return "SightingsScreen";
    }

    @RequestMapping(value = "/UpdateSightingsForm", method = RequestMethod.GET)
    public String UpdateSightingsForm(HttpServletRequest request, Model model) {
        String sightingsIdParameter = request.getParameter("identifier");
        int sightingId = Integer.parseInt(sightingsIdParameter);
        Sightings sightings = sightingsService.getSightingsById(sightingId);
        model.addAttribute("sightings", sightings);
        return "UpdateSightings";
    }

    @RequestMapping(value = "/updatedSightings", method = RequestMethod.GET)
    public String updatedSightings(@Valid @ModelAttribute("sightings") Sightings sightings, BindingResult result, HttpServletRequest request, Model model) {
        Sightings sightingsBlank = new Sightings();
        if (result.hasErrors()) {
            message = "There was a problem with the information you entered";
            model.addAttribute("message", message);
            return "UpdateSightings";
        }
        sightingsService.updateSightings(sightings);
        message = "Your Organizaiton has been updated";
        model.addAttribute("message", message);
        model.addAttribute("sightings", sightingsBlank);
        return "SightingsScreen";
    }

    @RequestMapping(value = "/AllLocationWhereSuperheroSeenForm", method = RequestMethod.GET)
    public String AllLocationWhereSuperheroSeenForm(@Valid @ModelAttribute("sightings") Sightings sightings, BindingResult result, HttpServletRequest request, Model model) {
        if (result.hasErrors()) {
            message = "There was a problem with the information you entered";
            return "SightingsScreen";
        }
        String sightingEnahncedIdParameter = request.getParameter("enahncedComponent.identifier");
        int sightingId = Integer.parseInt(sightingEnahncedIdParameter);
        Sightings sighting = new Sightings();
        Enhanced enhancedToSet = new Enhanced();
        enhancedToSet.setIdentifier(sightingId);
        sighting.setEnahncedComponent(enhancedToSet);
        List<Location> allLocationsInDBO = sightingsService.sightingsAtAnyLocation(sighting);
        Location locationBlankNow = new Location();
        message = "The information you requested is displayed below";
        model.addAttribute("message", message);
        model.addAttribute("allLocationsInDBO", allLocationsInDBO);
        model.addAttribute("location", locationBlankNow);
        return "LocationScreen";

    }

    @RequestMapping(value = "/AllLocationWhereSuperheroSeenList", method = RequestMethod.GET)
    public String AllLocationWhereSuperheroSeenList(@Valid @ModelAttribute("sightings") Sightings sightings, BindingResult result, HttpServletRequest request, Model model) {

        String sightingEnahncedIdParameter = request.getParameter("enahncedComponent.identifier");
        int sightingId = Integer.parseInt(sightingEnahncedIdParameter);
        Sightings sighting = new Sightings();
        Enhanced enhancedToSet = new Enhanced();
        enhancedToSet.setIdentifier(sightingId);
        sighting.setEnahncedComponent(enhancedToSet);
        List<Location> allLocationsInDBO = sightingsService.sightingsAtAnyLocation(sighting);
        Location locationBlankNow = new Location();
        message = "The information you requested is displayed below";
        model.addAttribute("message", message);
        model.addAttribute("allLocationsInDBO", allLocationsInDBO);
        model.addAttribute("location", locationBlankNow);
        return "LocationScreen";

    }

    @RequestMapping(value = "/AllSighitngsOnDateForm", method = RequestMethod.GET)
    public String AllSighitngsOnDateForm(@Valid @ModelAttribute("Sightings") Sightings sightings, BindingResult result, HttpServletRequest request, Model model) {
        List<Sightings> allSightingsOnDate = new ArrayList();
        if (result.hasErrors()) {

            message = "There was a problem with the information you entered";
            model.addAttribute("message", message);
            model.addAttribute("sightings", sightings);
            return "SpecialQueryDisplay";
        }
        String sightingsIddateStamp = request.getParameter("dateStamp");
        Sightings sightingOnDate = new Sightings();
        sightingOnDate.setDateStamp(sightingsIddateStamp);
        allSightingsOnDate = sightingsService.allSightingsDetailsOnDate(sightingOnDate);
        message = "The Sightings you requested are at the bottom of the page.";
        model.addAttribute("message", message);
        Sightings sightingsBlankNow = new Sightings();
        model.addAttribute("allLocationSightings", allSightingsOnDate);
        model.addAttribute("sightings", sightingsBlankNow);
        return "SightingsScreen";
    }

    @RequestMapping(value = "/AllSighitngsOnDateList", method = RequestMethod.GET)
    public String AllSighitngsOnDateList(@Valid @ModelAttribute("Sightings") Sightings sightings, BindingResult result, HttpServletRequest request, Model model) {
        List<Sightings> allSightingsOnDate = new ArrayList();
        String sightingsIddateStamp = request.getParameter("dateStamp");
        Sightings sightingOnDate = new Sightings();
        sightingOnDate.setDateStamp(sightingsIddateStamp);
        allSightingsOnDate = sightingsService.allSightingsDetailsOnDate(sightingOnDate);
        message = "The Sightings you requested are at the bottom of the page.";
        model.addAttribute("message", message);
        Sightings sightingsBlankNow = new Sightings();
        model.addAttribute("allLocationSightings", allSightingsOnDate);
        model.addAttribute("sightings", sightingsBlankNow);
        return "SightingsScreen";
    }

    @RequestMapping(value = "/AllEnhancedSeenAtLocationForm", method = RequestMethod.GET)
    public String AllEnhancedSeenAtLocationForm(@Valid @ModelAttribute("sightings") Sightings sightings, BindingResult result, HttpServletRequest request, Model model) {

        if (result.hasErrors()) {

            message = "There was a problem with the information you entered";
            model.addAttribute("message", message);
            return "SpecialQueryDisplay";
        }

        String locationEnahncedIdParameter = request.getParameter("locationComponet.locationID");
        int locationId = Integer.parseInt(locationEnahncedIdParameter);
        Sightings sighting = new Sightings();
        Location locationToSet = new Location();
        locationToSet.setLocationID(locationId);
        sighting.setLocationComponet(locationToSet);
        List<Enhanced> allEnhancedInDBO = sightingsService.allEnhancedAtLocation(sighting);

        Enhanced enhancedBlank = new Enhanced();
        message = "The information you requested is displayed below";
        model.addAttribute("message", message);
        model.addAttribute("allEnhancedInDBO", allEnhancedInDBO);
        model.addAttribute("enhanced", enhancedBlank);
        return "EnhancedScreen";
    }

    @RequestMapping(value = "/AllEnhancedSeenAtLocationList", method = RequestMethod.GET)
    public String AllEnhancedSeenAtLocationList(@Valid @ModelAttribute("sightings") Sightings sightings, BindingResult result, HttpServletRequest request, Model model) {

        String locationEnahncedIdParameter = request.getParameter("locationComponet.locationID");
        int locationId = Integer.parseInt(locationEnahncedIdParameter);
        Sightings sighting = new Sightings();
        Location locationToSet = new Location();
        locationToSet.setLocationID(locationId);
        sighting.setLocationComponet(locationToSet);
        List<Enhanced> allEnhancedInDBO = sightingsService.allEnhancedAtLocation(sighting);

        Enhanced enhancedBlank = new Enhanced();
        message = "The information you requested is displayed below";
        model.addAttribute("message", message);
        model.addAttribute("allEnhancedInDBO", allEnhancedInDBO);
        model.addAttribute("enhanced", enhancedBlank);
        return "EnhancedScreen";
    }
}
