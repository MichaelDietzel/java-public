/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting;

import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.Location;
import com.mycompany.superherosighting.model.Organization;
import com.mycompany.superherosighting.model.Sightings;
import com.mycompany.superherosighting.service.SuperHeroServiceEnhancedLayer;
import com.mycompany.superherosighting.service.SuperHeroServiceLocationLayer;
import com.mycompany.superherosighting.service.SuperHeroServiceOrganizationLayer;
import com.mycompany.superherosighting.service.SuperHeroServiceSightingsLayer;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Mike
 */
@Controller
public class LocationController {

    @Inject
    private SuperHeroServiceLocationLayer locationService;
    @Inject
    private SuperHeroServiceEnhancedLayer enhancedService;
    @Inject
    private SuperHeroServiceOrganizationLayer organizationService;
    @Inject
    private SuperHeroServiceSightingsLayer sightingsService;

    
    public LocationController() {
    }

    String message = "The program is ready for use";
    Enhanced enhancedForDisplay = new Enhanced();

    @RequestMapping(value = "/dboGetallLocation", method = RequestMethod.POST)
    public String dboGetallLocation(HttpServletRequest request, Model model) {
        List<Location> allLocationsInDBO = locationService.getAllLocations();
        Location location = new Location();
        model.addAttribute("location", location);
        model.addAttribute("allLocationsInDBO", allLocationsInDBO);
        return "LocationScreen";

    }


    @RequestMapping(value = "/addLocation", method = RequestMethod.POST)
    public String LocationHandler(@Valid @ModelAttribute("location") Location location, BindingResult result, HttpServletRequest request, Model model) {
        Location locationBlank = new Location();
        if (result.hasErrors()) {

            message = "error in your Location input information";
            model.addAttribute("message", message);
            return "LocationScreen";
        }

        locationService.addLocation(location);
        message = "Your Item has been added to the database";
        model.addAttribute("message", message);
        model.addAttribute("location", locationBlank);

        return "LocationScreen";
    }

    @RequestMapping(value = "/displayLocationForm", method = RequestMethod.GET)
    public String displayLocationForm(@Valid HttpServletRequest request, Model model) {
        String locationIdParameter = request.getParameter("identifier");
        int locationId = Integer.parseInt(locationIdParameter);

        Location location = locationService.getLocationIncidentId(locationId);
        model.addAttribute("location", location);
        message = "Your Locaiton is visible in the input boxes below";
        model.addAttribute("message", message);
        return "LocationScreen";
    }

    @RequestMapping(value = "/DeleteLocationForm", method = RequestMethod.GET)
    public String deleteLocation(@Valid @ModelAttribute("location") Location location, BindingResult result, HttpServletRequest request, Model model) {
        String locationIdParameter = request.getParameter("identifier");
        int locationId = Integer.parseInt(locationIdParameter);
        locationService.deleteLocation(locationId);
        model.addAttribute("location", location);

        return "LocationScreen";
    }

    @RequestMapping(value = "/UpdateLocationForm", method = RequestMethod.GET)
    public String UpdateLocationForm(HttpServletRequest request, Model model) {
        String locationIdParameter = request.getParameter("identifier");
        int locationId = Integer.parseInt(locationIdParameter);
        Location location = locationService.getLocationIncidentId(locationId);

        model.addAttribute("location", location);
        return "UpdateLocation";
    }

    @RequestMapping(value = "/updatedLocation", method = RequestMethod.GET)
    public String updatedLocation(@Valid @ModelAttribute("location") Location location, BindingResult result, HttpServletRequest request, Model model) {

        Location locationBlank = new Location();

        if (result.hasErrors()) {
            return "UpdateLocation";
        }
        locationService.updateLocatation(location);

        message = "Your Organizaiton has been updated";
        model.addAttribute("message", message);
        model.addAttribute("location", locationBlank);

        return "LocationScreen";
    }
}
