/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting;

import com.mycompany.superherosighting.dao.AdminDao;
import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.User;
import com.mycompany.superherosighting.service.AdminServiceLayer;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdminController {

    private AdminDao dao;
    private PasswordEncoder encoder;
    private String nameTolookUp;

    @Inject
    public AdminController(AdminDao dao, PasswordEncoder encoder) {
        this.dao = dao;
        this.encoder = encoder;
    }
    @Inject
    private AdminServiceLayer adminService;

    @RequestMapping(value = "/dboGetallAdmin", method = RequestMethod.POST)
    public String displayUserList(HttpServletRequest request, Model model) {
        List <User>allUsersInDbo = adminService.getAllUsers();
        model.addAttribute("allUsersInDbo", allUsersInDbo);
        return "AdminScreen";
    }

    @RequestMapping(value = "/displayUserForm", method = RequestMethod.GET)
    public String displayUserForm(Map<String, Object> model) {
        return "AdminScreen";
    }

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public String addUser(HttpServletRequest req) {
        User newUser = new User();

        newUser.setUsername(req.getParameter("username"));
        String clearPw = req.getParameter("password");
        String hashPw = encoder.encode(clearPw);
        newUser.setPassword(req.getParameter("password"));

        newUser.addAuthority("ROLE_SIDEKICK");
        if (null != req.getParameter("isAdmin")) {
            newUser.addAuthority("ROLE_ADMIN");
        }

        adminService.addUser(newUser);

        return "AdminScreen";
    }

    @RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
    public String deleteUser(@RequestParam("username") String username,
            Map<String, Object> model) {
        adminService.deleteUser(username);
        return "AdminScreen";
    }

    @RequestMapping(value = "/updateUser", method = RequestMethod.GET)
    public String updateUser(HttpServletRequest request, Model model) {
        User user = new User();
        nameTolookUp = request.getParameter("username");
        user = adminService.getUserToUpdate(nameTolookUp);
        model.addAttribute("user", user);
        return "UpdateAdmin";
    }

    @RequestMapping(value = "/updatedUser", method = RequestMethod.GET)
    public String updatedUser(@Valid @ModelAttribute("user") User user, BindingResult result, HttpServletRequest request, Model model) {
        User userBlank = new User();
        if (result.hasErrors()) {
            return "UpdateAdmin";
        }

        model.addAttribute("user", userBlank);
        String clearPw = user.getPassword();
        String hashPw = encoder.encode(clearPw);
        user.setPassword(hashPw);
        user.addAuthority("ROLE_SIDEKICK");
        if (null != request.getParameter("isAdmin")) {
            user.addAuthority("ROLE_ADMIN");
        }

        adminService.updateUser(user);

        return "AdminScreen";
    }

    @RequestMapping(value = "/disableUser", method = RequestMethod.GET)
    public String disableUser(HttpServletRequest request, Model model) {
        User user = new User();
        nameTolookUp = request.getParameter("username");
        user = adminService.getUserToUpdate(nameTolookUp);
        user.setEnabled("0");
        adminService.disableEnableUser(user);
        return "AdminScreen";
    }

    @RequestMapping(value = "/EnableUser", method = RequestMethod.GET)
    public String EnableUser(HttpServletRequest request, Model model) {
        User user = new User();
        nameTolookUp = request.getParameter("username");
        user = adminService.getUserToUpdate(nameTolookUp);
        user.setEnabled("1");
        adminService.disableEnableUser(user);
        return "AdminScreen";
    }

}
