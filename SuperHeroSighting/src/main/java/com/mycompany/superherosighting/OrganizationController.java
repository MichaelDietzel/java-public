/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superherosighting;

import com.mycompany.superherosighting.model.Enhanced;
import com.mycompany.superherosighting.model.Location;
import com.mycompany.superherosighting.model.Organization;
import com.mycompany.superherosighting.model.Sightings;
import com.mycompany.superherosighting.service.SuperHeroServiceEnhancedLayer;
import com.mycompany.superherosighting.service.SuperHeroServiceLocationLayer;
import com.mycompany.superherosighting.service.SuperHeroServiceOrganizationLayer;
import com.mycompany.superherosighting.service.SuperHeroServiceSightingsLayer;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Mike
 */
@Controller
public class OrganizationController {

    @Inject
    private SuperHeroServiceOrganizationLayer organizationService;


    public OrganizationController() {

    }
 
    String message = "The program is ready for use";


    @RequestMapping(value = "/dboGetallOrganization", method = RequestMethod.POST)
    public String dboGetallOrganization(HttpServletRequest request, Model model) {
        List<Organization> allOrganizationsInDBO = organizationService.getAllOrganizations();
        Organization organization = new Organization();
        model.addAttribute("organization", organization);
        model.addAttribute("allOrganizationsInDBO", allOrganizationsInDBO);
        return "OrganizationScreen";

    }

    @RequestMapping(value = "/addOrganization", method = RequestMethod.POST)
    public String OrganizationHandler(@Valid @ModelAttribute("organization") Organization organization, BindingResult result, HttpServletRequest request, Model model) {

        Organization organizationBlank = new Organization();

        if (result.hasErrors()) {
            message = "error in your Organization input information";

            return "OrganizationScreen";
        }
        organizationService.addOrganization(organization);
        message = "Your Item has been added to the database";
        model.addAttribute("message", message);
        model.addAttribute("organization", organizationBlank);
        return "OrganizationScreen";
    }

    @RequestMapping(value = "/displayOrganizationForm", method = RequestMethod.GET)
    public String displayOrganizationForm(@Valid HttpServletRequest request, Model model) {
        String organizationIdParameter = request.getParameter("identifier");
        int organizationId = Integer.parseInt(organizationIdParameter);
        Organization organization = organizationService.getOrganizationId(organizationId);
        message = "Your item is displayed in the input box bellow";
        model.addAttribute("organization", organization);
        return "OrganizationScreen";
    }

    @RequestMapping(value = "/DeleteOrganizationForm", method = RequestMethod.GET)
    public String deleteOrganization(@Valid @ModelAttribute("organization") Organization organization, BindingResult result, HttpServletRequest request, Model model) {
        String organizationIdParameter = request.getParameter("identifier");
        int organizationId = Integer.parseInt(organizationIdParameter);
        organizationService.deleteOrganization(organizationId);
        model.addAttribute("organization", organization);
        return "OrganizationScreen";
    }

    @RequestMapping(value = "/UpdateOrganizationForm", method = RequestMethod.GET)
    public String UpdateOrganizationForm(HttpServletRequest request, Model model) {
        String OrganizationIdParameter = request.getParameter("identifier");
        int OrganizationId = Integer.parseInt(OrganizationIdParameter);
        Organization organization = organizationService.getOrganizationId(OrganizationId);

        model.addAttribute("organization", organization);
        return "UpdateOrganization";
    }

    @RequestMapping(value = "/updatedOrganization", method = RequestMethod.GET)
    public String updatedOrganization(@Valid @ModelAttribute("organization") Organization organization, BindingResult result, HttpServletRequest request, Model model) {
        Organization organizationBlank =  new Organization();
        
        if (result.hasErrors()) {
            return "UpdateOrganization";
        }
        organizationService.updateOrganization(organization);
        message = "Your Organizaiton has been updated";
        model.addAttribute("organization", organizationBlank);
        return "OrganizationScreen";
    }
}
