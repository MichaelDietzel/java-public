<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- Directive for Spring Form tag libraries -->
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

    </head>
    <div class="navbar">
        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/index">Home</a></li>
            <li role="presentation"><a href="${pageContext.request.contextPath}/EnhancedScreen">Enhanced Screen</a></li>
            <li role="presentation"><a href="${pageContext.request.contextPath}/LocationScreen">Location Screen</a></li>
            <li role="presentation"><a href="${pageContext.request.contextPath}/SightingsScreen">Sightings Screen</a></li>
            <li role="presentation"><a href="${pageContext.request.contextPath}/OrganizationScreen">Organization Screen</a></li>
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                <li role="presentation"><a href="${pageContext.request.contextPath}/AdminScreen">Admin Screen</a></li>
                </sec:authorize>
        </ul>    
    </div>
    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <p>Hello : ${pageContext.request.userPrincipal.name}
            | <a href="<c:url value="/j_spring_security_logout" />" > Logout</a>
        </p>
    </c:if>
    <div>    
        <h2 text-align="center">${message}</h2>
    </div>

    <form action="dboGetallOrganization" method="POST">
        <button type="submit" 
                name="dboGetallOrganization"  
                value="displayAllSightings" 
                id="" 
                style="text-align: center" 
                class="btn btn-default ">
            Display all Organization 
        </button>
    </form>

    <div align="center" class= "col-md-12">  
        <h3>Organization Form</h3>
        <sf:form class="form-horizontal" role="form" modelAttribute="organization"
                 action="addOrganization" method="POST">

            <div class="form-group">
                <label for="organization-Name" class="col-md-2 control-label">organization Name:</label>
                <div class="col-md-4">
                    <sf:input type="text" class="form-control" id="organization-Name"
                              path="organizationName" placeholder="Organization Name" value="${organizationForDispaly.organizationName}"/>
                    <sf:errors path="organizationName" cssclass="error"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="organization-Description" class="col-md-2 control-label"> organization Description:</label>
                    <div class="col-md-4">
                    <sf:input type="text" class="form-control" id="organization-Description"
                              path="organizationDescription" placeholder="Organization Description" value="${organizationForDispaly.organizationDescription}"/>
                    <sf:errors path="organizationDescription" cssclass="error"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="organization-address-number" class="col-md-2 control-label"> organization address number:</label>
                    <div class="col-md-4">
                    <sf:input type="text" class="form-control" id="organization-address-number"
                              path="organizationAddressNumber" placeholder="Organization Address Number" value="${organizationForDispaly.organizationAddressNumber}"/>
                    <sf:errors path="organizationAddressNumber" cssclass="error"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="organization-address-street" class="col-md-2 control-label">organization address street:</label>
                    <div class="col-md-4">
                    <sf:input type="text" class="form-control" id="organization-address-street"
                              path="organizationAddressStreet" placeholder="Organization Address Street" value="${organizationForDispaly.organizationAddressStreet}"/>
                    <sf:errors path="organizationAddressStreet" cssclass="error"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="organization-address-City:" class="col-md-2 control-label">organization address City:</label>
                    <div class="col-md-4">
                    <sf:input type="text" class="form-control" id="organization-address-City:"
                              path="organizationAddressCity" placeholder="Organization Address City" value="${organizationForDispaly.organizationAddressCity}"/>
                    <sf:errors path="organizationAddressCity" cssclass="error"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="organization-address-Zip-Code" class="col-md-2 control-label">organization address Zip Code :</label>
                    <div class="col-md-4">
                    <sf:input type="text" class="form-control" id="organization-address-Zip-Code"
                              path="organizaitonAddressZipcode" placeholder="Organizaiton Address Zipcode" value="${organizationForDispaly.organizaitonAddressZipcode}"/>
                    <sf:errors path="organizaitonAddressZipcode" cssclass="error"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-organization-contact-Email" class="col-md-2 control-label">organization contact Email :</label>
                    <div class="col-md-4">
                    <sf:input type="text" class="form-control" id="add-organization-contact-Email"
                              path="organizationContactEmail" placeholder="Organization ContactEmail" value="${organizationForDispaly.organizationContactEmail}"/>
                    <sf:errors path="organizationContactEmail" cssclass="error"></sf:errors>
                    </div>
                </div>
            <sec:authorize access="hasAnyRole('ROLE_ADMIN')"> 
                <button type="submit" 
                        name="Organization"  
                        value="addOrganization" 
                        id="addOrganization" 
                        style="text-align: center" 
                        class="btn btn-default ">
                    Add Organization 


                </button>
            </sec:authorize>     
        </sf:form>
    </div>

    <div class="cl-md-8" align="center">                
        <div class="row" align="center">
            <div class="cl-md-8" align="center">
                <c:forEach var="organization" items="${allOrganizationsInDBO}">
                    <hr>    
                    <p align="center"> organization Name:</p>
                    <a href="displayOrganizationForm?identifier=${organization.organizationId}">
                        <c:out value="${organization.organizationName}"/></a><br>
                    <p align="center"> organization Description <c:out value="${organization.organizationDescription}"/> </p><br>
                    <p align="center"> organization Address Number <c:out value="${organization.organizationAddressNumber}"/> </p><br>
                    <p align="center"> organization Address Street <c:out value="${organization.organizationAddressStreet}"/> </p><br>
                    <p align="center"> organization Address City <c:out value="${organization.organizationAddressCity}"/> </p><br>
                    <p align="center"> organization Address Zip code  <c:out value="${organization.organizaitonAddressZipcode}"/> </p><br>
                    <p align="center"> organization Contact Email <c:out value="${organization.organizationContactEmail}"/> </p><br> 
                    <sec:authorize access="hasRole('ROLE_ADMIN')">    
                        <p allign="center"><a href="UpdateOrganizationForm?identifier=${organization.organizationId}">Edit Organization</a></p><br>
                        <p allign="center"><a href="DeleteOrganizationForm?identifier=${organization.organizationId}">Delete Organization</a></p><br>
                    </sec:authorize>

                    <hr> 
                </c:forEach>
            </div>
        </div>
    </div>
    <div class="cl-md-8" align="center">                
        <div class="row" align="center">
            <div class="cl-md-8" align="center">
                <c:forEach var="organization" items="${allOrganizationsInDBO}">
                    <hr>    
                    <p align="center"> organization Name:</p>
                    <a href="displayOrganizationForm?identifier=${organization.organizationId}">
                        <c:out value="${organization.organizationName}"/></a><br>
                    <p align="center"> organization Description <c:out value="${organization.organizationDescription}"/> </p><br>
                    <p align="center"> organization Address Number <c:out value="${organization.organizationAddressNumber}"/> </p><br>
                    <p align="center"> organization Address Street <c:out value="${organization.organizationAddressStreet}"/> </p><br>
                    <p align="center"> organization Address City <c:out value="${organization.organizationAddressCity}"/> </p><br>
                    <p align="center"> organization Address Zip code  <c:out value="${organization.organizaitonAddressZipcode}"/> </p><br>
                    <p align="center"> organization Contact Email <c:out value="${organization.organizationContactEmail}"/> </p><br>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <p allign="center"><a href="UpdateOrganizationForm?identifier=${organization.organizationId}">Edit Organization</a></p><br>
                        <p allign="center"><a href="DeleteOrganizationForm?identifier=${organization.organizationId}">Delete Organization</a></p><br>
                    </sec:authorize>  
                    <hr> 
                </c:forEach>
            </div>
        </div>
    </div> 
    <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

</body>

</html>
