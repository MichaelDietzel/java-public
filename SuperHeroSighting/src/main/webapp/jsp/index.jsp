<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<!DOCTYPE html>
<html>
    <head>

        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
    </head>
    <body>
        <div class="container">
            <h1>Super Hero Home Page</h1>
            <hr/>
            <div align="left" class="container">
                <div class="navbar">
                    <ul class="nav nav-tabs">
                        <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/index">Home</a></li>
                        <li role="presentation"><a href="${pageContext.request.contextPath}/EnhancedScreen">Enhanced Screen</a></li>
                        <li role="presentation"><a href="${pageContext.request.contextPath}/LocationScreen">Location Screen</a></li>
                        <li role="presentation"><a href="${pageContext.request.contextPath}/SightingsScreen">Sightings Screen</a></li>
                        <li role="presentation"><a href="${pageContext.request.contextPath}/OrganizationScreen">Organization Screen</a></li>
                            <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <li role="presentation"><a href="${pageContext.request.contextPath}/AdminScreen">Admin Screen</a></li>
                            </sec:authorize>
                    </ul>    
                </div>
            </div>
            <h2>Home Page</h2>
            <p> The changes page is a basic page that allows the user to add delete and update the database from  a minimum  user interface.</p>
            <p> 
            <p> messages will populate in the message box  to confirm changes to the database. </p>
            <p> confirmation messages will also appear, the goal is to be simple and  as intuitive as possible </p>
            <p> Lists of information will populate bellow the "add" update interfaces. The goal being to provide an intuitive way to see results</p>
            <p> I decided to make the information display in the manner it does because I used to work in SAS daily and to be honest i'm tired of staring at rows</p>
            <p> For my DAO  decisions I opted to run with basic SQL statements and simulate the where clause into Java logic. Why because I work in SQL Daily and it is boring 
            <p> So what I did is formulate Reusable basic SQL statements that lacked joins and can be used flexed to bring in data and simulate joins with java logic which is fun. </p> 


            <h3>Ten Sightings</h3>
            <div class="row">

                <div class="cl-md-8">
                    <c:forEach var="Sighting" items="${tenSightings}">
                        <hr>
                        <p align="left"> Enhanced Sighted :<c:out value="${Sighting.enhancedSighted}"/> </p><br>
                        <p align="left"> Date Stamp <c:out value="${Sighting.dateStamp}"/> </p><br>
                        <p align="left"> Enhanced Id <c:out value="${Sighting.enahncedComponent.identifier}"/> </p><br>
                        <p align="left"> location Id <c:out value="${Sighting.locationComponet.locationID}"/> </p><br>
                        <p align="left"> incident Id <c:out value="${Sighting.incidentId}"/> </p><br>
                        <hr>
                    </c:forEach>
                </div>
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

