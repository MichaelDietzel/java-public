<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- Directive for Spring Form tag libraries -->
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
    <title>Update Location</title>
    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
</head>
<div align="center" class= "col-md-12">
    <h3>Locations Form</h3>
    <sf:form class="form-horizontal" role="form" modelAttribute="location"
             action="updatedLocation" method="GET">

        <div class="form-group">
            <label for="Location-addressNumber" class="col-md-2 control-label">Enhanced sighted:</label>
            <div class="col-md-4">
                <sf:input type="text" class="form-control" id="Location-addressNumber"
                          path="addressNumber" placeholder="Location Address addressNumber" value="${locationForDisplay.addressNumber}"/>
                <sf:errors path="addressNumber" cssclass="error"></sf:errors>
                </div>
            </div>
            <div class="form-group">
                <label for="Location-addressStreet" class="col-md-2 control-label">Enhanced sighted:</label>
                <div class="col-md-4">
                <sf:input type="text" class="form-control" id="Location-addressStreet"
                          path="addressStreet" placeholder="Location Address Street"  value="${locationForDisplay.addressStreet}"/>
                <sf:errors path="addressStreet" cssclass="error"></sf:errors>
                </div>
            </div>
            <div class="form-group">
                <label for="Location-addressCity" class="col-md-2 control-label"> organization Description:</label>
                <div class="col-md-4">
                <sf:input type="text" class="form-control" id="Location-addressCity"
                          path="addressCity" placeholder="Location addressCity"  value="${locationForDisplay.addressCity}"/>
                <sf:errors path="addressCity" cssclass="error"></sf:errors>
                </div>
            </div>
            <div class="form-group">
                <label for="Location-addressZipcode" class="col-md-2 control-label"> organization address number:</label>
                <div class="col-md-4">
                <sf:input type="text" class="form-control" id="Location-addressZipcode"
                          path="addressZipcode" placeholder="Location addressZipcode"  value="${locationForDisplay.addressZipcode}"/>
                <sf:errors path="addressZipcode" cssclass="error"></sf:errors>
                </div>
            </div>
            <div class="form-group">
                <label for="Location-latitude" class="col-md-2 control-label">organization address street:</label>
                <div class="col-md-4">
                <sf:input type="text" class="form-control" id="Location-latitude"
                          path="latitude" placeholder="Location latitude"  value="${locationForDisplay.latitude}"/>
                <sf:errors path="latitude" cssclass="error"></sf:errors>
                </div>
            </div>
            <div class="form-group">
                <label for="Location-longitude" class="col-md-2 control-label">organization address street:</label>
                <div class="col-md-4">
                <sf:input type="text" class="form-control" id="Location-longitude"
                          path="longitude" placeholder="Location longitude"  value="${locationForDisplay.longitude}"/>
                <sf:errors path="longitude" cssclass="error"></sf:errors>
                </div>
            </div>      
            <div class="form-group">
                <label for="location-Description" class="col-md-2 control-label">organization address street:</label>
                <div class="col-md-4">
                <sf:input type="text" class="form-control" id="location-Description"
                          path="locationDescription" placeholder=" Location Description"  value="${locationForDisplay.locationDescription}"/>
                <sf:errors path="locationDescription" cssclass="error"></sf:errors>
                </div>
            <sf:hidden path="locationID"/>
        </div>     

        <button type="submit" 
                name="updatedLocation"  
                value="addLocation" 
                id="addOrganization" 
                style="text-align: center" 
                class="btn btn-default ">
            Update Location 
        </button>
    </sf:form>
    <form action="back" method="POST">
        <button type="submit" 
                name="Back"  
                value="displayAllSightings" 
                id="" 
                style="text-align: center" 
                class="btn btn-default ">
            Back
        </button>
    </form>
</div>
</div>   
<script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</body>
</html>
