<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- Directive for Spring Form tag libraries -->
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <title>Admin update Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
    </head>
    <div align="center" class= "col-md-12">  

        <h3>Admin Form</h3>
        <sf:form class="form-horizontal" role="form" modelAttribute="user"
                 action="updatedUser" method="GET">
            <div class="form-group">
                <label for="enhancedSighted-add" class="col-md-2 control-label">User username</label>
                <div class="col-md-4">
                    <sf:input type="text" class="form-control" id="enhancedSighted-add"
                              path="username"  value="${user.username}"/>
                    <sf:errors path="username" cssclass="error"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Sightings-enhancedId" class="col-md-2 control-label"> User password"</label>
                    <div class="col-md-4">
                    <sf:input type="text" class="form-control" id="Sightings-enhancedId"
                              path="password"  value="${user.password}"/>
                    <sf:errors path="password" cssclass="error"></sf:errors>
                    </div>
                </div>
                <div class="form-group">

                    Admin User? <input type="checkbox" 
                                       name="isAdmin" value="yes"/> <br/>
                   
                </div>
            </div>
        <sf:hidden path="id"/>
        <sf:hidden path="enabled"/>
    <button type="submit" 
            name="updatedUser"  
            id="" 
            style="text-align: center" 
            class="btn btn-default ">
        Update User 
    </button>
</sf:form>
<form action="back" method="POST">
    <button type="submit" 
            name="Back"  
            value="displayAllSightings" 
            id="" 
            style="text-align: center" 
            class="btn btn-default ">
        Back
    </button>
</form>
</div>
</div>      
<script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</html>
