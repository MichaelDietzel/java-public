<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- Directive for Spring Form tag libraries -->
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="navbar">
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="${pageContext.request.contextPath}/index">Home</a></li>
                <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/EnhancedScreen">Enhanced Screen</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/LocationScreen">Location Screen</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/SightingsScreen">Sightings Screen</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/OrganizationScreen">Organization Screen</a></li>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/AdminScreen">Admin Screen</a></li>
                    </sec:authorize>
            </ul>    
        </div>
        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <p>Hello : ${pageContext.request.userPrincipal.name}
                | <a href="<c:url value="/j_spring_security_logout" />" > Logout</a>
            </p>
        </c:if>            
        <div>    
            <h2 text-align="center">${message}</h2>
        </div>
        <hr>
        <form action="dboGetallEnhanced" method="POST">
            <button type="submit" 
                    name="dboGetallEnhanced"  
                    id="" 
                    style="text-align: center" 
                    class="btn btn-default ">
                Display all Enhanced 
            </button>
        </form>
        <hr>
        <div align="center" class= "col-md-12">

            <h3>Enhanced Form</h3>              
            <sf:form class="form-horizontal" role="form" modelAttribute="enhanced"
                     action="addEnhanced" method="POST">
                <div class="form-group">
                    <label for="add-Name" class="col-md-2 control-label"> Enhanced-Name:</label>
                    <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="add-name" path="name" placeholder=" Name" value="${enhancedForDisplay.name}"/>
                        <sf:errors path="name" cssclass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-Powers" class="col-md-2 control-label">Powers:</label>                          
                        <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="add-company" path="powers" placeholder="Company"  value="${enhancedForDisplay.powers}"/>
                        <sf:errors path="powers" cssclass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-Allignment" class="col-md-2 control-label">Allignment:</label>
                        <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="add-email"
                                  path="allignment" placeholder="Allignment:"  value="${enhancedForDisplay.allignment}"/>
                        <sf:errors path="allignment" cssclass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-Organization" class="col-md-2 control-label">Organization:</label>
                        <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="add-Organization"
                                  path="organizationComponent.organizationName" placeholder="Phone"  value="${enhancedForDisplay.organizationComponent.organizationName}"/>
                        <sf:errors path="organizationComponent.organizationName" cssclass="error"></sf:errors>
                        </div>
                    </div>
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                    <button type="submit" 
                            name="addEnhanced" 
                            path="action"
                            value="addEnhanced" 
                            id="addEnhanced" 
                            style="text-align: center" 
                            class="btn btn-default ">
                        Add Enhanced 
                    </button>
                </sec:authorize>
            </sf:form> 

            <br><br><br><br>
        </div>
        <div align="center" class= "col-md-12">
            <sec:authorize access="hasRole('ROLE_SIDEKICK')">
                <hr>
                <h3> special report</h3>
                <hr>

                <sf:form class="form-horizontal" role="form" modelAttribute="enhanced"
                         action="AllEnhancedInOrganizaitonForm" method="GET">
                    <label for="Sightings-locationId" class="col-md-2 control-label">All Enhanced In Organization :</label>
                    <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="Sightings-locationId"
                                  path="organizationComponent.organizationName" placeholder="Enhanced Organizaiton" value="${sightingsForDisplay.enahncedComponent.identifier}"/>
                        <sf:errors path="organizationComponent.organizationName" cssclass="error"></sf:errors>
                        </div>

                        <button type="submit" 
                                name="Back"  
                                value="AllEnhancedInOrganizaitonForm" 
                                id="" 
                                style="text-align: center" 
                                class="btn btn-default ">
                            Find all Enhanced In Organization
                        </button>

                </sf:form>
            </sec:authorize>   
        </div>
        <div align="center" class= "col-md-12">
            <hr>
            <sec:authorize access="hasRole('ROLE_SIDEKICK')">
                <sf:form class="form-horizontal" role="form" modelAttribute="enhanced"
                         action="AllOrganizationEnhancedBelongsToForm" method="GET">
                    <label for="Sightings-locationId" class="col-md-2 control-label">All Orgs enhanced belongs to :</label>
                    <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="Sightings-locationId"
                                  path="identifier" placeholder="Enhacned Id" value=""/>
                        <sf:errors path="identifier" cssclass="error"></sf:errors>
                        </div>
                        <button type="submit" 
                                name="Back"  
                                value="AllOrganizationEnhancedBelongsToForm" 
                                id="" 
                                style="text-align: center" 
                                class="btn btn-default ">
                            All Orgs enhanced belongs to:
                        </button>
                </sf:form>
            </sec:authorize>
            <br><br><br><br>
        </div>

        <div class="row" align="center">
            <div class="cl-md-8" align="center">
                <c:forEach var="thisEnhanced" items="${allEnhancedInDBO}">
                    <hr>
                    <p align="center"> Enhanced Name</p>
                    <a href="displayEnhancedForm?identifier=${thisEnhanced.identifier}"><c:out value="${thisEnhanced.name}"/></a><br>
                    <p align="center"> Enhanced Power <c:out value="${thisEnhanced.powers}"/> </p><br>
                    <p align="center"> Enhanced Alignment <c:out value="${thisEnhanced.allignment}"/> </p><br>
                    <p align="center"> Enhanced Organization <c:out value="${thisEnhanced.organizationComponent.organizationName}"/> </p><br>
                    <sec:authorize access="hasRole('ROLE_SIDEKICK')">
                        <p allign="center"><a href="UpdateEnhancedForm?identifier=${thisEnhanced.identifier}">Edit Enhanced</a></p><br
                        </sec:authorize>     
                        <sec:authorize access="hasRole('ROLE_ADMIN')">        
                            <p allign="center"><a href="DeleteEnhancedForm?identifier=${thisEnhanced.identifier}">Delete Enhanced</a></p><br>
                    </sec:authorize>     
                    <sec:authorize access="hasAnyRole('ROLE_SIDEKICK')">
                        <p allign="center"><a href="AllEnhancedInOrganizaitonList?organizationComponent.organizationName=${thisEnhanced.organizationComponent.organizationName}"> All Members Of this Organization</a></p><br>
                        <p allign="center"><a href="AllOrganizationEnhancedBelongsToList?identifier=${thisEnhanced.identifier}"> All Organizaiton This Enhanced is part of</a></p><br>
                    </sec:authorize>  
                    <hr>    
                </c:forEach>
            </div>
        </div>
    </body>
</html>
