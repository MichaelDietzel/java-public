<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- Directive for Spring Form tag libraries -->
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Update Enhanced</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
    </head>
    <body>
        <div align="left" class="container">  
            <h1>Update Enhanced.</h1>    
            <h2 text-align="center">${message}</h2>
        </div>
        <hr>
        <div align="center" class= "col-md-12">
            <h3>Enhanced Form</h3>    
            <sf:form class="form-horizontal" role="form" modelAttribute="enhanced"
                     action="updatedEnhanced" method="GET">
                    <div class="form-group">
                        <label for="add-Name" class="col-md-2 control-label"> Enhanced-Name:</label>
                        <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="add-name" path="name" placeholder=" Name" value="${enhancedForDisplay.name}"/>
                        <sf:errors path="name" cssclass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-Powers" class="col-md-2 control-label">Powers:</label>                          
                        <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="add-company" path="powers" placeholder="Company"  value="${enhancedForDisplay.powers}"/>
                        <sf:errors path="powers" cssclass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-Allignment" class="col-md-2 control-label">Allignment:</label>
                        <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="add-email"
                                  path="allignment" placeholder="Email"  value="${enhancedForDisplay.allignment}"/>
                        <sf:errors path="allignment" cssclass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-Organization" class="col-md-2 control-label">Organization:</label>
                        <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="add-Organization"
                                  path="organizationComponent.organizationName" placeholder="Phone"  value="${enhancedForDisplay.organizationComponent.organizationName}"/>
                        <sf:errors path="organizationComponent.organizationName" cssclass="error"></sf:errors>
                        </div>
                        <sf:hidden path="identifier"/>
                    </div>
                    <button type="submit" 
                            name="updatedEnhanced" 
                            path="action"
                            value="updatedEnhanced" 
                            id="addEnhanced" 
                            style="text-align: center" 
                            class="btn btn-default ">
                        Update Enhanced 
                    </button>
            </sf:form> 
            <form action="back" method="POST">
                <button type="submit" 
                        name="Back"  
                        value="displayAllSightings" 
                        id="" 
                        style="text-align: center" 
                        class="btn btn-default ">
                    Back
                </button>
            </form>

            <!-- Placed at the end of the document so the pages load faster -->
            <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>