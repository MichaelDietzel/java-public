<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- Directive for Spring Form tag libraries -->
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
    <title>Update Organization</title>
    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
</head>
<div align="center" class= "col-md-12">  
    <h3>Organization Form</h3>
    <sf:form class="form-horizontal" role="form" modelAttribute="organization"
             action="updatedOrganization" method="GET">

            <div class="form-group">
                <label for="organization-Name" class="col-md-2 control-label">organization Name:</label>
                <div class="col-md-4">
                <sf:input type="text" class="form-control" id="organization-Name"
                          path="organizationName" placeholder="Organization Name" value="${organizationForDispaly.organizationName}"/>
                <sf:errors path="organizationName" cssclass="error"></sf:errors>
                </div>
            </div>
            <div class="form-group">
                <label for="organization-Description" class="col-md-2 control-label"> organization Description:</label>
                <div class="col-md-4">
                <sf:input type="text" class="form-control" id="organization-Description"
                          path="organizationDescription" placeholder="Organization Description" value="${organizationForDispaly.organizationDescription}"/>
                <sf:errors path="organizationDescription" cssclass="error"></sf:errors>
                </div>
            </div>
            <div class="form-group">
                <label for="organization-address-number" class="col-md-2 control-label"> organization address number:</label>
                <div class="col-md-4">
                <sf:input type="text" class="form-control" id="organization-address-number"
                          path="organizationAddressNumber" placeholder="Organization Address Number" value="${organizationForDispaly.organizationAddressNumber}"/>
                <sf:errors path="organizationAddressNumber" cssclass="error"></sf:errors>
                </div>
            </div>
            <div class="form-group">
                <label for="organization-address-street" class="col-md-2 control-label">organization address street:</label>
                <div class="col-md-4">
                <sf:input type="text" class="form-control" id="organization-address-street"
                          path="organizationAddressStreet" placeholder="Organization Address Street" value="${organizationForDispaly.organizationAddressStreet}"/>
                <sf:errors path="organizationAddressStreet" cssclass="error"></sf:errors>
                </div>
            </div>
            <div class="form-group">
                <label for="organization-address-City:" class="col-md-2 control-label">organization address City:</label>
                <div class="col-md-4">
                <sf:input type="text" class="form-control" id="organization-address-City:"
                          path="organizationAddressCity" placeholder="Organization Address City" value="${organizationForDispaly.organizationAddressCity}"/>
                <sf:errors path="organizationAddressCity" cssclass="error"></sf:errors>
                </div>
            </div>
            <div class="form-group">
                <label for="organization-address-Zip-Code" class="col-md-2 control-label">organization address Zip Code :</label>
                <div class="col-md-4">
                <sf:input type="text" class="form-control" id="organization-address-Zip-Code"
                          path="organizaitonAddressZipcode" placeholder="Organizaiton Address Zipcode" value="${organizationForDispaly.organizaitonAddressZipcode}"/>
                <sf:errors path="organizaitonAddressZipcode" cssclass="error"></sf:errors>
                </div>
            </div>
            <div class="form-group">
                <label for="add-organization-contact-Email" class="col-md-2 control-label">organization contact Email :</label>
                <div class="col-md-4">
                <sf:input type="text" class="form-control" id="add-organization-contact-Email"
                          path="organizationContactEmail" placeholder="Organization ContactEmail" value="${organizationForDispaly.organizationContactEmail}"/>
                <sf:errors path="organizationContactEmail" cssclass="error"></sf:errors>
                </div>
                <sf:hidden path="organizationId"/>
            </div>
            <button type="submit" 
                    name="updatedOrganization"  
                    value="addOrganization" 
                    id="addOrganization" 
                    style="text-align: center" 
                    class="btn btn-default ">
                Update Organization 
            </button>     
    </sf:form>
        <form action="back" method="POST">
        <button type="submit" 
                name="Back"  
                value="displayAllSightings" 
                id="" 
                style="text-align: center" 
                class="btn btn-default ">
            Back
        </button>
    </form>
</div>
</div>   
<script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</body>
</html>
