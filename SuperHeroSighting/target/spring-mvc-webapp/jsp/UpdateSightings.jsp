<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- Directive for Spring Form tag libraries -->
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
    <title>Update Sightings</title>
    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
</head>
<div align="center" class= "col-md-12">  
    <h3>Organization Form</h3>
    <div align="center" class= "col-md-12">
        <h3>Sightings Form</h3>
        <sf:form class="form-horizontal" role="form" modelAttribute="sightings"
                 action="updatedSightings" method="GET">
            <div class="form-group">
                <label for="enhancedSighted-add" class="col-md-2 control-label">Enhanced sighted:</label>
                <div class="col-md-4">
                    <sf:input type="text" class="form-control" id="enhancedSighted-add"
                              path="enhancedSighted" placeholder="Sightings enhanced Sighted" value="${sightingsForDisplay.enhancedSighted}"/>
                    <sf:errors path="enhancedSighted" cssclass="error"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Sightings-enhancedId" class="col-md-2 control-label"> Sightings Enhanced Id"</label>
                    <div class="col-md-4">
                    <sf:input type="text" class="form-control" id="Sightings-enhancedId"
                              path="enahncedComponent.identifier" placeholder="Sightings Enhanced Id" value="${sightingsForDisplay.enahncedComponent.identifier}"/>
                    <sf:errors path="enahncedComponent.identifier" cssclass="error"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Sightings-locationId" class="col-md-2 control-label"> Sightings location Id</label>
                    <div class="col-md-4">
                    <sf:input type="text" class="form-control" id="Sightings-locationId"
                              path="locationComponet.locationID" placeholder="Sightings location Id" value="${sightingsForDisplay.locationComponet.locationID}"/>
                    <sf:errors path="locationComponet.locationID" cssclass="error"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Sightings-dateStamp" class="col-md-2 control-label">Sightings Date Stamp</label>
                    <div class="col-md-4">
                    <sf:input type="date" class="form-control" id="Sightings-dateStamp"
                              path="dateStamp" placeholder="Sightings Date Stamp" value="${sightingsForDisplay.dateStamp}"/>
                    <sf:errors path="dateStamp" cssclass="error"></sf:errors>
                    </div>
                <sf:hidden path="incidentId"/>
            </div>



            <button type="submit" 
                    name="updatedSightings"  
                    value="addSightings" 
                    id="" 
                    style="text-align: center" 
                    class="btn btn-default ">
                Update Sighting 
            </button>
        </sf:form>
        <form action="back" method="POST">
            <button type="submit" 
                    name="Back"  
                    value="displayAllSightings" 
                    id="" 
                    style="text-align: center" 
                    class="btn btn-default ">
                Back
            </button>
        </form>
    </div>
</div>      
<script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</body>
</html>
