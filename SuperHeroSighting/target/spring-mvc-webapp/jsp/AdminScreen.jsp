<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="navbar">
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="${pageContext.request.contextPath}/index">Index</a></li>
                <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/EnhancedScreen">Enhanced Screen</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/LocationScreen">Location Screen</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/SightingsScreen">Sightings Screen</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/OrganizationScreen">Organization Screen</a></li>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/AdminScreen">Admin Screen</a></li>
                    </sec:authorize>
            </ul>    
        </div>
        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <p>Hello : ${pageContext.request.userPrincipal.name}
                | <a href="<c:url value="/j_spring_security_logout" />" > Logout</a>
            </p>
        </c:if>            
        <div>    
            <h2 text-align="center">${message}</h2>
        </div>
        <hr>
        <form action="dboGetallAdmin" method="POST">
            <button type="submit" 
                    name="dboGetallAdmin"  
                    id="" 
                    style="text-align: center" 
                    class="btn btn-default ">
                Display all Users 
            </button>
        </form>
        <hr>
        <div align="center" class= "col-md-12">

            <h3>Admin Form</h3>              
            <form class="form-horizontal" method="POST" action="addUser" >
                Username: <input type="text" 
                                 name="username"/><br/>
                Password: <input type="password" 
                                 name="password"/><br/>
                Admin User? <input type="checkbox" 
                                   name="isAdmin" value="yes"/> <br/>
                <input class="btn btn-default" type="submit" value="Add User"/><br/>
            </form>

            <br><br><br><br>
        </div>


        <div class="cl-md-8" align="center">
            <c:forEach var="thisUser" items="${allUsersInDbo}">
                <c:out value="${thisUser.username}"/> |
                <a href="deleteUser?username=${thisUser.username}">Delete</a><br/><br/>
                <a href="updateUser?username=${thisUser.username}">Edit</a><br/><br/>
                <c:choose>
                    <c:when test = "${thisUser.enabled == '1'}">
                        <a href="disableUser?username=${thisUser.username}">Disable</a><br/><br/>
                    </c:when>
                    <c:otherwise>
                        <a href="EnableUser?username=${thisUser.username}">Enable</a><br/><br/>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
