<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- Directive for Spring Form tag libraries -->
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <div align="left" class="container">  
        <h1>Sightings Page.</h1>
        <form action="headerSubmit" method="POST">
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/index">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/EnhancedScreen">Enhanced Screen</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/LocationScreen">Location Screen</a></li>
                    <li role="presentation"  class="active"><a href="${pageContext.request.contextPath}/SightingsScreen">Sightings Screen</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/OrganizationScreen">Organization Screen</a></li>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li role="presentation"><a href="${pageContext.request.contextPath}/AdminScreen">Admin Screen</a></li>
                        </sec:authorize>
                </ul>    
            </div>
        </form>                    
 
        <div>    
            <h2 text-align="center">${message}</h2>
        </div>
        <hr>
        <form action="dboGetallSighting" method="POST">
            <button type="submit" 
                    name="dboGetallSighting"  
                    value="displayAllSightings" 
                    id="" 
                    style="text-align: center" 
                    class="btn btn-default ">
                Display all Sighting 
            </button>
        </form>
        <hr>
        <div align="center" class= "col-md-12">
            <h3>Sightings Form</h3>
            <sf:form class="form-horizontal" role="form" modelAttribute="sightings"
                     action="addSightings" method="POST">

                <div class="form-group">
                    <label for="enhancedSighted-add" class="col-md-2 control-label">Enhanced sighted:</label>
                    <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="enhancedSighted-add"
                                  path="enhancedSighted" placeholder="Sightings enhanced Sighted" value="${sightingsForDisplay.enhancedSighted}"/>
                        <sf:errors path="enhancedSighted" cssclass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Sightings-enhancedId" class="col-md-2 control-label"> Sightings Enhanced Id"</label>
                        <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="Sightings-enhancedId"
                                  path="enahncedComponent.identifier" placeholder="Sightings Enhanced Id" value="${sightingsForDisplay.enahncedComponent.identifier}"/>
                        <sf:errors path="enahncedComponent.identifier" cssclass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Sightings-locationId" class="col-md-2 control-label"> Sightings location Id</label>
                        <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="Sightings-locationId"
                                  path="locationComponet.locationID" placeholder="Sightings location Id" value="${sightingsForDisplay.locationComponet.locationID}"/>
                        <sf:errors path="locationComponet.locationID" cssclass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Sightings-dateStamp" class="col-md-2 control-label">Sightings Date Stamp</label>
                        <div class="col-md-4">
                        <sf:input type="date" class="form-control" id="Sightings-dateStamp"
                                  path="dateStamp" placeholder="Sightings Date Stamp" value="${sightingsForDisplay.dateStamp}"/>
                        <sf:errors path="dateStamp" cssclass="error"></sf:errors>
                        </div>
                    </div>



                    <button type="submit" 
                            name="Sighting"  
                            value="addSightings" 
                            id="" 
                            style="text-align: center" 
                            class="btn btn-default ">
                        Add Sighting 
                    </button>
            </sf:form>
            <br>
            <br>
            <br>
            <br>
        </div>

        <h3>Special Querys</h3>

        <div align="center" class= "col-md-12">
            <hr>
            <sf:form class="form-horizontal" role="form" modelAttribute="sightings"
                     action="AllLocationWhereSuperheroSeenForm" method="GET">
                <label for="Sightings-locationId" class="col-md-2 control-label">List all locations of sightings of Enhanced Id: </label>
                <div class="col-md-4">
                    <sf:input type="text" class="form-control" id="Sightings-locationId"
                              path="enahncedComponent.identifier" placeholder="Sightings location Id" value="${sightingsForDisplay.enahncedComponent.identifier}"/>
                    <sf:errors path="enahncedComponent.identifier" cssclass="error"></sf:errors>
                    </div>
                    <button type="submit" 
                            name="Back"  
                            value="AllLocationWhereSuperheroSeenForm" 
                            id="" 
                            style="text-align: center" 
                            class="btn btn-default ">
                        All Sightings at this Location
                    </button>
            </sf:form>
            <hr>
        </div>         


        <div class="cl-md-8" align="center">
            <hr>
            <sf:form class="form-horizontal" role="form" modelAttribute="sightings"
                     action="AllSighitngsOnDateForm" method="GET">
                <label for="Sightings-dateStamp" class="col-md-2 control-label">Sightings Date Stamp</label>
                <div class="col-md-4">
                    <sf:input type="date" class="form-control" id="Sightings-dateStamp"
                              path="dateStamp" placeholder="Sightings Date Stamp" value="${sightingsForDisplay.dateStamp}"/>
                    <sf:errors path="dateStamp" cssclass="error"></sf:errors>
                    </div>
                    <button type="submit" 
                            name="Back"  
                            value="AllSighitngsOnDateForm" 
                            id="" 
                            style="text-align: center" 
                            class="btn btn-default ">
                        All Sightings on this date:
                    </button>

            </sf:form>
            <hr>
        </div>  

        <div align="center" class= "col-md-12">
            <sf:form class="form-horizontal" role="form" modelAttribute="sightings"
                     action="AllEnhancedSeenAtLocationForm" method="GET">
                <label for="Sightings-locationId" class="col-md-2 control-label">All Enhanced Seen At Location ID </label>
                <div class="col-md-4">
                    <sf:input type="text" class="form-control" id="Sightings-locationId"
                              path="locationComponet.locationID" placeholder="Location Component" value="${sightingsForDisplay.enahncedComponent.identifier}"/>
                    <sf:errors path="locationComponet.locationID" cssclass="error"></sf:errors>
                    </div>
                    <button type="submit" 
                            name="Back"  
                            value="AllLocationWhereSuperheroSeenForm" 
                            id="" 
                            style="text-align: center" 
                            class="btn btn-default ">
                        All Enhanced at this Location
                    </button>
            </sf:form>
            <br><br><br><br>
        </div>  





        <div class="cl-md-8" align="center">
            <c:forEach var="Sightings" items="${allLocationSightings}">
                <hr>
                <p align="center"> Sighting incident Id <c:out value="${Sightings.incidentId}"/> </p><br>
                <p align="center"> Sighting Enhanced sighted :<c:out value="${Sightings.enhancedSighted}"/> </p><br>
                <p align="center"> Sighting Date Stamp  <c:out value="${Sightings.dateStamp}"/> </p><br>
                <hr>
                <p align="center"> Enhanced Name <c:out value="${Sightings.enahncedComponent.name}"/> </p><br>
                <p align="center"> Enhanced Power <c:out value="${Sightings.enahncedComponent.powers}"/> </p><br>
                <p align="center"> Enhanced Alignment <c:out value="${Sightings.enahncedComponent.allignment}"/> </p><br>
                <p align="center"> Enhanced Organization <c:out value="${Sightings.enahncedComponent.organizationComponent.organizationName}"/> </p><br>
                <hr>
                <p align="center"> Location Address Number <c:out value="${Sightings.locationComponet.addressNumber}"/> </p><br>
                <p align="center"> Location Address Street <c:out value="${Sightings.locationComponet.addressStreet}"/> </p><br>
                <p align="center"> Location Address City <c:out value="${Sightings.locationComponet.addressCity}"/> </p><br>
                <p align="center"> Location Address Zip Code <c:out value="${Sightings.locationComponet.addressZipcode}"/> </p><br>
                <p align="center"> Location latitude <c:out value="${Sightings.locationComponet.latitude}"/> </p><br>
                <p align="center"> Location longitude <c:out value="${Sightings.locationComponet.longitude}"/> </p><br>
                <p align="center"> Location location Description <c:out value="${Sightings.locationComponet.locationDescription}"/> </p><br>

                <hr>
            </c:forEach>
        </div>
        <div class="row" align="center">
            <div class="cl-md-8" align="center">
                <c:forEach var="Sightings" items="${allSightingsInDBO}">
                    <hr>
                    <p align="center"> Enhanced Spotted:</p>
                    <a href="displaySightingsForm?identifier=${Sightings.incidentId}">
                        <c:out value="${Sightings.enhancedSighted}"/></a><br>
                    <p align="center"> Sighting Date Stamp  <c:out value="${Sightings.dateStamp}"/> </p><br>
                    <p align="center"> Sightings Enhanced id </p> 
                    <a href="displayEnhancedForm?identifier=${Sightings.enahncedComponent.identifier}">
                        <c:out value="${Sightings.enahncedComponent.identifier}"/></a> <br>
                    <p align="center"> Sightings Location Id </p>    
                    <a href="displayLocationForm?identifier=${Sightings.locationComponet.locationID}">
                        <c:out value="${Sightings.locationComponet.locationID}"/></a> <br>
                    <p allign="center"><a href="UpdateSightingsForm?identifier=${Sightings.incidentId}">Edit Sighting</a></p><br>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">    
                        <p allign="center"><a href="DeleteSightingsForm?identifier=${Sightings.incidentId}">Delete Sighting</a></p><br><b>
                        </sec:authorize>    
                        <p allign="center"><a href="AllLocationWhereSuperheroSeenList?enahncedComponent.identifier=${Sightings.enahncedComponent.identifier}"> All Locations Where Super hero Seen</a></p><br>
                        <p allign="center"><a href="AllSighitngsOnDateList?dateStamp=${Sightings.dateStamp}"> All Sightings on this Date</a></p><br>
                        <p allign="center"><a href="AllSightingsatLocationList?enahncedComponent.identifier=${Sightings.enahncedComponent.identifier}">All Sightings At Location</a></p><br>
                        <p allign="center"><a href="AllEnhancedSeenAtLocationList?locationComponet.locationID=${Sightings.locationComponet.locationID}">All Enhanced Seen At Location</a></p></b><br>
                    <hr>
                </c:forEach>
            </div>
        </div>
    </div>

    <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

</body>

</html>
