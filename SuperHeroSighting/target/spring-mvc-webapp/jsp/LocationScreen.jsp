<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- Directive for Spring Form tag libraries -->
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

    </head>
    <div align="left" class="container">  
        <h1>Location Page.</h1>
        <div class="navbar">
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="${pageContext.request.contextPath}/index">Home</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/EnhancedScreen">Enhanced Screen</a></li>
                <li role="presentation"  class="active"><a href="${pageContext.request.contextPath}/LocationScreen">Location Screen</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/SightingsScreen">Sightings Screen</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/OrganizationScreen">Organization Screen</a></li>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/AdminScreen">Admin Screen</a></li>
                    </sec:authorize>
            </ul>    
        </div>
        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <p>Hello : ${pageContext.request.userPrincipal.name}
                | <a href="<c:url value="/j_spring_security_logout" />" > Logout</a>
            </p>
        </c:if>            

        <div>    
            <h2 text-align="center">${message}</h2>
        </div>
        <form action="dboGetallLocation" method="POST">
            <button type="submit" 
                    name="dboGetallLocation"  
                    value="displayAllSightings" 
                    id="" 
                    style="text-align: center" 
                    class="btn btn-default ">
                Display all Location 
            </button>
        </form>
        <div align="center" class= "col-md-12">
            <h3>Locations Form</h3>
            <sf:form class="form-horizontal" role="form" modelAttribute="location"
                     action="addLocation" method="POST">

                <div class="form-group">
                    <label for="Location-addressNumber" class="col-md-2 control-label">Address Number:</label>
                    <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="Location-addressNumber"
                                  path="addressNumber" placeholder="Location Address addressNumber" value="${locationForDisplay.addressNumber}"/>
                        <sf:errors path="addressNumber" cssclass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Location-addressStreet" class="col-md-2 control-label">Address Street:</label>
                        <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="Location-addressStreet"
                                  path="addressStreet" placeholder="Location Address Street"  value="${locationForDisplay.addressStreet}"/>
                        <sf:errors path="addressStreet" cssclass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Location-addressCity" class="col-md-2 control-label"> address City:</label>
                        <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="Location-addressCity"
                                  path="addressCity" placeholder="Location addressCity"  value="${locationForDisplay.addressCity}"/>
                        <sf:errors path="addressCity" cssclass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Location-addressZipcode" class="col-md-2 control-label"> address Zipcode:</label>
                        <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="Location-addressZipcode"
                                  path="addressZipcode" placeholder="Location addressZipcode"  value="${locationForDisplay.addressZipcode}"/>
                        <sf:errors path="addressZipcode" cssclass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Location-latitude" class="col-md-2 control-label">latitude:</label>
                        <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="Location-latitude"
                                  path="latitude" placeholder="Location latitude"  value="${locationForDisplay.latitude}"/>
                        <sf:errors path="latitude" cssclass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Location-longitude" class="col-md-2 control-label">longitude:</label>
                        <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="Location-longitude"
                                  path="longitude" placeholder="Location longitude"  value="${locationForDisplay.longitude}"/>
                        <sf:errors path="longitude" cssclass="error"></sf:errors>
                        </div>
                    </div>      
                    <div class="form-group">
                        <label for="location-Description" class="col-md-2 control-label">location Description:</label>
                        <div class="col-md-4">
                        <sf:input type="text" class="form-control" id="location-Description"
                                  path="locationDescription" placeholder=" Location Description"  value="${locationForDisplay.locationDescription}"/>
                        <sf:errors path="locationDescription" cssclass="error"></sf:errors>
                        </div>
                    </div>     

                    <button type="submit" 
                            name="Location"  
                            value="addLocation" 
                            id="addOrganization" 
                            style="text-align: center" 
                            class="btn btn-default ">
                        Add Location 
                    </button>
            </sf:form>
            <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

            </body>
            <div class="row" align="center">
                <div class="cl-md-8" align="center">
                    <c:forEach var="Location" items="${allLocationsInDBO}">
                        <hr>
                        <p align="center"> Location Address:</p>
                        <a href="displayLocationForm?identifier=${Location.locationID}">
                            <c:out value="${Location.addressNumber}"/> <c:out value="${Location.addressStreet}"/><br><c:out value="${Location.addressCity}"/><br><c:out value="${Location.addressZipcode}"/>  </a><br>
                        <p align="center"> latitude <c:out value="${Location.latitude}"/> </p><br>
                        <p align="center"> longitude  <c:out value="${Location.longitude}"/> </p><br>
                        <p align="center"> location Description <c:out value="${Location.locationDescription}"/> </p><br>
                        <p align="center"> location ID <c:out value="${Location.locationID}"/> </p><br>
                        <p allign="center"><a href="UpdateLocationForm?identifier=${Location.locationID}">Update Location</a></p><br>
                        <sec:authorize access="hasAnyRole('ROLE_ADMIN')">   
                            <p allign="center"><a href="DeleteLocationForm?identifier=${Location.locationID}">Delete Location</a></p><br>
                        </sec:authorize> 
                        <hr> 
                    </c:forEach>
                </div>
            </div>
            </html>
