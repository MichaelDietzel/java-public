DROP DATABASE IF EXISTS DVDLibrary;

CREATE DATABASE DVDLibrary;

/* foreign key Super Hero */
CREATE TABLE DVDLibrary.dvd
(
 DVDKey int not null, 
 DvdName varChar(20) not null,
 DvDDescription longtext not null,
 ActorStar varChar(20) not null,
 MovieType varChar(20) not null, 
 releaseDate int not null AUTO_INCREMENT,
 primary key(DVDKey)
);