
import com.mycompany.DTO.DVD;
import com.mycompany.mavenproject13.DVDinterface;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class DVDDAO implements DVDinterface {

    private static final String SQL_INSERT_DVD
            = "insert into dvd"
            + "( DvdName, DvDDescription, ActorStar, MovieType)"
            + "values (?, ?, ?, ?,?,?)";
    private static final String SQL_DELETE_DVD
            = "delete from enhanced where DVDKey = ?";
    private static final String SQL_SELECT_DVD
            = "select * from enhanced where DVDKey = ?";
    private static final String SQL_UPDATE_DVD
            = "update enhanced set "
            + "DvdName= ?, DvDDescription = ?, ActorStar = ?, MovieType = ? "
            + "where DVDKey = ?";
    private static final String SQL_SELECT_ALL_DVD
            = "select * from DVD";
    private static final String SQL_SELECT_ENHANCED_BY_ACTOR
            = "select * from ENHANCED where ActorStar = ?";

    private static final class DVDMapper implements RowMapper<DVD> {

        public DVD mapRow(ResultSet rs, int rowNum) throws SQLException {
            DVD thisDVD = new DVD();

            thisDVD.setActorStar(rs.getString("ActorStar"));
            thisDVD.setMovieType(rs.getString("MovieType"));
            thisDVD.setReleaseDate(rs.getString("releaseDate"));
            thisDVD.setDvdName(rs.getString("DvdName"));
            thisDVD.setDvDDescription("DvDDescription");
            thisDVD.setDvdKey(rs.getInt("DVDKey"));
            return thisDVD;

        }
    }

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public DVD addEnhanced(DVD thisDVD) {
        jdbcTemplate.update(SQL_INSERT_DVD,
                thisDVD.getDvdKey(),
                thisDVD.getActorStar(),
                thisDVD.getDvDDescription(),
                thisDVD.getDvdName(),
                thisDVD.getMovieType(),
                thisDVD.getReleaseDate());
        int newUniqueId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                Integer.class);
        thisDVD.setDvdKey(newUniqueId);

        return thisDVD;
    }

    @Override
    public void deleteDVD(int UniqueId) {
        jdbcTemplate.update(SQL_DELETE_DVD, UniqueId);
    }

    @Override
    public void updateDVD(DVD thisDVD) {
        jdbcTemplate.update(SQL_UPDATE_DVD,
                thisDVD.getDvdKey(),
                thisDVD.getActorStar(),
                thisDVD.getDvDDescription(),
                thisDVD.getDvdName(),
                thisDVD.getMovieType(),
                thisDVD.getReleaseDate(),
                thisDVD.getDvdKey());
    }

    @Override
    public DVD getDVD(int DVDId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_DVD,
                    new DVDMapper(), DVDId);
        } catch (EmptyResultDataAccessException ex) {

            return null;
        }

    }

    @Override
    public List<DVD> getAllDVD() {
        return jdbcTemplate.query(SQL_SELECT_ALL_DVD,
                new DVDMapper());
    }

    @Override
    public List<DVD> getAllDVD(String identifier) {

        return jdbcTemplate.query(SQL_SELECT_ENHANCED_BY_ACTOR,
                new DVDDAO.DVDMapper(),
                identifier);
    }

}
