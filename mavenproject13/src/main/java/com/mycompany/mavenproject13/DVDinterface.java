/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject13;

import com.mycompany.DTO.DVD;
import java.util.List;

/**
 *
 * @author Mike
 */
public interface DVDinterface {

       public void deleteDVD(int UniqueId);

    public DVD getDVD(int DVDId);
    
    public void updateDVD(DVD thisDVD);
    
    public DVD addEnhanced(DVD thisDVD);
    
    public List<DVD> getAllDVD();
    
    public List<DVD> getAllDVD(String identifier);
}
