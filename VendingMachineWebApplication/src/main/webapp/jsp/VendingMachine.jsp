<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Vending Machine</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet"> 
        <link href="${pageContext.request.contextPath}/css/vending.css" rel="stylesheet">  
    </head>
    <body>
        <div class="container" Style="text-align: center">
            <h1 style="Text-align: center">Vending Machine</h1>
            <ul class="list-group" id="errorMessages"></ul>
            <hr/>
            <div class="row">
                <div class="col-md-9" id="prod-diplay">
                    <form action="getItem" method="POST" >
                        <c:forEach var="currentItem" items="${itemList}">

                            <button style="height:250px;width:250px" name ="itemSelection" value="${currentItem.itemNumber}"   type="submit"  class="btn btn-default" id="products-btn"  >
                                <p align="left"> <c:out value="${currentItem.itemNumber}"/> </p><br>
                                <p> Name: <c:out value="${currentItem.name}"/> </p><br><br><br>
                                <p> Price: $<c:out value="${currentItem.price}"/> </p><br>
                                <p> Quantity: <c:out value="${currentItem.quantity}              "/> </p>
                            </button>

                        </c:forEach>
                    </form>

                </div>

                <div class="col-md-3" id="bank">

                    <div class="row">

                        <h3 Style="text-align: center">Total $ In</h3>
                        <div class="col-md-12">
                            <form action="addMoney" method="POST">
                                <div class="form-group">
                                    <input value="$${displayMoney}" class="form-control" style="text-align: center" id="display-money" placeholder="Enter Money" readonly/>
                                </div>
                                <div class="col-md-6">
                                    <button type="submit" 
                                            name="money"  
                                            value="one" 
                                            id="addOne" 
                                            style="text-align: center" 
                                            class="btn btn-default ">
                                        Add Dollar
                                    </button>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <button type="submit"
                                                name="money"  
                                                value="qrtr" 
                                                id="addQRTR" 
                                                style="text-align: center" 
                                                class="btn btn-default">
                                            Add Quarter
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <button type="submit"
                                                name="money" 
                                                value="dime" 
                                                id="addDime" 
                                                style="text-align: center"
                                                class="btn btn-default">
                                            Add Dime
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <button type="submit"
                                                name="money" 
                                                value="nickel"  
                                                id="addNickel" 
                                                style="text-align: center"
                                                class="btn btn-default">
                                            Add Nickels
                                        </button>
                                    </div>
                                </div>

                            </form> 
                        </div>
                        <div>
                            <form action="makePurchase" method="POST">                
                                <div class="form-group">
                                    <hr/>
                                    <h2 Style="text-align: center">MESSAGE</h2>
                                    <c:choose>
                                        <c:when test="${(outcomeMessage == 'SOLDOUT')}">
                                            <input class="form-control" style="text-align: center" id="displaymessage" value="Sold Out">
                                        </c:when>
                                        <c:when test="${(outcomeMessage == 'SOLD')}">
                                            <input class="form-control" style="text-align: center" id="displaymessage" value="Thank you!!!">
                                        </c:when> 
                                        <c:when test="${(outcomeMessage == 'SHORT')}">
                                            <input class="form-control" style="text-align: center" id="displaymessage" value="Please insert ${shortChange}">
                                        </c:when> 
                                        <c:otherwise>
                                            <input class="form-control" style="text-align: center" id="displaymessage" value=" ">
                                        </c:otherwise>  
                                    </c:choose>
                                </div>
                                <div class="form-group">
                                    <label for="displayItem" style="text-align: center" class="col-sm-2 control-label">
                                        Item:
                                    </label>
                                    <div class="col-md-8">
                                        <input value="${itemId}" class="form-control" style="text-align: center"  id="vending" placeholder="item no." readonly>
                                    </div>
                                </div>
                                <button type="submit"
                                        name="purchase" 
                                        value="makePurchased"
                                        id="makePurchase" 
                                        style="text-align: center" class="btn btn-default">Make Purchase</button>
                            </form>  
                            <hr/>

                            <h1 Style="text-align: center">Change</h1>
                            <div class="form-group" >

                                <form action="makeChange" method="POST"> 
                                    <c:choose>
                                        <c:when test="${(coins.getQuarters() == 0) && (coins.getDimes() == 00) && (coins.getNickles() == 0)}">
                                            <input class="form-control" style="text-align: left" id="changes" value=" ">
                                        </c:when>
                                        <c:otherwise>
                                            <input class="form-control" style="text-align: left" id="changes" value=" Quarters :${coins.getQuarters()} Dimes :${coins.getDimes()} Nickles :${coins.getNickles()} ">
                                        </c:otherwise>    
                                    </c:choose>




                                    </div>
                                    <form action="makeChange" method="POST">        
                                        <button type="submit"
                                                name="change"
                                                value="returnMoney"
                                                id="returnChange" 
                                                style="text-align: center" 
                                                class="btn btn-default">Change Return</button>
                                    </form> 
                            </div> 
                        </div>
                    </div>
                </div>

                <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
                <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
                </body>
                </html>
