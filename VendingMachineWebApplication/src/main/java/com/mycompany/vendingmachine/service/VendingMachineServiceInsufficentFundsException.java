/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.service;

/**
 *
 * @author michael
 */
public class VendingMachineServiceInsufficentFundsException extends Exception{
        public VendingMachineServiceInsufficentFundsException(String message) {
        super(message);
    }

    public VendingMachineServiceInsufficentFundsException(String message,Throwable cause) {
        super(message, cause);
    }
}
