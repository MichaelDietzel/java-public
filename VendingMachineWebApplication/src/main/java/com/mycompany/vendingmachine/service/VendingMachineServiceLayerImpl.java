/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.service;

import com.mycompany.vendingmachine.dao.VendingMachineDao;
import com.mycompany.vendingmachine.dao.VendingMachineDaoException;
import com.mycompany.vendingmachine.model.Change;

import com.mycompany.vendingmachine.model.Item;
import java.math.BigDecimal;
import static java.math.BigDecimal.ZERO;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 *
 * @author michael
 */
public class VendingMachineServiceLayerImpl implements VendingMachineServiceLayer {

    private VendingMachineDao dao;
    BigDecimal amountOfFundsNeeded = new BigDecimal("0.00"); 
    private BigDecimal totalFunds = new BigDecimal("0.00");
   

    public VendingMachineServiceLayerImpl(VendingMachineDao dao) {
        this.dao = dao;
    }

    @Override
    public void addFunds(BigDecimal funds) throws VendingMachineServiceInsufficentFundsException {

        totalFunds = totalFunds.add(funds);
        totalFunds.setScale(2, RoundingMode.HALF_UP);
        
    }

    @Override
    public String subtractFunds(Item selectedItem) throws VendingMachineServiceInsufficentFundsException {
        int quantity = Integer.valueOf(selectedItem.getQuantity());
        if (quantity > 0) {
            BigDecimal noFunds = new BigDecimal("0.00");
            BigDecimal selectedItemPrice = new BigDecimal(selectedItem.getPrice());
            totalFunds = totalFunds.subtract(selectedItemPrice);
            int fundsCheck = totalFunds.compareTo(noFunds);
            if (fundsCheck < 0) {
                totalFunds = totalFunds.add(selectedItemPrice);
                shortFunds(selectedItem);
            
                return "SHORT";
            }else{
                try {
                    updateInventory(selectedItem);
                } catch (VendingMachineServiceException ex) {
                    Logger.getLogger(VendingMachineServiceLayerImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
                
        }else{
            return "SOLDOUT";                 
                    }
        return "SOLD";
        }

    @Override
    public Item retreiveItem(String userVendingSelection) throws VendingMachineServiceException {

        int itemId = Integer.valueOf(userVendingSelection);
        int idAsInt;

        if (userVendingSelection.equals(" ")) {
            userVendingSelection = null;
        }
        if (userVendingSelection != null) {
            idAsInt = Integer.valueOf(itemId);
        } else {
            throw new VendingMachineServiceException(" no item selected");
        }

        if (idAsInt < 0 || idAsInt > 9) {
            throw new VendingMachineServiceException(" Item selected");
        }

        Item foundItem = null;
        foundItem = dao.getItem(userVendingSelection);

        return foundItem;
    }

    @Override
    public void updateInventory(Item selectedItem) throws VendingMachineServiceException {

        int quantity = Integer.valueOf(selectedItem.getQuantity());

        if (quantity < 0) {
            throw new VendingMachineServiceException("item sold out");
        }
        
        if (quantity > 0 ) {
            quantity--;
            selectedItem.setQuantity(Integer.toString(quantity));
            dao.updateInventory(selectedItem);
        }

       
    }

    @Override
    public Change changeDispense() throws VendingMachineServiceInsufficentFundsException {
        int coins = 0;

        Change change = new Change();

        BigDecimal converter = new BigDecimal("100");

        converter = converter.multiply(totalFunds);
        converter.setScale(0, RoundingMode.HALF_UP);
        coins = converter.intValueExact();

        change.setQuarters(coins / 25);
        coins = coins - (change.getQuarters() * 25);

        change.setDimes(coins / 10);
        coins = coins - (change.getDimes() * 10);

        change.setNickles(coins / 5);
        coins = coins - (change.getNickles() * 5);

        totalFunds = totalFunds.multiply(ZERO);

        return change;
    }

    @Override
    public void shortFunds(Item foundItem
    ) {
        BigDecimal negOne = new BigDecimal("-1");
        BigDecimal selectedItemPrice = new BigDecimal(foundItem.getPrice());
        BigDecimal shortFunds = totalFunds.subtract(selectedItemPrice);
        amountOfFundsNeeded = shortFunds.multiply(negOne);

        
    }

    @Override
    public Change removeCoins() {
        Change removedCoins = new Change();

        removedCoins.setQuarters(0);
        removedCoins.setDimes(0);
        removedCoins.setNickles(0);

        return removedCoins;
    }

    @Override
    public BigDecimal getFunds() {
        return totalFunds;
    }

    @Override
    public BigDecimal getShortFunds() {
        return amountOfFundsNeeded;
    }

}
