/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.service;


import com.mycompany.vendingmachine.model.Change;
import com.mycompany.vendingmachine.model.Item;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author michael
 */
public interface VendingMachineServiceLayer {
     
     
    public void addFunds(BigDecimal funds)
        throws VendingMachineServiceInsufficentFundsException;

    public String subtractFunds( Item selectedItem)
        throws VendingMachineServiceInsufficentFundsException;    

    public Item retreiveItem(String userVendingSelection)
        throws VendingMachineServiceException;
    
    public void updateInventory(Item selectedItem)
        throws VendingMachineServiceException;    

    public Change changeDispense()
        throws VendingMachineServiceInsufficentFundsException;

    public void shortFunds(Item foundItem);

    public Change removeCoins();

    public BigDecimal getFunds();

    public Object getShortFunds();
       
}
