/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.service;

import com.mycompany.vendingmachine.model.Change;
import com.mycompany.vendingmachine.model.Item;
import java.math.BigDecimal;
import static java.math.RoundingMode.DOWN;

/**
 *
 * @author Mike
 */
public class VendingMachineServiceLayerStubImpl implements VendingMachineServiceLayer {

    private Item onlyItem = new Item();
    private Change onlyCoins = new Change();
    private BigDecimal totalFunds = new BigDecimal("0.00");
    private String correct = "correct";
    private String incorrect = "incorrect";
    private BigDecimal amountOfFundsNeeded = new BigDecimal("0.00");

    public VendingMachineServiceLayerStubImpl() {

        onlyItem.setItemNumber("1");
        onlyItem.setName("1");
        onlyItem.setPrice("1");
        onlyItem.setQuantity("1");

        onlyCoins.setDimes(1);
        onlyCoins.setQuarters(1);
        onlyCoins.setNickles(1);

    }

    @Override
    public void addFunds(BigDecimal funds) throws VendingMachineServiceInsufficentFundsException {

        totalFunds = totalFunds.add(funds);

    }

    @Override
    public String subtractFunds(Item selectedItem) throws VendingMachineServiceInsufficentFundsException {
        BigDecimal subtractor = new BigDecimal(selectedItem.getPrice());
        totalFunds = totalFunds.subtract(subtractor);
        return "anything";

    }

    @Override
    public Item retreiveItem(String userVendingSelection) throws VendingMachineServiceException {
        return onlyItem;
    }

    @Override
    public void updateInventory(Item selectedItem) throws VendingMachineServiceException {
        Item foundItem = selectedItem;
        int quantity = Integer.valueOf(foundItem.getQuantity());
        quantity--;
        if (quantity == 0) {

        }
    }

    @Override
    public Change changeDispense() throws VendingMachineServiceInsufficentFundsException {
    

        return onlyCoins;
    }

    @Override
    public void shortFunds(Item foundItem) {
        String ItemCost = foundItem.getPrice();
        BigDecimal cost = new BigDecimal(ItemCost);
        totalFunds = totalFunds.subtract(cost);
        totalFunds.setScale(2, DOWN);

          }

    @Override
    public Change removeCoins() {
        Change empty = new Change();
        return empty;
    }

    @Override
    public BigDecimal getFunds() {
        return totalFunds;
    }

    @Override
    public Object getShortFunds() {
        return amountOfFundsNeeded;
    }

}
