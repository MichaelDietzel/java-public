/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.dao;

import com.mycompany.vendingmachine.model.Item;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Mike
 */
public class VendingMachineDaoStubImpl implements VendingMachineDao{
   
    private Map< String , Item> itemMap =  new HashMap<>();
    private DecimalFormat df = new DecimalFormat("0.00");
    Item onlyItem = new Item();
    public VendingMachineDaoStubImpl() {
    
    onlyItem.setItemNumber("1");
    onlyItem.setName("1");
    onlyItem.setQuantity("1");
    onlyItem.setPrice ("1");
    itemMap.put(onlyItem.getItemNumber(), onlyItem);
    }
    
    @Override
    public ArrayList getInventory() {       
     
        
        Collection<Item> c = itemMap.values();
        return new ArrayList(c);
    }

    @Override
    public void updateInventory(Item updateItem) {
    
    }

    @Override
    public Item getItem(String userVendingSelection) {
        
       return onlyItem;
    }
    
}
