/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.dao;

import com.mycompany.vendingmachine.model.Item;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Mike
 */
public class VendingMachineDaoImpl implements VendingMachineDao{
     private Map< String , Item> itemMap =  new HashMap<>();
     private boolean initialPull = false;
     private DecimalFormat df = new DecimalFormat("0.00");
     
    @Override
    public ArrayList getInventory()  {
        
    int quantity;
    String name;
    String itemNumber;
        
    if ( initialPull == false ) {
        for(int number=1; number<10; number++){
            //Quantity 
            Item itemToAdd = new Item();
            quantity = number;
            itemToAdd.setQuantity(Integer.toString(quantity));
            
            // name generator
            name = Integer.toString(number);
            itemToAdd.setName(name);
            
            // Cost generator
            double cost = number + (number *.05);
            BigDecimal price = new BigDecimal(Double.toString(cost)).setScale(2, RoundingMode.DOWN);
            String stringPrice = price.toString();
            itemToAdd.setPrice(stringPrice);
            
            
            // iterNumber generator
            itemNumber =  Integer.toString(number);
            itemToAdd.setItemNumber(itemNumber);
             itemMap.put(itemNumber, itemToAdd);
        }    
        initialPull = true;
    }
        Collection<Item> c = itemMap.values();
        return new ArrayList(c);
    }

    @Override
    public void updateInventory(Item updateItem) {
        itemMap.replace(updateItem.getItemNumber(), updateItem);

    }

    @Override
    public Item getItem(String userVendingSelection) {
       Item foundItem = itemMap.get(userVendingSelection);
       
       return foundItem;
    }
    
}
