/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.dao;


import com.mycompany.vendingmachine.model.Item;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author michael
 */
public interface VendingMachineDao {
    
    public ArrayList getInventory() ;

       
    public void updateInventory(Item updateItem);

    public Item getItem(String userVendingSelection);
    
    

}
