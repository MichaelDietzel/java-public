/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.model;

/**
 *
 * @author Mike
 */
public class Change {
    int Quarters;
    int Dimes;
    int Nickles;
    int Pennies;
    String Message;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public int getQuarters() {
        return Quarters;
    }

    public void setQuarters(int Quarters) {
        this.Quarters = Quarters;
    }

    public int getDimes() {
        return Dimes;
    }

    public void setDimes(int Dimes) {
        this.Dimes = Dimes;
    }

    public int getNickles() {
        return Nickles;
    }

    public void setNickles(int Nickles) {
        this.Nickles = Nickles;
    }

    public int getPennies() {
        return Pennies;
    }

    public void setPennies(int Pennies) {
        this.Pennies = Pennies;
    }
}
