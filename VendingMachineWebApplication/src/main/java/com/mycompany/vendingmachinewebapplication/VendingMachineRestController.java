/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *//**
package com.mycompany.vendingmachinewebapplication;

import com.mycompany.vendingmachine.old.VendingMachineDao;
import com.mycompany.vendingmachine.model.Change;
import com.mycompany.vendingmachine.model.Item;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@CrossOrigin 
@Controller
public class VendingMachineRestController {
    
    @Inject 
    private VendingMachineDao dao;
    public VendingMachineRestController(VendingMachineDao dao) {
        this.dao = dao;
    }
    
    @RequestMapping(value = "/money/{amount}/item/{id}", method = RequestMethod.GET)
    @ResponseBody
    public void VendItem(@PathVariable("id")long Id,
                         @PathVariable("amount")long amount,
                         @Valid
                         @RequestBody Change change){   
        dao.VendItem(change);  
}   
    @RequestMapping(value = {"/items"}, method = RequestMethod.GET)
    @ResponseBody
    public List<Item> getAllItems() {
    return dao.getAllItems();
}

}
**/