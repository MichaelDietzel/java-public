package com.mycompany.vendingmachinewebapplication;

import com.mycompany.vendingmachine.model.Item;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.mycompany.vendingmachine.dao.VendingMachineDao;
import com.mycompany.vendingmachine.model.Change;
import com.mycompany.vendingmachine.service.VendingMachineServiceException;
import com.mycompany.vendingmachine.service.VendingMachineServiceInsufficentFundsException;
import com.mycompany.vendingmachine.service.VendingMachineServiceLayer;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Controller
public class VendingMachineController {

    @Inject
    private VendingMachineDao dao;
    @Inject
    private VendingMachineServiceLayer service;

    public VendingMachineController(VendingMachineDao dao, VendingMachineServiceLayer service) {
        this.dao = dao;
        this.service = service;
    }

    public VendingMachineController() {
    }

    String itemId;
    BigDecimal displayMoney = new BigDecimal("0.00");
    String changeMessage;
    String outputMessage;
    Change coins = new Change();
    DecimalFormat df = new DecimalFormat("0.00");
    String quarters = "Quarters :";
    String dimes = " Dimes :";
    String nickles = " Nickles :";

    @RequestMapping(value = {"/display", "/"}, method = RequestMethod.GET)
    public String displayAllItems(HttpServletRequest request, Model model) {
        List<Item> itemList = dao.getInventory();
        model.addAttribute("itemList", itemList);
        model.addAttribute("itemId", itemId);
        model.addAttribute("displayMoney", df.format(displayMoney));
        model.addAttribute("changeMessage", changeMessage);
        model.addAttribute("outputMessage", outputMessage);

        return "VendingMachine";

    }

    @RequestMapping(value = "/addMoney", method = RequestMethod.POST)
    public String money(HttpServletRequest request) throws ItemIntegrityException, VendingMachineServiceInsufficentFundsException, VendingMachineServiceException {
        String addMoney = request.getParameter("money");
        int idAsInt;

        if (addMoney.equals("one")) {
            BigDecimal Dollar = new BigDecimal("1.00");
            displayMoney = service.addFunds(Dollar);
            changeMessage = " ";
            return "redirect:/display";

        }
        if (addMoney.equals("qrtr")) {
            BigDecimal Quarter = new BigDecimal("0.25");
            displayMoney = service.addFunds(Quarter);
            changeMessage = " ";
            return "redirect:/display";
        }
        if (addMoney.equals("dime")) {
            BigDecimal dime = new BigDecimal("0.10");
            displayMoney = service.addFunds(dime);
            changeMessage = " ";
            return "redirect:/display";
        }
        if (addMoney.equals("nickel")) {
            BigDecimal nickel = new BigDecimal("0.05");
            displayMoney = service.addFunds(nickel);
            changeMessage = " ";
            return "redirect:/display";
        }
        return "redirect:/display";
    }
    @RequestMapping(value = "/makeChange", method = RequestMethod.POST)
    public String MakePurchase(HttpServletRequest request) throws ItemIntegrityException, VendingMachineServiceInsufficentFundsException, VendingMachineServiceException {
        String currentMoney = request.getParameter("change");
  
        if (currentMoney.equals("returnMoney")) {
            itemId = " ";
            outputMessage = " ";
            coins = service.changeDispense();
            changeMessage =  quarters+String.valueOf(coins.getQuarters())+dimes+String.valueOf(coins.getDimes())+nickles+ String.valueOf(coins.getNickles());
            BigDecimal confirmZero = new BigDecimal("0.00");
            displayMoney = service.addFunds(confirmZero);
            return "redirect:/display";
        }
         return "redirect:/display";
    }
    @RequestMapping(value = "/makePurchase", method = RequestMethod.POST)
    public String makeChange(HttpServletRequest request) throws ItemIntegrityException, VendingMachineServiceInsufficentFundsException, VendingMachineServiceException {
        String currentMoney = request.getParameter("purchase");
        displayMoney.setScale(2, RoundingMode.DOWN);
        int idAsInt;
        if (currentMoney.equals("makePurchased")) {
            if (itemId.equals(" ")) {
                itemId = null;
            }
            if (itemId != null) {
                idAsInt = Integer.valueOf(itemId);
            } else {
                outputMessage = " Please Make a selection";
                return "redirect:/display";
            }
            if (idAsInt < 0 || idAsInt > 9) {
                outputMessage = " Number is too large or too small";
                return "redirect:/display";
            }
            if (idAsInt > 10 || idAsInt < 0) {
                outputMessage = " Number is out of range";
                return "redirect:/display";
            }
            Item foundItem = service.retreiveItem(Integer.toString(idAsInt));
            purchaseEvent(foundItem);
        }

        return "redirect:/display";
    }

    @RequestMapping(value = "/getItem", method = RequestMethod.POST)
    public String itemSelection(HttpServletRequest request) throws ItemIntegrityException, VendingMachineServiceInsufficentFundsException, VendingMachineServiceException {
        itemId = request.getParameter("itemSelection");
        return "redirect:/display";
    }

    private void purchaseEvent(Item foundItem) throws VendingMachineServiceInsufficentFundsException, VendingMachineServiceException {
        BigDecimal compareDisplayMoney = new BigDecimal("0.00");
        int foundItemQuantity = Integer.valueOf(foundItem.getQuantity());
        
        
        if (foundItemQuantity > 0) {
             compareDisplayMoney = service.subtractFunds(foundItem);
            compareDisplayMoney.setScale(2, RoundingMode.DOWN);
        }
            
        if (compareDisplayMoney.equals(displayMoney)) {
            changeMessage = " ";
            BigDecimal shortFunds = service.shortFunds(foundItem);
            outputMessage = " Please insert " + shortFunds;
        } else {
            outputMessage = service.updateInventory(foundItem);
            coins = service.changeDispense();
            changeMessage =  quarters+String.valueOf(coins.getQuarters())+dimes+String.valueOf(coins.getDimes())+nickles+ String.valueOf(coins.getNickles());
            displayMoney.equals(compareDisplayMoney);
            BigDecimal confirmZero = new BigDecimal("0.00");
            displayMoney = service.addFunds(confirmZero);
        }
    }

}
