package com.mycompany.vendingmachinewebapplication.controller;

import com.mycompany.vendingmachine.model.Item;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.mycompany.vendingmachine.dao.VendingMachineDao;
import com.mycompany.vendingmachine.model.Change;
import com.mycompany.vendingmachine.service.VendingMachineServiceException;
import com.mycompany.vendingmachine.service.VendingMachineServiceInsufficentFundsException;
import com.mycompany.vendingmachine.service.VendingMachineServiceLayer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
public class VendingMachineController {

    @Inject
    private VendingMachineDao dao;
    @Inject
    private VendingMachineServiceLayer service;

    public VendingMachineController(VendingMachineDao dao, VendingMachineServiceLayer service) {
        this.dao = dao;
        this.service = service;
    }

    public VendingMachineController() {
    }

    String itemId;
    BigDecimal displayMoney = new BigDecimal("0.00");
    String outcomeMessage;
    Change coins = new Change();
    DecimalFormat df = new DecimalFormat("0.00");
    
    
    
    
    @RequestMapping(value = {"/display", "/"}, method = RequestMethod.GET)
    public String displayAllItems(HttpServletRequest request, Model model) {
        List<Item> itemList = dao.getInventory();

        model.addAttribute("itemList", itemList);
        model.addAttribute("itemId", itemId);
        model.addAttribute("displayMoney", df.format(service.getFunds()));
        model.addAttribute("outcomeMessage", outcomeMessage);
        model.addAttribute("coins", coins);
        model.addAttribute("shortChange",df.format(service.getShortFunds()));

        coins = service.removeCoins();
        outcomeMessage = "NA";
        return "VendingMachine";

    }

    @RequestMapping(value = "/addMoney", method = RequestMethod.POST)
    public String money(HttpServletRequest request) throws ItemIntegrityException, VendingMachineServiceInsufficentFundsException, VendingMachineServiceException {
        String addMoney = request.getParameter("money");

        if (addMoney.equals("one")) {
            BigDecimal Dollar = new BigDecimal("1.00");
            service.addFunds(Dollar);
            return "redirect:/display";

        }
        if (addMoney.equals("qrtr")) {
            BigDecimal Quarter = new BigDecimal("0.25");
            service.addFunds(Quarter);
            return "redirect:/display";
        }
        if (addMoney.equals("dime")) {
            BigDecimal dime = new BigDecimal("0.10");
            service.addFunds(dime);
            return "redirect:/display";
        }
        if (addMoney.equals("nickel")) {
            BigDecimal nickel = new BigDecimal("0.05");
            service.addFunds(nickel);
            return "redirect:/display";
        }
        return "redirect:/display";
    }

    @RequestMapping(value = "/makeChange", method = RequestMethod.POST)
    public String makeChange(HttpServletRequest request) throws ItemIntegrityException, VendingMachineServiceInsufficentFundsException, VendingMachineServiceException {
        itemId = " ";
        outcomeMessage = " ";
        coins = service.changeDispense();
        return "redirect:/display";

    }

    @RequestMapping(value = "/makePurchase", method = RequestMethod.POST)
    public String makePurchase(HttpServletRequest request) throws ItemIntegrityException, VendingMachineServiceInsufficentFundsException, VendingMachineServiceException {
        Item foundItem = new Item();
        try {
            foundItem = service.retreiveItem(itemId);
        } catch (VendingMachineServiceException ex) {
            outcomeMessage = "problem with itemId input";
        }
        purchaseEvent(foundItem);

        return "redirect:/display";
    }

    @RequestMapping(value = "/getItem", method = RequestMethod.POST)
    public String itemSelection(HttpServletRequest request) throws ItemIntegrityException, VendingMachineServiceInsufficentFundsException, VendingMachineServiceException {
        itemId = request.getParameter("itemSelection");
        return "redirect:/display";
    }

    public void purchaseEvent(Item foundItem) throws VendingMachineServiceInsufficentFundsException {

        outcomeMessage = service.subtractFunds(foundItem);
        coins = service.changeDispense();

    }
}
