/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachinewebapplication;

/**
 *
 * @author Mike
 */
public class ItemIntegrityException extends Exception {
 
    public ItemIntegrityException(String message) {
        super(message);
    }
}
