/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.service;

import com.mycompany.vendingmachine.dao.VendingMachineDao;
import com.mycompany.vendingmachine.dao.VendingMachineDaoStubImpl;
import com.mycompany.vendingmachine.model.Change;
import com.mycompany.vendingmachine.model.Item;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author michael
 */
public class VendingMachineServiceImplTest {
 
   private VendingMachineServiceLayerStubImpl myService; 
 
    public VendingMachineServiceImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws Exception {
             ApplicationContext ctx = 
                 new ClassPathXmlApplicationContext("test-applicationContext.xml");
          myService =  ctx.getBean("VendingMachineServiceLayer", VendingMachineServiceLayerStubImpl.class);
        

    }

    @After
    public void tearDown() {
    }


    @Test
    public void addFunds() throws Exception {
        BigDecimal money = new BigDecimal("1.00");
        myService.addFunds(money);
        BigDecimal addedMoney = myService.getFunds();
        
        assertEquals(money, addedMoney);
    }

    @Test
    public void subtractFunds() throws Exception {
        
        BigDecimal moneyMore = new BigDecimal("100.00"); 
        BigDecimal compare = new BigDecimal("99.00");
        
        Item testItem = new Item();
        
        testItem.setItemNumber("a1");
        testItem.setName("food");
        testItem.setQuantity("1");
        testItem.setPrice("1.00");
      
        
        myService.addFunds(moneyMore);
        myService.subtractFunds(testItem);
        moneyMore = myService.getFunds();
        
        assertEquals(moneyMore,compare);
    }


    @Test
    public void retreiveItem() throws Exception {
       Item onlyItem = myService.retreiveItem("1");
       assertEquals(onlyItem.getItemNumber(),"1"); 
    }
    
    @Test
    public void updateInventory() throws Exception {
        Item onlyItem = new Item();
        
        onlyItem.setItemNumber("1");
        onlyItem.setName("1");
        onlyItem.setPrice("1");
        onlyItem.setQuantity("1");
        
   
        assertEquals( "1",onlyItem.getQuantity());    
    }

    @Test
    public void changeDispense() throws Exception {
        Change changed = myService.changeDispense();
        
         assertEquals(changed.getDimes() ,1); 
    }

    @Test
    public void shortFunds() throws Exception {
 
    }

}
