/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.dao;

import com.mycompany.vendingmachine.dao.VendingMachineDao;
import com.mycompany.vendingmachine.dao.VendingMachineDaoStubImpl;
import com.mycompany.vendingmachine.model.Item;
import com.mycompany.vendingmachine.service.VendingMachineServiceLayerStubImpl;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author michael
 */
public class VendingMachineDaoImplTest {

     private  VendingMachineDaoStubImpl myDao;
 
    public VendingMachineDaoImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws Exception {
             ApplicationContext ctx = 
                 new ClassPathXmlApplicationContext("test-applicationContext.xml");
          myDao =  ctx.getBean("VendingMachineDao", VendingMachineDaoStubImpl.class);
        

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of individualOrderLocator method, of class FloorMasteryDaoFileImpl.
     */
    @Test
    public void VendingMachineDao() throws Exception {
   
    }

    @Test
    public void testGetInventory() throws Exception {
      List<Item> itemList =   myDao.getInventory();
      
      assertEquals(itemList.size(), 1);
    }

    /**
     * Test of allOrders method, of class FloorMasteryDaoFileImpl.
     */
    @Test
    public void getItem() throws Exception {
       
     Item foundItem = myDao.getItem("1");
     assertEquals(foundItem.getItemNumber(), "1");
    }


}
