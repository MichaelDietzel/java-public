/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.assignment1;

/**
 *
 * @author apprentice
 */
public class Allthemaths {
    public static void main(String []args){
        System.out.print("1+2 is : ");
        System.out.println(1 + 2);
        
        System.out.print("42001 modulus 5 is :");
        System.out.println(42000 % 5);
        
        System.out.print("5565.0 divided by 22.0 :");
        System.out.println(5565 / 22);
        
        System.out.print("223 times 31 -42 : ");
        System.out.println(223 * 31 -42);
        
        System.out.print(" is 4 greater than -1 ?");
        System.out.println(4 > -1);
    
        System.out.println("\n****** Now make the computer do harder math");
       
        System.out.print(" 8043.52 minus 4.2 plus 23.0 divided by 5 :");
        System.out.println( (((8043.52 -4.2)+23.0))/5); 
        // This forces the  equation to occur in the manner it was printed online
        
        System.out.print("11111 modulus 3 minus 67  minus 1 + 9 :");
        System.out.println((11111 % 3)-67 - 1 +9 ); 
        
        System.out.print("44 minus 22 minus 11 minus 66 minus 88 minus 76 minus 11 minus 33 is :");
        System.out.println(44 - 22 - 11 - 66 - 88 - 76 - 11 - 33);
        
        System.out.print(" 22 times 3 minus 1 plus 4 times 6 :");
        System.out.println( 22 * 3 -1 + 4 * 6 );
        
        System.out.print(" Is 67 greather than 4 * 5? :");
        System.out.println( 67 > (4 *5 ) );   
        
        System.out.print(" Is 78 greather than 4 * 5? :" );
        System.out.println( 78 > (4 *5 ) );  
        
        System.out.print(" other  answer  see inline comment :");
        System.out.println( "excludes  decimal  portion of the answers");
        /** the system simply reads the lack of a decimal as place to stop.
        *   the reality is that the information is still held in memory just not
        *   displayed 
        */
    }
    
}
