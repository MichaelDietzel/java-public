
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class IntrestRateCalculator {
    @SuppressWarnings("empty-statement")
    public static void main( String [] Args){
        
        //scanner 
        Scanner inputReader = new Scanner(System.in);
        
        //variable declaration
        int investmentYears ;
        double interestRate ;
        double principleInvestmentEndOfYear= 0;
        double principleInvestmentBegOfYear ;
        double netProfit ;
        double currentBalance ;
        double compoundOccurances ;
        int frequency ;
        int intervalCounter;
        int yearOutput = 0;
        
        
        // string 
        String userYear;
        String userRate;
        String userInvestment;
                
        //inputs
        System.out.println("How many years will the investment be inliquid :");
            userYear = inputReader.nextLine();
            //parse
            investmentYears = parseInt(userYear);
        
        System.out.println("What is the intrest rate of the investment :");
            userRate = inputReader.nextLine();
            //parse
            interestRate = parseDouble(userRate);
            interestRate = (interestRate / 100);
            
            
        System.out.println("What is the principle :");
            userInvestment = inputReader.nextLine();
            //parse
            principleInvestmentBegOfYear = parseDouble(userInvestment);
            currentBalance = principleInvestmentBegOfYear;

        System.out.println("How often is intrest accrued 1) daily  2) monthly 3) quarterly ");
            frequency = inputReader.nextInt();
            // Coumpound decider
            if       (frequency == 1){
                     interestRate = interestRate / 365;
                     compoundOccurances = 365;
            }else if (frequency == 2){
                     interestRate = interestRate /12;  
                     compoundOccurances = 12;
            }else {
                     interestRate = interestRate/4;
                     compoundOccurances = 4;
            }
                     
        
       //calculations intrest accured
        for (;investmentYears> 0;  investmentYears --){         
            yearOutput ++;
                  // Differnt compound periods
                    for(intervalCounter = 0; intervalCounter != compoundOccurances; intervalCounter ++)  {
                         currentBalance = currentBalance * (1 + interestRate);
                      }

                   // output 
                   System.out.println(" End of year : " + yearOutput);
                   System.out.println(" Principle at the start of year  " + principleInvestmentBegOfYear);
                   netProfit = currentBalance - principleInvestmentBegOfYear;
                   System.out.println(" This years profit  :" + netProfit);
                   principleInvestmentBegOfYear = currentBalance;
        }             
    }
}