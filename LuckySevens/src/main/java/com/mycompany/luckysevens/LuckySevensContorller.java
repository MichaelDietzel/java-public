/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.luckysevens;

import static com.fasterxml.jackson.databind.node.DecimalNode.ZERO;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Mike
 */
@Controller
public class LuckySevensContorller {

    private int totalRollsInt = 1;
    String totalRolls;
    private int highetMoneyRoll;
    BigDecimal highestMoney = new BigDecimal("0.00").setScale(2, RoundingMode.DOWN);
    String money;

    @RequestMapping(value = "/rollDice", method = RequestMethod.POST)
    public String mainMethod(HttpServletRequest request, Map<String, Object> model) {
        money = request.getParameter("moneyToburn");
        BigDecimal moneyForCalculation = new BigDecimal(money).setScale(2, RoundingMode.DOWN);
        BigDecimal zero = new BigDecimal("0.00");
        BigDecimal one = new BigDecimal("1.00");
        BigDecimal four = new BigDecimal("4.00");

        Random dice = new Random();
        highestMoney = moneyForCalculation;

        int highestMoneyComparision;
        int comparisionOutOfMoney = moneyForCalculation.compareTo(zero);

        while (comparisionOutOfMoney > 0) {
            int pickedNumberOne = dice.nextInt(5) + 1;
            int pickedNumberTwo = dice.nextInt(5) + 1;
            int totalPicked = pickedNumberOne + pickedNumberTwo;
  
            if (totalPicked == 7) {
               moneyForCalculation = moneyForCalculation.add(four);
            } else {
               moneyForCalculation = moneyForCalculation.subtract(one);
            }

            highestMoneyComparision = highestMoney.compareTo(moneyForCalculation);
            if (highestMoneyComparision < 0) {
                highestMoney = moneyForCalculation;
                highetMoneyRoll = totalRollsInt;
            }
            totalRollsInt++;
           
            comparisionOutOfMoney = moneyForCalculation.compareTo(zero);
        }
        model.put("money", money);
        model.put("totalRollsInt", totalRollsInt);
        model.put("highetMoneyRoll", highetMoneyRoll);
        model.put("highestMoney", highestMoney);
        return "results";
    }

}
