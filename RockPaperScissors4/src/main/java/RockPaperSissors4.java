
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class RockPaperSissors4 {

    public static void main(String[] args) {
        // Scanner and Random
        Scanner newInput = new Scanner(System.in);
        Random randomizer = new Random();

        // array
        String[] TieWinLossArray;
        TieWinLossArray = new String[11];

        // Variabiles
        int userInput = 0;
        int AiSelection = 0;
        int outcome = 0;
        int roundChoice = 0;
        int gameChoice = 0;

        // Nonreset variables
        int match = 1;

        String userChoice;
        String AiChoice;
        while (gameChoice == 0) {
            while (roundChoice == 0) {
                System.out.println(" How many rounds would you like to play 1-10");
                roundChoice = newInput.nextInt();
                // good idea bad idea?
                roundChoice = printErrorMessage(roundChoice);

            }
            System.out.println("This is match :" + match);
            for (int roundNumber = 1; roundChoice >= roundNumber; roundNumber++) {
                //input
                System.out.println("This is round" + roundNumber + " Of " + roundChoice);
                System.out.println("Please select : 1) rock 2) paper 3) Scissors ");
                userInput = newInput.nextInt();
                AiSelection = randomizer.nextInt(2) + 1;

                outcome = decideOutcome(userInput, AiSelection);

                if (outcome == 0) {
                    userChoice = calculateOutput(userInput);
                    AiChoice = calculateOutput(AiSelection);
                    TieWinLossArray[roundNumber] = "Tie";
                    System.out.println("Tie");
                    System.out.println("You chose " + userChoice + " and Ai chose " + AiChoice);
                } else if (outcome == 1) {
                    userChoice = calculateOutput(userInput);
                    AiChoice = calculateOutput(AiSelection);
                    TieWinLossArray[roundNumber] = "Win";
                    System.out.println("Win!");
                    System.out.println("You chose" + userChoice + " and Ai chose " + AiChoice);
                } else if (outcome == -1) {
                    userChoice = calculateOutput(userInput);
                    AiChoice = calculateOutput(AiSelection);
                    TieWinLossArray[roundNumber] = "loss";
                    System.out.println("loss");
                    System.out.println("You chose " + userChoice + " and Ai chose " + AiChoice);
                } else {
                    userChoice = calculateOutput(userInput);
                    AiChoice = calculateOutput(AiSelection);
                    TieWinLossArray[roundNumber] = "invalid";
                    System.out.println("loss");
                    System.out.println("You chose invalid and Ai chose " + AiChoice);
                }
            }
            matchResultsPrint(TieWinLossArray, roundChoice);
            gameChoice = runProgramAgain(gameChoice);
            if (gameChoice == 0) {
                userInput = variableReset(userInput);
                AiSelection = variableReset(AiSelection);
                outcome = variableReset(outcome);
                roundChoice = variableReset(roundChoice);

                match++;

            }
        }

    }
// 1 rock  2 paper 3 scisiors             

    private static int decideOutcome(int user, int Ai) {
        int outcome = -10;
        if (user == Ai) {
            outcome = 0;
        } else if (user == 1 && Ai == 3) {
            outcome = 1;
        } else if (user == 1 && Ai == 2) {
            outcome = -1;
        } else if (user == 2 && Ai == 1) {
            outcome = 1;
        } else if (user == 2 && Ai == 3) {
            outcome = -1;
        } else if (user == 3 && Ai == 1) {
            outcome = -1;
        } else if (user == 3 && Ai == 2) {
            outcome = 1;
        }

        return outcome;
    }

    // not needed and probably a better way of doing this but why not get more practice.
    private static String calculateOutput(int selection) {
        String selected = "initalized";
        switch (selection) {
            case 1:
                selected = "rock";
                break;
            case 2:
                selected = "paper";
                break;
            case 3:
                selected = "scissors";
                break;
            default:
                break;
        }
        return selected;
    }

    private static int printErrorMessage(int roundChoice) {
        if (roundChoice == 0 || roundChoice >= 11) {
            System.out.println(" Invalid choice pick a number one to ten");
            roundChoice = 0;
        }
        return roundChoice;
    }

    private static void matchResultsPrint(String[] TieWinLossArray, int roundChoice) {

        System.out.println("Here is the outcomes :");
        for (int roundNumber = 1; roundChoice >= roundNumber; roundNumber++) {
            System.out.print(" " + roundNumber + ")" + TieWinLossArray[roundNumber]);
        }
    }

    public static int runProgramAgain(int gameChoice) {
        Scanner newInput = new Scanner(System.in);

        int input;

        System.out.println("Did you want to play again?  1= Yes , 2 = No:");
        input = newInput.nextInt();
        if (input == 1) {
            System.out.println(" Sure starting a new round. ");
        } else {
            System.out.println(" Thanks for playing! ");
            gameChoice = 1;
        }

        return gameChoice;
    }

    //maybe there is a better way?
    private static int variableReset(int input) {
        input = 0;

        return input;
    }

}
