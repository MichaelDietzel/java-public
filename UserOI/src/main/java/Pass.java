
import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class Pass implements UserOI {

    public String prompt;

    @Override
    public double readDouble(String prompt) {
        double value;
        Scanner userInput = new Scanner(System.in);

        System.out.println(" Please enter a double");
        prompt = userInput.nextLine();
        value = Double.parseDouble(prompt);

        return value;
    }

    @Override
    public double readDouble(String prompt, double min, double max) {
        int compare = 0;
        double value;
        Scanner userInput = new Scanner(System.in);

        this.prompt = prompt;
        value = Double.parseDouble(prompt);

        System.out.println(" Please enter a number between " + min + "and" + max);

        if (value >= min && value <= max) {
            compare = 1;
            System.out.println(" Good pick");
        }
        while (compare != 1) {
            System.out.println("Please try again");
            value = readDouble(prompt);
            if (value > min && value < max) {
                compare = 1;
                System.out.println(" Good pick");
            }
        }

        return value;
    }

    @Override
    public float readFloat(String prompt) {
        float value;
        Scanner userInput = new Scanner(System.in);

        System.out.println(" Please enter a Float");
        String userInputDouble = userInput.nextLine();
        value = parseFloat(userInputDouble);

        return value;
    }

    @Override
    public float readFloat(String prompt, float min, float max) {
        int compare = 0;
        float value;
        Scanner userInput = new Scanner(System.in);

        this.prompt = prompt;
        value = Float.parseFloat(prompt);

        System.out.println(" Please enter a number between " + min + "and" + max);

        if (value >= min && value <= max) {
            compare = 1;
            System.out.println(" Good pick");
        }
        while (compare != 1) {
            System.out.println("Please try again");
            value = readFloat(prompt);
            if (value > min && value < max) {
                compare = 1;
                System.out.println(" Good pick");
            }
        }
        return value;
    }

    @Override
    public int readInt(String prompt) {
        int value;
        Scanner userInput = new Scanner(System.in);

        System.out.println(" Please enter a Int");
        String userInputDouble = userInput.nextLine();
        value = parseInt(userInputDouble);
        return value;
    }

    @Override
    public int readInt(String prompt, int min, int max) {
        int compare = 0;
        int value;
        Scanner userInput = new Scanner(System.in);

        this.prompt = prompt;
        value = parseInt(prompt);

        System.out.println(" Please enter a number between " + min + "and" + max);

        if (value >= min && value <= max) {
            compare = 1;
            System.out.println(" Good pick");
        }
        while (compare != 1) {
            System.out.println("Please try again");
            value = readInt(prompt);
            if (value > min && value < max) {
                compare = 1;
                System.out.println(" Good pick");
            }
        }
        return value;
    }

    @Override
    public long readLong(String prompt) {
        long value;
        Scanner userInput = new Scanner(System.in);

        System.out.println(" Please enter a double");
        String userInputDouble = userInput.nextLine();
        value = Long.parseLong(userInputDouble);

        return value;
    }

    @Override
    public long readLong(String prompt, long min, long max) {
        int compare = 0;
        int value;

        this.prompt = prompt;
        value = parseInt(prompt);

        System.out.println(" Please enter a number between " + min + "and" + max);

        if (value >= min && value <= max) {
            compare = 1;
            System.out.println(" Good pick");
        }
        while (compare != 1) {
            System.out.println("Please try again");
            value = readInt(prompt);
            if (value > min && value < max) {
                compare = 1;
                System.out.println(" Good pick");
            }
        }
        return value;
    }

    @Override
    public String readString(String prompt) {
        System.out.println("All values passed");
        this.prompt = "Prompt completed";
        return prompt;
    }

    @Override
    public void print(String message) {

        readString(message);
        System.out.println(prompt);
    }

}
