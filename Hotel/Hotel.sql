
DROP DATABASE 
if EXISTS hotel;

CREATE DATABASE hotel;


Create table hotel.Hotel_Org
(
  hotelRollupOrg int(6) not null,
  hotelOrgCode int (6) not null, 
  hotelEmployeeNameFull VarChar (30) not null,
  hotelIdNumber int(6) not null,
  hotelLevelOneManager int(6) not null,
  hotelEmployeeID int (6) not null,
  hotelLevelTwoManager int(6)  null,
  hotelLevelThreeManger int (6)  null,
  hotelLevelFourManager int (6)  null,
  hotelLevelFiveManager int (6)  null,
  hotelLevelSixmanager int (6) Null,
  hotelLevelSevenmanager int (6)  Null,
  hotelLevelEightManager int (6)  Null,
  hotelRepIndicator int (1) not null,
  hotelJobName varChar(30) not null, 
  primary key (hotelEmployeeID)
);

create table hotel.Purchases
(
 PurcahsingDate date not null,
 purchaserId int(6) not null, 
 purchaserJobName varChar(30) not null, 
 towels int null, 
 soap  int null,
 ect int null, 
 primary key (PurcahsingDate)
);


Create table hotel.Hotel_Physical
(
 
 dateStamp date not null,
 hotelIdNumber int not null, 
 totalavailableRooms int not null,
 totaloccupiedRooms int not null,
 openIndicator int not null, 
 hotelzipcode int (5) not null,
 htelAddress longtext not null,
 index dateStamp(dateStamp),
 primary key(hotelIdNumber)
);

Create table hotel.Hotel_rooms
(
	hotelRoomIdNumberInternal INT NOT NULL auto_increment,
    hotelIdNumber int not null, 
    hotelRoomNumber int not NULL,
    roomIdConnected1 int  Null,
    roomIdConnected2 int  null, 
    roomIdConnected3 int  Null,
    hotelOccupancyLimit int not Null,
    hotelRoomFloor 	int not Null,
    hotelRoomFloorNumber int not Null, 
    hotelRoomFeatures   longtext not null,
    hotelRoomSpaIndicator int Null,
    hotelRoomMiniBarIndicator int Null,
    hotelRoomFridgeIndicator int Null,
    hotelRoomIndicator int Null,
	hotelRoomBedString VarChar(30)not null,
    hotelBedKingIndicator int Null,
    hotelBedDoubleIndicator int Null,
    hotelBedSingleIndicator int Null,
    hotelRoomBaseCost int not Null,
    hotelRoomBedCount int not Null Default 1,
    hotelBathRoomCount int not Null Default 1,
    hotelRoomFloorOccupied int not Null,
    RoomOccupancyindicator int not null Default 0, 
    RoomOccupancyBeginDate date not null,
    RoomOccupancyEndDate date not null,
	PRIMARY KEY (hotelRoomIdNumberInternal)
) ;

Create table hotel.Hotel_Reservation_Details(
	customerReservationId INT NOT NULL auto_increment,
    customerReservationTimeStamp dateTime not null,
    customerId int not null,
	customerMessage LONGTEXT null,
    customerFirstName varChar(30) not null,
    customerLastName varChar(30) not null,
    customerOrganizationIndicator int not null,
    customerOrganizationName longtext null,
    customerDOB  date not null, 
    customerReservationDate date not null,
	customerWantSpaIndicator int Null,
    customerWantMinibarIndicator int Null,
    customerWantFridgeIndicator int Null,
    custoemrRoomsRequested int Null,
    customerRoomAdjacencyIndicator int Null, 
    customerDateOfArival date not null,
    customerDateOfDeparture date not null,
    customerPromoCode varChar(30) null,	
    primary key(customerReservationId) 
);

Create table  hotel.Customer_add_ons (
	customerOrderId int not null auto_increment,
    hotelRoomIdNumberInternal int not null,
    EmployeeIdUsedtoProvide int not null,
    customerRoomNumber int not null,
	customerTVAddOn int null,
    costOfTVAddOn double not null,
    customerFoodAddOn int null,
    costOfFoodAddOn double not null,
    customerCustom int null,
    costOfCustomAddOn double not null,
    customerDamage int null,
    costOfDamageAddOn double not null,
    customerConfrenceRoom double null, 
    costOfConfrenceRoomAddOn double not null, 
	primary key(customerOrderId)
);

Create table hotel.customerDataTable (
	customerID int not null auto_increment,
	customerFirstName varChar(30) not null,
    customerLastName varChar(30) not null,
    customerEmail varChar(100) not Null,
    customerPhoneNumber double not null,
    customerCellPhone double null,
    customerPromoCode int not null,
    primary key(customerId) 
);

create table hotel.Billing_external(
	customerID int not null,
    customerReservationId int not null,
    customerPromoCode  varChar(30) null,
    customerRoomCost decimal null,
    paidIndicator int not null default 0,
    customerOrdierId int not null,
    totalCostofAddonID int not null,
    
    primary key (customerID)
);