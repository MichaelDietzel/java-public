DROP DATABASE 
if EXISTS hotel2;

CREATE DATABASE hotel2;

/* start Customer
	PK TaxId
    FK
   */ 
create table hotel2.Customer(
customerID int not null auto_increment,
firstName  varchar(30) not null,
middleName varChar(30) not null,
lastName varChar(30) not null,
age int not null, 
reservationId int not null, 
primary key(CustomerId)
);
/* start Guests 
	PK GuestId
    FK reservationid
   */ 
create table hotel2.Guests(
GuestId int not null auto_increment,
FirstName varChar(30) not null,
MiddleName varChar(30) not Null,
lastName varChar(30) not null,
age int not null,
reservationid int not null,
primary key (GuestId)
);
/* start Rooms
PK RoomNumber 
DK RoomTypeID
*/
Create table hotel2.Rooms(
RoomNumber int not null,
RoomTypeId int not null,
floor int not null,
OccupancyMaximum int not null, 
primary key (RoomNumber)
);
/* start room price 
	PK RoomPriceId
    FK RoomPrice, taxId
   */ 
create table hotel2.RoomsPrice(
RoomPriceId int not null,
RoomNumber int not null,
RoomPrice float not null,
fromDate date not null,
toDate date not null,
taxId date not null, 
primary key (RoomPriceID)
);
/* start room Type 
	PK RoomTypeID
    FK
   */ 
Create table hotel2.RoomTypes(
roomTypeID int not null auto_increment,
roomTypeDescription longtext not null,
primary key(RoomTypeID)
);
/* start room Amentities 
	PK RoomNumber
    FK
   */ 
create table hotel2.RoomAmenities(
RoomNumber int not null,
AmenityId int not null,
primary key(RoomNumber)
);
/* start AmenitiesDetails
	PK AmentityId
    FK PriceId
   */ 
create table hotel2.AmenitiesDetails (
AmentityId int not null,
AmentityName varChar(20) not null,
PriceId  int not null,
primary key(AmentityId)
);
/* start AddonRoomServices 
	PK ServiceID
    FK
   */ 
create table hotel2.AddonRoomServices(
 ServiceID int not null auto_increment,
 ServiceName varChar(20) not null,
 ServiceDescription longtext not null, 
 primary key(ServiceID)
);
/* start AddonAssociationTable
	PK ServiceID
    FK
*/
create table hotel2.AddonReservationTable(
ReservationID int not null,
ServiceID int not null, 
purchaseDate date not null,
primary key (ServiceID)
);
/* start AddonPrices 
	PK AddonPriceID
    FK ServiceId , TaxId
   */ 
create table hotel2.AddonPrices(
AddonPriceID int not null auto_increment,
FromDate date not null,
ToDate date not null,
AddonPrice float not null,
TaxId int not null,
primary key(addonPriceID)
);
/* start Taxes  
	PK RoomNumber
    FK RoomTypeId
   */
create table hotel2.Taxes(
TaxId int not null auto_increment,
Amount float not null, 
Description longtext,
primary key(TaxId)
);
/* start ServiceOrder
	PK serviceOrderID
    FK roomNumber
   */ /*
create table hotel2.ServiceOrder (
serviceOrderID int not null auto_increment,
roomNumber int not null,
orderDate date not null,
Primary Key(serviceOrderID)
);
/* start ServiceOrderDetails
	PK ServiceOrderIDDetails
    FK RoomNumber
   *//* 
create table hotel2.ServiceOrderDetails(
ServiceOrderDetailsID int not null auto_increment,
ServiceOrderID int not null,
RoomNumber int not null,
OrderDate date not null,
orderRequest varchar(20) not null,
orderPrice float not null,
primary key(ServiceOrderDetailsID)
);*/
/* start reservations
	PK reservationID
    FK promationCode customerID
   */ 
create table hotel2.reservations(
reservationID int not null,
customerID int not null,
StartDate date not null,
EndDate date not null,
promationCode int null, 
primary key (reservationID)
);
/* start reservationDetail
	PK ReservationDetailID
    FK ReservationID RoomNumber
   */ 
create table hotel2.reservationDetail(
ReservationDetailID int not null, 
ReservationID int not null,
RoomNumber int not null,
StartDate date not null,
EndDate date not null, 
Primary key (reservationID)
);
/* start Promotions
	PK promotionID
    FK
   */ 
create table hotel2.promotions(
promotionID int not null  auto_increment,
promotionCodeAmount float not null,
primary key (promotionID)
);
/* start hote2.promotionCodes
	PK PromotionCode
    FK promotionID
   */ 
create table hotel2.promotionCodes(
promotionCode int not null,
PromotionStartDate date not null,
PromtionEndDate date not null,
promotionID int not null,
primary key(PromotionCode)
);