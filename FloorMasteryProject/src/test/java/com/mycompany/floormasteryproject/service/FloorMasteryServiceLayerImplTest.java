/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.service;

import com.mycompany.floormasteryproject.dto.Order;
import com.mycompany.floormasteryproject.dto.Products;
import com.mycompany.floormasteryproject.dto.Tax;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class FloorMasteryServiceLayerImplTest {

    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
    FloorMasteryServiceLayer myService = ctx.getBean("myService", FloorMasteryServiceLayer.class);

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of findOrder method, of class floormasteryServiceLayerImpl.
     */
    @Test
    public void testFindOrder() throws Exception {
        Order activeOrder = new Order();
        List<Order> forSize = myService.findOrderDate(activeOrder);
        assertEquals(1, forSize.size());
    }

    /**
     * Test of addAnOrder method, of class floormasteryServiceLayerImpl.
     */
    @Test
    public void testAddAnOrder() throws Exception {
        BigDecimal decimalString = new BigDecimal("200");
        Products onlyProduct = new Products();
        Tax onlyTax = new Tax();
        Order onlyItem = new Order();
        onlyTax.setState("notAState");
        onlyTax.setTaxRate(decimalString);

        onlyProduct.setCostperSquareFoot(decimalString);
        onlyProduct.setLaborCostperSquareFoot(decimalString);
        onlyProduct.setProduct("not a Product");

        onlyItem.setArea(decimalString);
        onlyItem.setCustomerName(",,,,,,,");
        onlyItem.setFinalLaborCosts(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setFinalTotal(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setMaterialCost(decimalString);
        onlyItem.setOrderNumber(1);
        onlyItem.setProductComponent(onlyProduct);
        onlyItem.setTaxComponent(onlyTax);

        Order addedItem = myService.addAnOrder(onlyItem);

        assertEquals(",,,,,,,", addedItem.getCustomerName());
        assertEquals(2, addedItem.getOrderNumber());

    }

    /**
     * Test of findAllOrder method, of class floormasteryServiceLayerImpl.
     */
    @Test
    public void testReplaceOrder() throws Exception {
        Order passedOrder = new Order();
        List<Order> countTheseOrders = myService.findAllOrder(passedOrder);

        assertEquals(1, countTheseOrders.size());
    }

    /**
     * Test of findAndRemove method, of class floormasteryServiceLayerImpl.
     */
    @Test
    public void testFindAndRemove() throws Exception {
        Order passOrder = new Order();
        passOrder.setOrderNumber(1);
        List<Order> countTheseOrders = myService.findAllOrder(passOrder);
        myService.findAndRemoveOrder(passOrder);
        List<Order> countAgain = myService.findAllOrder(passOrder);

        assertEquals(1, countAgain.size());
    }

    /**
     * Test of saveChanges method, of class floormasteryServiceLayerImpl.
     */
    @Test
    public void testSaveChanges() throws Exception {
        Order activeOrder = new Order();
        activeOrder.setOrderNumber(1);

        myService.saveChanges(activeOrder);

    }

    /**
     * Test of findOrderForEdit method, of class floormasteryServiceLayerImpl.
     */
    @Test
    public void testFindOrderForEdit() throws Exception {
        Order activeOrder = new Order();
        activeOrder.setOrderNumber(1);
        activeOrder.setCustomerName("%%%%%%%");

        Order orderToCompare = new Order();

        orderToCompare = myService.findAndEditOrder(activeOrder);

        int newINt = 0;


    }

    @Test
    public void OrderNumberScenarioOrderScenario() throws Exception {
        Order activeOrder = new Order();
        activeOrder.setOrderNumber(1);
        activeOrder.setCustomerName("%%%%%%%");
        Order orderToCompare = new Order();

        orderToCompare = myService.findAndEditOrder(activeOrder);

        int newINt = 0;

        assertEquals(activeOrder.getOrderNumber(), orderToCompare.getOrderNumber());
    }

    @Test
    public void testsProductList() throws Exception {
        List<Products> allProducts = myService.getProductList();
        int listSize = allProducts.size();

        assertEquals(listSize, 1);

    }

    @Test
    public void testTaxList() throws Exception {

        List<Tax> allTax = myService.getTaxsList();
        int listSize2 = allTax.size();

        assertEquals(listSize2, 1);

    }

    public void testTax() throws Exception {

        List<Tax> allTax = myService.getTaxsList();
        int listSize2 = allTax.size();

        assertEquals(listSize2, 1);

    }
}
