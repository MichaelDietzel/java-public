/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.dao;

import com.mycompany.floormasteryproject.dto.Order;
import com.mycompany.floormasteryproject.dto.Products;
import com.mycompany.floormasteryproject.dto.Tax;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author michael
 */
public class FloorMasteryDaoFileImplTest {

    FloorMasteryDao myDao = new FloorMasteryDaoStubImpl();

    public FloorMasteryDaoFileImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws Exception {
        int orderValue = 0;
        BigDecimal decimalString = new BigDecimal("200");
        Products onlyProduct = new Products();
        Tax onlyTax = new Tax();
        Order onlyItem = new Order();
        onlyTax.setState("notAState");
        onlyTax.setTaxRate(decimalString);

        onlyProduct.setCostperSquareFoot(decimalString);
        onlyProduct.setLaborCostperSquareFoot(decimalString);
        onlyProduct.setProduct("notAProduct");

        String time = "02022002";

        LocalDate date = LocalDate.parse(time, DateTimeFormatter.ofPattern("MMddyyyy"));

        onlyItem.setOrderDate(date);
        onlyItem.setArea(decimalString);
        onlyItem.setCustomerName("potato");
        onlyItem.setFinalLaborCosts(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setFinalTotal(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setMaterialCost(decimalString);
        onlyItem.setOrderNumber(1);
        onlyItem.setProductComponent(onlyProduct);
        onlyItem.setTaxComponent(onlyTax);

        List<Order> inventory = myDao.getAllOrders(onlyItem);
        for (Order eachOrder : inventory) {
            inventory = myDao.removeOrder(onlyItem);
            orderValue = orderValue + 1;
            myDao.update(inventory, eachOrder);
        }

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of individualOrderLocator method, of class FloorMasteryDaoFileImpl.
     */
    @Test
    public void testIndividualOrderLocator() throws Exception {
        Order activeOrder = new Order();
        activeOrder.setOrderDate(LocalDate.of(2002, 02, 02));
        Order FromDao = myDao.getOneOrder(activeOrder);
        Order AlsoFromDao = myDao.getOneOrder(activeOrder);
        assertEquals(FromDao, AlsoFromDao);
    }

    /**
     * Test of getOrder method, of class FloorMasteryDaoFileImpl.
     */
    /**
     * Test of orderAdd method, of class FloorMasteryDaoFileImpl.
     */
    @Test
    public void testOrderAdd() throws Exception {
        BigDecimal decimalString = new BigDecimal("200");
        Products onlyProduct = new Products();
        Tax onlyTax = new Tax();
        Order onlyItem = new Order();
        onlyTax.setState("notAState");
        onlyTax.setTaxRate(decimalString);

        onlyProduct.setCostperSquareFoot(decimalString);
        onlyProduct.setLaborCostperSquareFoot(decimalString);
        onlyProduct.setProduct("notAProduct");

        onlyItem.setArea(decimalString);
        onlyItem.setCustomerName("potato");
        onlyItem.setFinalLaborCosts(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setFinalTotal(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setMaterialCost(decimalString);
        onlyItem.setOrderNumber(1);
        onlyItem.setProductComponent(onlyProduct);
        onlyItem.setTaxComponent(onlyTax);

        List<Order> daoList = new ArrayList();
        daoList.add(onlyItem);

        assertEquals(daoList.size(), 1);
    }

    /**
     * Test of allOrders method, of class FloorMasteryDaoFileImpl.
     */
    @Test
    public void testAllOrders() throws FloorMasteryDaoException {
        BigDecimal decimalString = new BigDecimal("200");
        Products onlyProduct = new Products();
        Tax onlyTax = new Tax();
        Order onlyItem = new Order();
        onlyTax.setState("notAState");
        onlyTax.setTaxRate(decimalString);

        onlyProduct.setCostperSquareFoot(decimalString);
        onlyProduct.setLaborCostperSquareFoot(decimalString);
        onlyProduct.setProduct("notAProduct");

        onlyItem.setArea(decimalString);
        onlyItem.setCustomerName("potato");
        onlyItem.setFinalLaborCosts(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setFinalTotal(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setMaterialCost(decimalString);
        onlyItem.setOrderNumber(1);
        onlyItem.setProductComponent(onlyProduct);
        onlyItem.setTaxComponent(onlyTax);

        Products secondProduct = new Products();
        Tax secondTax = new Tax();
        Order secondItem = new Order();

        secondTax.setState("notAState");
        secondTax.setTaxRate(decimalString);

        secondProduct.setCostperSquareFoot(decimalString);
        secondProduct.setLaborCostperSquareFoot(decimalString);
        secondProduct.setProduct("notAProduct");

        secondItem.setArea(decimalString);
        secondItem.setCustomerName("potato");
        secondItem.setFinalLaborCosts(decimalString);
        secondItem.setFinalTaxCosts(decimalString);
        secondItem.setFinalTotal(decimalString);
        secondItem.setFinalTaxCosts(decimalString);
        secondItem.setMaterialCost(decimalString);
        secondItem.setOrderNumber(1);
        secondItem.setProductComponent(onlyProduct);
        secondItem.setTaxComponent(onlyTax);

        myDao.addOrderToDao(onlyItem);
        myDao.addOrderToDao(secondItem);

    }

    /**
     * Test of readOrders method, of class FloorMasteryDaoFileImpl.
     */
    @Test
    public void testReadOrders() throws Exception {
        BigDecimal decimalString = new BigDecimal("200");
        Products onlyProduct = new Products();
        Tax onlyTax = new Tax();
        Order onlyItem = new Order();
        onlyTax.setState("notAState");
        onlyTax.setTaxRate(decimalString);

        onlyProduct.setCostperSquareFoot(decimalString);
        onlyProduct.setLaborCostperSquareFoot(decimalString);
        onlyProduct.setProduct("notAProduct");

        onlyItem.setArea(decimalString);
        onlyItem.setCustomerName("potato");
        onlyItem.setFinalLaborCosts(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setFinalTotal(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setMaterialCost(decimalString);
        onlyItem.setOrderNumber(1);
        onlyItem.setProductComponent(onlyProduct);
        onlyItem.setTaxComponent(onlyTax);

        List<Order> allOrders = myDao.getAllOrders(onlyItem);
        List<Order> allOrdersAgain = myDao.getAllOrders(onlyItem);

        assertEquals(allOrders, allOrdersAgain);
    }

    /**
     * Test of setDate method, of class FloorMasteryDaoFileImpl.
     */
    @Test
    public void testSetDate() throws Exception {
        Order activeOrder = new Order();
        LocalDate information = null;
        LocalDate information2 = null;
        String time = "02022002";
        information = LocalDate.parse(time, DateTimeFormatter.ofPattern("MMddyyyy"));
        information2 = LocalDate.parse(time, DateTimeFormatter.ofPattern("MMddyyyy"));
        activeOrder.setOrderDate(information);
        myDao.setDate(activeOrder);
        assertEquals(information, information2);
    }

    /**
     * Test of update method, of class FloorMasteryDaoFileImpl.
     */
    @Test
    public void testUpdate() throws FloorMasteryDaoException {
        BigDecimal decimalString = new BigDecimal("200");
        Products onlyProduct = new Products();
        Tax onlyTax = new Tax();
        Order onlyItem = new Order();
        onlyTax.setState("notAState");
        onlyTax.setTaxRate(decimalString);

        onlyProduct.setCostperSquareFoot(decimalString);
        onlyProduct.setLaborCostperSquareFoot(decimalString);
        onlyProduct.setProduct("notAProduct");

        onlyItem.setArea(decimalString);
        onlyItem.setCustomerName("Green");
        onlyItem.setFinalLaborCosts(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setFinalTotal(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setMaterialCost(decimalString);
        onlyItem.setOrderNumber(1);
        onlyItem.setProductComponent(onlyProduct);
        onlyItem.setTaxComponent(onlyTax);
        List<Order> updatedOrders = new ArrayList();

        updatedOrders.add(onlyItem);

        List<Order> allOrders = myDao.getAllOrders(onlyItem);

    }

    /**
     * Test of saveChanges method, of class FloorMasteryDaoFileImpl.
     */
    @Test
    public void testSaveChanges() throws Exception {
        BigDecimal decimalString = new BigDecimal("200");
        Products onlyProduct = new Products();
        Tax onlyTax = new Tax();
        Order onlyItem = new Order();
        onlyTax.setState("notAState");
        onlyTax.setTaxRate(decimalString);

        onlyProduct.setCostperSquareFoot(decimalString);
        onlyProduct.setLaborCostperSquareFoot(decimalString);
        onlyProduct.setProduct("notAProduct");

        String time = "02022002";
        String time2 = "02022003";
        LocalDate date = LocalDate.parse(time, DateTimeFormatter.ofPattern("MMddyyyy"));
        LocalDate date2 = LocalDate.parse(time2, DateTimeFormatter.ofPattern("MMddyyyy"));

        onlyItem.setOrderDate(date);
        onlyItem.setArea(decimalString);
        onlyItem.setCustomerName("potato");
        onlyItem.setFinalLaborCosts(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setFinalTotal(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setMaterialCost(decimalString);
        onlyItem.setOrderNumber(1);
        onlyItem.setProductComponent(onlyProduct);
        onlyItem.setTaxComponent(onlyTax);

        Products secondProduct = new Products();
        Tax secondTax = new Tax();
        Order secondItem = new Order();

        secondTax.setState("notAState");
        secondTax.setTaxRate(decimalString);

        secondProduct.setCostperSquareFoot(decimalString);
        secondProduct.setLaborCostperSquareFoot(decimalString);
        secondProduct.setProduct("notAProduct");

        secondItem.setOrderDate(date2);
        secondItem.setArea(decimalString);
        secondItem.setCustomerName("potato");
        secondItem.setFinalLaborCosts(decimalString);
        secondItem.setFinalTaxCosts(decimalString);
        secondItem.setFinalTotal(decimalString);
        secondItem.setFinalTaxCosts(decimalString);
        secondItem.setMaterialCost(decimalString);
        secondItem.setOrderNumber(1);
        secondItem.setProductComponent(onlyProduct);
        secondItem.setTaxComponent(onlyTax);

        myDao.setDate(onlyItem);
        myDao.addOrderToDao(onlyItem);
        List<Order> allOrders = myDao.getAllOrders(onlyItem);
        myDao.setDate(secondItem);
        myDao.addOrderToDao(secondItem);
        List<Order> allOrders2 = myDao.getAllOrders(secondItem);
        Order order1 = allOrders.get(0);
        Order order2 = allOrders2.get(0);
        Boolean provesFalse = order1.equals(order2);

        assertEquals(provesFalse, true);

    }

    @Test
    public void removeOrder() throws Exception {
        BigDecimal decimalString = new BigDecimal("200");
        Products onlyProduct = new Products();
        Tax onlyTax = new Tax();
        Order onlyItem = new Order();
        onlyTax.setState("notAState");
        onlyTax.setTaxRate(decimalString);

        onlyProduct.setCostperSquareFoot(decimalString);
        onlyProduct.setLaborCostperSquareFoot(decimalString);
        onlyProduct.setProduct("notAProduct");

        onlyItem.setArea(decimalString);
        onlyItem.setCustomerName("potato");
        onlyItem.setFinalLaborCosts(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setFinalTotal(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setMaterialCost(decimalString);
        onlyItem.setOrderNumber(1);
        onlyItem.setProductComponent(onlyProduct);
        onlyItem.setTaxComponent(onlyTax);

        Products secondProduct = new Products();
        Tax secondTax = new Tax();
        Order secondItem = new Order();

        secondTax.setState("notAState");
        secondTax.setTaxRate(decimalString);

        secondProduct.setCostperSquareFoot(decimalString);
        secondProduct.setLaborCostperSquareFoot(decimalString);
        secondProduct.setProduct("notAProduct");

        secondItem.setArea(decimalString);
        secondItem.setCustomerName("potato");
        secondItem.setFinalLaborCosts(decimalString);
        secondItem.setFinalTaxCosts(decimalString);
        secondItem.setFinalTotal(decimalString);
        secondItem.setFinalTaxCosts(decimalString);
        secondItem.setMaterialCost(decimalString);
        secondItem.setOrderNumber(1);
        secondItem.setProductComponent(onlyProduct);
        secondItem.setTaxComponent(onlyTax);

        myDao.addOrderToDao(onlyItem);
        myDao.addOrderToDao(secondItem);

        List<Order> allOrders = myDao.getAllOrders(secondItem);
        myDao.removeOrder(secondItem);

        assertEquals(allOrders.size(), 1);
    }

}
