/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. for a new commit. 
 */
package com.mycompany.floormasteryproject.controller;

import com.mycompany.floormasteryproject.dao.FloorMasteryDaoException;
import com.mycompany.floormasteryproject.dao.PriceAndTaxProductDaoException;
import com.mycompany.floormasteryproject.dto.Order;
import com.mycompany.floormasteryproject.dto.Products;
import com.mycompany.floormasteryproject.dto.Tax;
import com.mycompany.floormasteryproject.service.FloorMasteryServiceNegativeFundsException;
import com.mycompany.floormasteryproject.service.FloorMasteryServiceException;
import com.mycompany.floormasteryproject.service.FloorMasteryServiceNonMatchException;
import com.mycompany.floormasteryproject.ui.FloorMasteryView;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.mycompany.floormasteryproject.service.FloorMasteryServiceLayer;

/**
 *
 * @author michael
 */
public class FloorMasteryController {

    private FloorMasteryView view;
    private FloorMasteryServiceLayer service;

    public FloorMasteryController(FloorMasteryServiceLayer service, FloorMasteryView view) {
        this.service = service;
        this.view = view;

    }

    public void run() {

        trainOrProduction();

    }

    private void trainOrProduction()  {
        int training = 0;
        training = view.trainingMenu();
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        if (training == 1) {
            this.service = ctx.getBean("myTrainingService", FloorMasteryServiceLayer.class);
        }
        if (training == 2) {
            this.service = ctx.getBean("myService", FloorMasteryServiceLayer.class);
        }
       
        mainMenu();

    }

    private void mainMenu()  {
        Boolean mainMenuActive = true;

        Order activeOrder = new Order();
        while (mainMenuActive == true) {

            int menuSelection = view.mainMenuSelection(activeOrder);
            switch (menuSelection) {
                case 1:
                    try {
                        displayOrders(activeOrder);
                    } catch (FloorMasteryDaoException e) {
                        view.fireNoMatchError();
                    } catch (FloorMasteryServiceException ex){
                        view.noDataInOrder();
                    }
                    break;
                case 2:
                    try{
                    addAnOrder();
                    }catch (PriceAndTaxProductDaoException ex){
                        view.negativeAmountFound();
                    }
                        
                    break;
                case 3:
                    try{
                    editAnOrder();
                    }catch(FloorMasteryServiceException ex){
                        view.noDataInDate();
                    }catch(FloorMasteryServiceNegativeFundsException ex){
                        view.negativeAmountFound();
                    }catch (PriceAndTaxProductDaoException ex){
                        view.negativeAmountFound();
                    }
                    break;
                case 4:
                    boolean skipDate = false;
                    try {
                    removeAnOrder(activeOrder);
                    }catch (FloorMasteryServiceException ex){
                        view.noDataInOrder();
                    }
                    break;
                case 5:
                    try {
                    saveCurrentWork(activeOrder);
                     }catch (FloorMasteryServiceException ex){
                        view.noDataInOrder();
                    }
                    break;
                case 6:
                    mainMenuActive = false;
                    endSession();
                    break;
            }
        }

    }

    private Order displayOrders(Order activeOrder) throws FloorMasteryDaoException, FloorMasteryServiceException {
        LocalDate userInputedDate = view.displayOrderDate();
        activeOrder.setOrderDate(userInputedDate);
        List<Order> toDisplayAllOrders = service.findOrderDate(activeOrder);
        if (toDisplayAllOrders == null || toDisplayAllOrders.size() == 0) {
            view.noDataInDate();
        } else {
            view.displayOrders(toDisplayAllOrders);
        }

        return activeOrder;
    }

    private Order addAnOrder() throws  PriceAndTaxProductDaoException {
        Order activeOrder = new Order();
        LocalDate userInputedDate = view.displayOrderDate();
        List<Products> productList = service.getProductList();
        List<Tax> taxList = service.getTaxsList();
        activeOrder.setOrderDate(userInputedDate);
        
        boolean editDate = false;
        activeOrder = view.addOrderDisplay(activeOrder, productList, taxList, editDate);
        try {
            Order orderToPrint = service.addAnOrder(activeOrder);
            view.printSingleOrder(orderToPrint);
            if (activeOrder.getProductComponent() == null || activeOrder.getTaxComponent() == null) {
                view.noDataInOrder();
            }
        } catch (PriceAndTaxProductDaoException ex) {
            view.noDataInOrder();
        }catch (FloorMasteryServiceNegativeFundsException ex){
            view.negativeAmountFound();
        }

        return activeOrder;
    }

    private void editAnOrder() throws FloorMasteryServiceException, FloorMasteryServiceNegativeFundsException, PriceAndTaxProductDaoException {
      
        Order activeOrder = new Order();
        LocalDate userInputedDate = view.displayOrderDate();
        activeOrder.setOrderDate(userInputedDate);
         try {
        List<Order> toDisplayAllOrders = service.findAllOrder(activeOrder);
        if (toDisplayAllOrders == null || toDisplayAllOrders.size() == 0) {
            view.noDataInOrder();

        } else {
             
            view.displayOrders(toDisplayAllOrders);

            Order forLargestOrderNumber = toDisplayAllOrders.get(toDisplayAllOrders.size() - 1);
            int maximumOrderNumber = forLargestOrderNumber.getOrderNumber();
            activeOrder.setOrderNumber(view.SelectOrderEdit(activeOrder, maximumOrderNumber));
            Order orderToEdit = service.findAndEditOrder(activeOrder);
            if (orderToEdit == null) {
                view.noDataInOrder();
            } else {

                List<Products> productList = service.getProductList();
                List<Tax> taxList = service.getTaxsList();
                boolean editDate = true;
                orderToEdit.setOrderDate(userInputedDate);
                orderToEdit = view.addOrderDisplay(orderToEdit, productList, taxList, editDate);
                if (orderToEdit.getProductComponent() == null || orderToEdit.getTaxComponent() == null) {
                    view.noDataInOrder();
                }

                int keepOrDiscard = view.keepOrDiscard();
                if (keepOrDiscard == 1) {
                    if (activeOrder.getOrderDate() != orderToEdit.getOrderDate()) {
                        service.addAnOrder(orderToEdit);
                        service.findAndRemoveOrder(activeOrder);
                    } else {
                        service.replaceOrder(orderToEdit);
                    }
                } else {
                    view.abortOrder(orderToEdit);
                }
        
              }
                      }
        
          }catch (FloorMasteryServiceException ex){
              view.noDataInDate();
          }
           catch (FloorMasteryServiceNegativeFundsException ex){
              view.negativeAmountFound();
          }catch (PriceAndTaxProductDaoException ex){
              view.noDataInOrder();
          }
    }
    
    private void saveCurrentWork(Order activeOrder) throws FloorMasteryServiceException {

        try {
            service.saveChanges(activeOrder);
        } catch (FloorMasteryServiceException ex) {
            view.noDataInOrder();
        }
    }

    private void removeAnOrder(Order activeOrder) throws FloorMasteryServiceException {
        LocalDate userInputedDate;
        userInputedDate = activeOrder.getOrderDate();
        userInputedDate = view.displayOrderDate();

        activeOrder.setOrderDate(userInputedDate);
        try
        {
        int maximumNumberOfRecords;
        List<Order> toDisplayAllOrders = service.findAllOrder(activeOrder);
        view.displayOrders(toDisplayAllOrders);
        if (toDisplayAllOrders == null || toDisplayAllOrders.size() == 0) {
            view.abortOrder(activeOrder);
        } else {
            maximumNumberOfRecords = toDisplayAllOrders.size() - 1;
            Order orderNumberMax = toDisplayAllOrders.get(maximumNumberOfRecords);
            int maximumOrderNumber = orderNumberMax.getOrderNumber();

            activeOrder = view.SelectOrderRemove(activeOrder, maximumOrderNumber);

            activeOrder.setOrderDate(userInputedDate);
            int keepOrDiscard = view.keepOrDiscard();
            if (keepOrDiscard == 1) {
                activeOrder.setOrderDate(userInputedDate);
                service.findAndRemoveOrder(activeOrder);
            } else {
                view.abortOrder(activeOrder);
            }

        }
        }catch(FloorMasteryServiceException ex){
            view.noDataInDate();
        }
    }

    private void endSession() {;
        view.fireEndProgram();
    }

}
