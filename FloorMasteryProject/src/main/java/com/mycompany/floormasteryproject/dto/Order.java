/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.dto;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;
import org.eclipse.core.internal.runtime.Product;

/**
 *
 * @author michael
 */
public class Order {

    private LocalDate orderDate;
    private int orderNumber ;
    
    private String customerName; 
            
    private BigDecimal area;
    private BigDecimal materialCost;
    private BigDecimal finalLaborCosts;
    private BigDecimal finalTaxCosts;
    private BigDecimal finalTotal;

    Products productComponent;
    Tax TaxComponent;

    public Order() {

    }

    public Tax getTaxComponent() {
        return TaxComponent;
    }

    public void setTaxComponent(Tax TaxComponent) {
        this.TaxComponent = TaxComponent;
    }

    public Products getProductComponent() {
        return productComponent;
    }

    public void setProductComponent(Products productComponent) {
        this.productComponent = productComponent;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public BigDecimal getArea() {
        return area;
    }

    public void setArea(BigDecimal area) {
        this.area = area.setScale(2, RoundingMode.DOWN);
    }

    public BigDecimal getMaterialCost() {
        return materialCost;
    }

    public void setMaterialCost(BigDecimal materialCost) {
        this.materialCost = materialCost.setScale(2, RoundingMode.DOWN);
    }

    public BigDecimal getFinalLaborCosts() {
        return finalLaborCosts;
    }

    public void setFinalLaborCosts(BigDecimal finalLaborCosts) {
        this.finalLaborCosts = finalLaborCosts.setScale(2, RoundingMode.DOWN);
    }

    public BigDecimal getFinalTaxCosts() {
        return finalTaxCosts;
    }

    public void setFinalTaxCosts(BigDecimal finalTaxCosts) {
        this.finalTaxCosts = finalTaxCosts.setScale(2, RoundingMode.DOWN);
    }

    public BigDecimal getFinalTotal() {
        return finalTotal;
    }

    public void setFinalTotal(BigDecimal finalTotal) {
        this.finalTotal = finalTotal.setScale(2, RoundingMode.DOWN);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.orderDate);
        hash = 59 * hash + this.orderNumber;
        hash = 59 * hash + Objects.hashCode(this.customerName);
        hash = 59 * hash + Objects.hashCode(this.area);
        hash = 59 * hash + Objects.hashCode(this.materialCost);
        hash = 59 * hash + Objects.hashCode(this.finalLaborCosts);
        hash = 59 * hash + Objects.hashCode(this.finalTaxCosts);
        hash = 59 * hash + Objects.hashCode(this.finalTotal);
        hash = 59 * hash + Objects.hashCode(this.productComponent);
        hash = 59 * hash + Objects.hashCode(this.TaxComponent);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Order other = (Order) obj;
        if (this.orderNumber != other.orderNumber) {
            return false;
        }
        if (!Objects.equals(this.customerName, other.customerName)) {
            return false;
        }
        if (!Objects.equals(this.orderDate, other.orderDate)) {
            return false;
        }
        if (!Objects.equals(this.area, other.area)) {
            return false;
        }
        if (!Objects.equals(this.materialCost, other.materialCost)) {
            return false;
        }
        if (!Objects.equals(this.finalLaborCosts, other.finalLaborCosts)) {
            return false;
        }
        if (!Objects.equals(this.finalTaxCosts, other.finalTaxCosts)) {
            return false;
        }
        if (!Objects.equals(this.finalTotal, other.finalTotal)) {
            return false;
        }
        if (!Objects.equals(this.productComponent, other.productComponent)) {
            return false;
        }
        if (!Objects.equals(this.TaxComponent, other.TaxComponent)) {
            return false;
        }
        return true;
    }

}
