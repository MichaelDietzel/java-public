/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.dto;

import java.math.BigDecimal;

/**
 *
 * @author michael
 */
public class Products {

    private String product;
    private BigDecimal costperSquareFoot;
    private BigDecimal laborCostperSquareFoot;

    public Products() {

    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public BigDecimal getCostperSquareFoot() {
        return costperSquareFoot;
    }

    public void setCostperSquareFoot(BigDecimal costperSquareFoot) {
        this.costperSquareFoot = costperSquareFoot;
    }

    public BigDecimal getLaborCostperSquareFoot() {
        return laborCostperSquareFoot;
    }

    public void setLaborCostperSquareFoot(BigDecimal laborCostperSquareFoot) {
        this.laborCostperSquareFoot = laborCostperSquareFoot;
    }

}
