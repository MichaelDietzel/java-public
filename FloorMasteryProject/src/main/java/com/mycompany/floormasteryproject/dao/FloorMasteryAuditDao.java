package com.mycompany.floormasteryproject.dao;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author michael
 */
public interface FloorMasteryAuditDao {

    public void writeAuditEntry(String entry) throws FloorMasteryDaoException;
}
