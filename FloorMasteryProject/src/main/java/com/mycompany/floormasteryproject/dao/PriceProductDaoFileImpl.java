/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.dao;

import com.mycompany.floormasteryproject.dto.Order;
import com.mycompany.floormasteryproject.dto.Products;
import com.mycompany.floormasteryproject.dto.Tax;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author michael
 */
public class PriceProductDaoFileImpl implements PriceProductDao {

    private Map< String, Products> productList = new HashMap<>();
    private String INVENTORY_FILE = "Data/Products.txt";
    public static final String DELIMITER = ",";

    public void getInventory() throws PriceAndTaxProductDaoException {
        Scanner inventoryScanner;
        try {
            inventoryScanner = new Scanner(new BufferedReader(new FileReader(INVENTORY_FILE)));
        } catch (FileNotFoundException e) {
            throw new PriceAndTaxProductDaoException("Could not load roster data into memory.", e);
        }

        String currentInventoryLine;
        String[] currentTokens;
        while (inventoryScanner.hasNextLine()) {
            currentInventoryLine = inventoryScanner.nextLine();
            currentTokens = currentInventoryLine.split(DELIMITER);
            Products currentProducts = new Products();

            currentProducts.setProduct(currentTokens[0]);
            BigDecimal Productcost = new BigDecimal(currentTokens[1]);
            currentProducts.setCostperSquareFoot(Productcost);
            BigDecimal Laborcost = new BigDecimal(currentTokens[2]);
            currentProducts.setLaborCostperSquareFoot(Laborcost);
            productList.put(currentProducts.getProduct(), currentProducts);

        }
        inventoryScanner.close();
    }

    @Override
    public Products getInventoryPrices(String product) throws PriceAndTaxProductDaoException {

        productList.clear();
        getInventory();
        Products nullProduct = null;

        Products returnProduct = productList.get(product);
        if (returnProduct != null) {
            return returnProduct;
        }

        return nullProduct;
    }

    @Override
    public List<Products> getListOfProducts() {

        productList.clear();
        try {
            getInventory();
        } catch (PriceAndTaxProductDaoException ex) {
            return null;
        }
        List<Products> allProducts = findAllorders();

        return allProducts;
    }

    private List<Products> findAllorders() {
        return new ArrayList<>(productList.values());

    }

}
