/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.dao;

import com.mycompany.floormasteryproject.dto.Order;
import com.mycompany.floormasteryproject.dto.Products;
import com.mycompany.floormasteryproject.dto.Tax;
import com.mycompany.floormasteryproject.service.FloorMasteryServiceNegativeFundsException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author michael
 */
public class StateTaxDaoImpl implements StateTaxDao {

    private Map< String, Tax> taxList = new HashMap<>();
    private String INVENTORY_FILE = "Data/Taxes.txt";
    public static final String DELIMITER = ",";

    public void getTax() throws PriceAndTaxProductDaoException {
        Scanner inventoryScanner;
        try {
            inventoryScanner = new Scanner(new BufferedReader(new FileReader(INVENTORY_FILE)));
        } catch (FileNotFoundException e) {
            throw new PriceAndTaxProductDaoException("Could not load roster data into memory.", e);
        }

        String currentInventoryLine;
        String[] currentTokens;
        while (inventoryScanner.hasNextLine()) {
            currentInventoryLine = inventoryScanner.nextLine();
            currentTokens = currentInventoryLine.split(DELIMITER);

            Tax currentTax = new Tax();

            currentTax.setState(currentTokens[0]);
            BigDecimal stateTax = new BigDecimal(currentTokens[1]);
            currentTax.setTaxRate(stateTax);

            taxList.put(currentTax.getState(), currentTax);

        }
        inventoryScanner.close();
    }

    @Override
    public Tax getTaxAmounts(String state) throws PriceAndTaxProductDaoException {

        taxList.clear();
        getTax();
        Tax nullTax = null;

        Tax returnTax = taxList.get(state);
        if (returnTax != null) {
            return returnTax;
        }

        return nullTax;
    }

    @Override
    public List<Tax> getListofTax() {

        taxList.clear();
        try {
            getTax();
        } catch (PriceAndTaxProductDaoException ex) {
            return null;
        }
        List<Tax> allTax = findAllorders();

        return allTax;
    }

    private List<Tax> findAllorders() {
        return new ArrayList<>(taxList.values());

    }

}
