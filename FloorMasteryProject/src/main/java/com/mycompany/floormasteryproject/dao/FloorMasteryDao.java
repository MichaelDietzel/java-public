/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.dao;

import com.mycompany.floormasteryproject.dto.Order;
import java.util.List;
import java.util.Map;

/**
 *
 * @author michael
 */
public interface FloorMasteryDao {

    public Order getOneOrder(Order activeOrder) throws FloorMasteryDaoException;

    public void addOrderToDao(Order activeOrder) throws FloorMasteryDaoException;

    public void update(List<Order> updatedOrders, Order activeOrder) throws FloorMasteryDaoException;

    public List<Order> getAllOrders(Order activeOrder) throws FloorMasteryDaoException;

    public void setDate(Order activeOrder) throws FloorMasteryDaoException;

    public void saveChanges() throws FloorMasteryDaoException;

    public List<Order> removeOrder(Order activeOrder);

}
