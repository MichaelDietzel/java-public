/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.dao;

import com.mycompany.floormasteryproject.dto.Order;
import com.mycompany.floormasteryproject.dto.Products;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author michael
 */
public class PriceProductDaoStubImpl implements PriceProductDao {

    Products onlyProduct = new Products();
    List<Products> onlyProductList = new ArrayList();

    public PriceProductDaoStubImpl() {

        BigDecimal decimalString = new BigDecimal("200");
        onlyProduct.setCostperSquareFoot(decimalString);
        onlyProduct.setLaborCostperSquareFoot(decimalString);
        onlyProduct.setProduct("notAProduct");

        onlyProductList.add(onlyProduct);

    }

    @Override
    public Products getInventoryPrices(String product) throws PriceAndTaxProductDaoException {

        return onlyProduct;
    }

    @Override
    public List<Products> getListOfProducts() {
        return onlyProductList;
    }

}
