/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.dao;

import com.mycompany.floormasteryproject.dto.Order;
import com.mycompany.floormasteryproject.dto.Products;
import com.mycompany.floormasteryproject.dto.Tax;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author michael
 */
public class StateTaxDaoStubImpl implements StateTaxDao {

    Tax onlyTax = new Tax();
    List<Tax> Tax = new ArrayList();

    public StateTaxDaoStubImpl() {

        BigDecimal decimalString = new BigDecimal("200");
        onlyTax.setTaxRate(decimalString);
        onlyTax.setState("Not Ohio");

        Tax.add(onlyTax);

    }

    @Override
    public Tax getTaxAmounts(String state) throws PriceAndTaxProductDaoException {

        return onlyTax;
    }

    @Override
    public List<Tax> getListofTax() {
        return Tax;
    }

}
