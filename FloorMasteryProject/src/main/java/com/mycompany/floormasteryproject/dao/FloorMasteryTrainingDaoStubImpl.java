/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.dao;

import com.mycompany.floormasteryproject.dto.Order;
import com.mycompany.floormasteryproject.dto.Products;
import com.mycompany.floormasteryproject.dto.Tax;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author michael
 */
public class FloorMasteryTrainingDaoStubImpl implements FloorMasteryDao {

    Products onlyProduct = new Products();
    Tax onlyTax = new Tax();
    Order onlyItem = new Order();
    List<Order> orderList = new ArrayList();
    String dateForFilename = "02022002";
    String ORDER_FILE = "Orders_" + dateForFilename + ".txt";

    public FloorMasteryTrainingDaoStubImpl() {
        BigDecimal decimalString = new BigDecimal("200");

        onlyTax.setState("notAState");
        onlyTax.setTaxRate(decimalString);

        onlyProduct.setCostperSquareFoot(decimalString);
        onlyProduct.setLaborCostperSquareFoot(decimalString);
        onlyProduct.setProduct("notAProduct");

        onlyItem.setArea(decimalString);
        onlyItem.setCustomerName("%%%%%");
        onlyItem.setFinalLaborCosts(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setFinalTotal(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setMaterialCost(decimalString);
        onlyItem.setOrderNumber(1);
        onlyItem.setProductComponent(onlyProduct);
        onlyItem.setTaxComponent(onlyTax);

        orderList.add(onlyItem);

    }

    @Override
    public Order getOneOrder(Order activeOrder) throws FloorMasteryDaoException {

        return onlyItem;
    }

    @Override
    public void addOrderToDao(Order activeOrder) throws FloorMasteryDaoException {
    }

    @Override
    public void update(List<Order> updatedOrders, Order activeOrder) throws FloorMasteryDaoException {

    }

    @Override
    public List getAllOrders(Order activeOrder) throws FloorMasteryDaoException {

        return orderList;
    }

    @Override
    public void setDate(Order activeOrder) throws FloorMasteryDaoException {

    }

    @Override
    public void saveChanges() throws FloorMasteryDaoException {

    }

    @Override
    public List<Order> removeOrder(Order activeOrder) {
        return orderList;
    }

}
