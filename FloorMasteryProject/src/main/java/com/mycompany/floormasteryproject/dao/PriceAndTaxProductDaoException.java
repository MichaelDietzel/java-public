/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.dao;

/**
 *
 * @author michael
 */
public class PriceAndTaxProductDaoException extends Exception {

    public PriceAndTaxProductDaoException(String message) {
        super(message);
    }

    public PriceAndTaxProductDaoException(String message,
            Throwable cause) {
        super(message, cause);
    }
}
