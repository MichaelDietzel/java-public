/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.dao;

import com.mycompany.floormasteryproject.dto.Order;
import com.mycompany.floormasteryproject.dto.Products;
import com.mycompany.floormasteryproject.dto.Tax;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.String.format;
import static java.lang.String.format;
import static java.lang.String.format;
import static java.lang.String.format;
import static java.lang.String.valueOf;
import java.math.BigDecimal;
import java.text.DateFormat;
import static java.text.MessageFormat.format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.eclipse.core.internal.runtime.Product;

/**
 *
 * @author michael
 */
public class FloorMasteryTrainingDaoFileImpl implements FloorMasteryDao {

    private Map< Integer, Order> orderList = new HashMap<>();
    private Map< LocalDate, List<Order>> allChanges = new HashMap<>();
    private String dateForFilename = null;
    private String ORDER_FILE = null;
    public static final String DELIMITER = ",";

    @Override
    public Order getOneOrder(Order activeOrder) throws FloorMasteryDaoException {
        setDate(activeOrder);
        List<Order> allOrders = new ArrayList();
        setDate(activeOrder);
        if (allChanges.get(activeOrder.getOrderDate()) != null) {
            allOrders = allChanges.get(activeOrder.getOrderDate());
            for (Order thisOrder : allOrders) {
                if (activeOrder.getOrderNumber() == thisOrder.getOrderNumber()) {
                    return thisOrder;
                }
            }
        }
        try {
            readFile();
        } catch (FloorMasteryDaoException ex) {
            return null;
        }
        Order foundOrder = orderList.get(activeOrder.getOrderNumber());
        if (foundOrder == null) {
            return null;
        }

        return foundOrder;
    }

    public void readFile() throws FloorMasteryDaoException {
        Scanner recordScanner = null;

        try {
            recordScanner = new Scanner(new BufferedReader(new FileReader(ORDER_FILE)));

            String currentInventoryLine;
            String[] currentTokens;
            while (recordScanner.hasNextLine()) {
                currentInventoryLine = recordScanner.nextLine();
                currentTokens = currentInventoryLine.split(DELIMITER);
                Order savedOrder = new Order();
                Tax savedTax = new Tax();
                Products savedProduct = new Products();
                String StringOrderNumber = (currentTokens[0]);
                savedOrder.setOrderNumber(Integer.valueOf(StringOrderNumber));
                savedOrder.setCustomerName(currentTokens[1]);
                savedTax.setState(currentTokens[2]);
                BigDecimal taxRate = new BigDecimal(currentTokens[3]);
                savedTax.setTaxRate(taxRate);
                savedProduct.setProduct(currentTokens[4]);
                BigDecimal setArea = new BigDecimal(currentTokens[5]);
                savedOrder.setArea(setArea);
                BigDecimal costPerSqFoot = new BigDecimal(currentTokens[6]);
                savedProduct.setCostperSquareFoot(costPerSqFoot);
                BigDecimal laborCostsPerSqFoot = new BigDecimal(currentTokens[7]);
                savedProduct.setLaborCostperSquareFoot(laborCostsPerSqFoot);
                BigDecimal MaterialCost = new BigDecimal(currentTokens[8]);
                savedOrder.setMaterialCost(MaterialCost);
                BigDecimal LaborCost = new BigDecimal(currentTokens[9]);
                savedOrder.setFinalLaborCosts(LaborCost);
                BigDecimal taxCost = new BigDecimal(currentTokens[10]);
                savedOrder.setFinalTaxCosts(taxCost);
                BigDecimal totalCost = new BigDecimal(currentTokens[11]);
                savedOrder.setFinalTotal(totalCost);
                orderList.put(savedOrder.getOrderNumber(), savedOrder);
                savedOrder.setTaxComponent(savedTax);
                savedOrder.setProductComponent(savedProduct);
            }
            recordScanner.close();
        } catch (FileNotFoundException e) {
            throw new FloorMasteryDaoException("Could not load order roster data into memory.", e);
        }

    }

    @Override
    public void addOrderToDao(Order activeOrder) throws FloorMasteryDaoException {
        setDate(activeOrder);
        orderList.put(activeOrder.getOrderNumber(), activeOrder);

        List<Order> allOrders = findAllorders();

        allOrders = allOrders
                .stream()
                .distinct()
                .collect(Collectors.toList());

        allChanges.put(activeOrder.getOrderDate(), allOrders);
        orderList.clear();
    }

    private void writeOrder() throws FloorMasteryDaoException {

        PrintWriter out;

        try {
            out = new PrintWriter(new FileWriter(ORDER_FILE));
        } catch (IOException e) {
            throw new FloorMasteryDaoException("Could not read data into the file", e);
        }
        List<Order> OrderList = findAllorders();

        for (Order writeOrder : OrderList) {
            out.println(writeOrder.getOrderNumber() + DELIMITER
                    + writeOrder.getCustomerName() + DELIMITER
                    + writeOrder.getTaxComponent().getState() + DELIMITER
                    + writeOrder.getTaxComponent().getTaxRate() + DELIMITER
                    + writeOrder.getProductComponent().getProduct() + DELIMITER
                    + writeOrder.getArea() + DELIMITER
                    + writeOrder.getProductComponent().getCostperSquareFoot() + DELIMITER
                    + writeOrder.getProductComponent().getLaborCostperSquareFoot() + DELIMITER
                    + writeOrder.getMaterialCost() + DELIMITER
                    + writeOrder.getFinalLaborCosts() + DELIMITER
                    + writeOrder.getFinalTaxCosts() + DELIMITER
                    + writeOrder.getFinalTotal() + DELIMITER);

            out.flush();
        }
        out.close();
    }

    private List<Order> findAllorders() {
        return new ArrayList<>(orderList.values());
    }

    @Override
    public List<Order> getAllOrders(Order activeOrder) throws FloorMasteryDaoException {
        List<Order> allOrders = new ArrayList();
        setDate(activeOrder);
        if (allChanges.get(activeOrder.getOrderDate()) != null) {
            allOrders = allChanges.get(activeOrder.getOrderDate());
            return allOrders;
        }
        try {
            readFile();
        } catch (FloorMasteryDaoException ex) {
            return null;
        }
        allOrders = findAllordersCurrentSession();
        return allOrders;
    }

    @Override()
    public void setDate(Order activeOrder) throws FloorMasteryDaoException {

        dateForFilename = activeOrder.getOrderDate().format(DateTimeFormatter.ofPattern("MMddyyyy"));
        ORDER_FILE = "Orders_" + dateForFilename + ".txt";

    }

    @Override
    public void update(List<Order> updatedListofOrder, Order activeOrder) {

        orderList.clear();
        for (Order currentOrder : updatedListofOrder) {
            orderList.put(currentOrder.getOrderNumber(), currentOrder);

        }

        List<Order> allOrders = orderList.values()
                .stream()
                .distinct()
                .collect(Collectors.toList());
        allChanges.put(activeOrder.getOrderDate(), updatedListofOrder);

    }

    @Override
    public void saveChanges() throws FloorMasteryDaoException {

    }

    @Override
    public List<Order> removeOrder(Order activeOrder) {
        boolean match = false;
        int index = 0;
        int indexToRemove = 0;
        List<Order> allOrders = allChanges.get(activeOrder.getOrderDate());
        for (Order theseOrders : allOrders) {
            if (theseOrders.getOrderNumber() == activeOrder.getOrderNumber()) {
                match = true;
                indexToRemove = index;
            }
            if (match == false) {
                index = index + 1;
            }
        }
        if (allOrders == null || allOrders.size() == 0) {
            allOrders.clear();
            return null;
        }
        allOrders.remove(indexToRemove);

        return allOrders;
    }

    private List<Order> findAllordersCurrentSession() {
        LocalDate date = LocalDate.parse(dateForFilename, DateTimeFormatter.ofPattern("MMddyyyy"));
        List<Order> allOrder = new ArrayList();
        List<Order> allOrdersForCurrentToday = allChanges.get(date);
        if (allOrdersForCurrentToday == null) {
            for (Order collection : orderList.values()) {
                allOrder.add(collection);
            }
            return allOrder;
        } else {
            allOrder = allOrdersForCurrentToday
                    .stream()
                    .distinct()
                    .collect(Collectors.toList());
        }
        return allOrder;
    }
}
