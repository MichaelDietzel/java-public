/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.dao;

import com.mycompany.floormasteryproject.dto.Order;
import com.mycompany.floormasteryproject.dto.Products;
import com.mycompany.floormasteryproject.dto.Tax;
import java.util.List;

/**
 *
 * @author michael
 */
public interface StateTaxDao {

    public Tax getTaxAmounts(String state) throws PriceAndTaxProductDaoException;

    public List<Tax> getListofTax();

}
