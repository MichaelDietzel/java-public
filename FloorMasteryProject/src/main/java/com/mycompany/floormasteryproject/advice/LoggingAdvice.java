package com.mycompany.floormasteryproject.advice;

import com.mycompany.floormasteryproject.dao.FloorMasteryAuditDao;
import com.mycompany.floormasteryproject.dao.FloorMasteryDaoException;
import org.aspectj.lang.JoinPoint;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author michael
 */
public class LoggingAdvice {

    FloorMasteryAuditDao auditDao;

    public LoggingAdvice(FloorMasteryAuditDao auditDao) {
        this.auditDao = auditDao;
    }

    public void createExceptionAuditEntry(JoinPoint jp, FloorMasteryDaoException dataAccessEx) {
        Object[] args = jp.getArgs();

        String auditEntry = jp.getSignature().getName() + ": FloorMasteryDataException";

        for (Object currentArg : args) {
            auditEntry += currentArg;

        }
        try {
            auditDao.writeAuditEntry(auditEntry);

        } catch (FloorMasteryDaoException ex) {
            System.err.println("ERROR: could not create audit entery in logging advice:");
        }
    }

    public void createAuditEntry(JoinPoint jp) {
        Object[] args = jp.getArgs();
        String auditEntry = jp.getSignature().getName() + ": ";
        for (Object currentArg : args) {
            auditEntry += currentArg;

        }
        try {
            auditDao.writeAuditEntry(auditEntry);

        } catch (FloorMasteryDaoException e) {
            System.err.println("ERROR: could not create audit entery in logging advice:");
        }
    }
}
