/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.service;

/**
 *
 * @author michael
 */
public class FloorMasteryServiceException extends Exception {

    public FloorMasteryServiceException(String message) {
        super(message);
    }

    public FloorMasteryServiceException(String message, Throwable cause) {
        super(message, cause);
    }

}
