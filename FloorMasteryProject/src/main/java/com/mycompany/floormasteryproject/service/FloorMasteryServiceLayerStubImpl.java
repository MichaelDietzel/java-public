/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.service;

import com.mycompany.floormasteryproject.dao.FloorMasteryAuditDao;
import com.mycompany.floormasteryproject.dao.FloorMasteryDao;
import com.mycompany.floormasteryproject.dao.FloorMasteryDaoException;
import com.mycompany.floormasteryproject.dao.PriceProductDao;
import com.mycompany.floormasteryproject.dao.StateTaxDao;
import com.mycompany.floormasteryproject.dto.Order;
import com.mycompany.floormasteryproject.dto.Products;
import com.mycompany.floormasteryproject.dto.Tax;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.core.internal.runtime.Product;

/**
 *
 * @author michael
 */
public class FloorMasteryServiceLayerStubImpl implements FloorMasteryServiceLayer {

    private FloorMasteryAuditDao audit;
    private FloorMasteryDao dao;
    private PriceProductDao priceDao;
    private StateTaxDao taxDao;

    private int Training;
    private BigDecimal one = new BigDecimal("1.00").setScale(2, RoundingMode.DOWN);
    private Order onlyItem = new Order();
    private Products onlyProduct = new Products();
    private Tax onlyTax = new Tax();
    private List<Order> onlyItemsList = new ArrayList();
    private List<Products> onlyProductsList = new ArrayList();
    private List<Tax> onlyTaxList = new ArrayList();

    public FloorMasteryServiceLayerStubImpl() {
        this.dao = dao;
        this.audit = audit;
        this.priceDao = priceDao;
        this.taxDao = taxDao;

        BigDecimal decimalString = new BigDecimal("200");

        onlyTax.setState("notAState");
        onlyTax.setTaxRate(decimalString);
        onlyTaxList.add(onlyTax);

        onlyProduct.setCostperSquareFoot(decimalString);
        onlyProduct.setLaborCostperSquareFoot(decimalString);
        onlyProduct.setProduct("notAProduct");
        onlyProductsList.add(onlyProduct);

        String time = "02022002";
        LocalDate date = LocalDate.parse(time, DateTimeFormatter.ofPattern("MMddyyyy"));

        onlyItem.setOrderDate(date);
        onlyItem.setArea(decimalString);
        onlyItem.setCustomerName("potato");
        onlyItem.setFinalLaborCosts(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setFinalTotal(decimalString);
        onlyItem.setFinalTaxCosts(decimalString);
        onlyItem.setMaterialCost(decimalString);
        onlyItem.setOrderNumber(1);
        onlyItem.setProductComponent(onlyProduct);
        onlyItem.setTaxComponent(onlyTax);

        onlyItemsList.add(onlyItem);

    }

    @Override
    public List<Order> findOrderDate(Order activeOrder) throws FloorMasteryServiceException {
        if (activeOrder.getOrderDate().equals(onlyItem.getOrderDate())) {

            return onlyItemsList;
        } else {
            return null;
        }
    }

    @Override
    public Order addAnOrder(Order activeOrder) throws FloorMasteryServiceNegativeFundsException, com.mycompany.floormasteryproject.dao.PriceAndTaxProductDaoException {
        return onlyItem;
    }

    @Override
    public void replaceOrder(Order activeOrder) throws FloorMasteryServiceNegativeFundsException {

    }

    @Override
    public List<Order> findAllOrder(Order activeOrder) throws FloorMasteryServiceException {

        return onlyItemsList;
    }

    @Override
    public void findAndRemoveOrder(Order activeOrder) {

    }

    @Override
    public void saveChanges(Order activeOrder) {

    }

    @Override
    public Order findAndEditOrder(Order activeOrder) {

        return onlyItem;
    }

    @Override
    public List<Tax> getTaxsList() {

        return onlyTaxList;
    }

    @Override
    public List<Products> getProductList() {
        return onlyProductsList;
    }

}
