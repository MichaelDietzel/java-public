/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.service;

/**
 *
 * @author michael
 */
public class FloorMasteryServiceNonMatchException extends Exception {

    public FloorMasteryServiceNonMatchException(String message) {
        super(message);
    }

    public FloorMasteryServiceNonMatchException(String message, Throwable cause) {
        super(message, cause);
    }
}
