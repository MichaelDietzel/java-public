/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.service;

/**
 *
 * @author michael
 */
import com.mycompany.floormasteryproject.dao.FloorMasteryAuditDao;
import com.mycompany.floormasteryproject.dao.FloorMasteryDao;
import com.mycompany.floormasteryproject.dao.FloorMasteryDaoException;
import com.mycompany.floormasteryproject.dao.PriceProductDao;
import com.mycompany.floormasteryproject.dao.PriceAndTaxProductDaoException;
import com.mycompany.floormasteryproject.dao.StateTaxDao;
import com.mycompany.floormasteryproject.dto.Order;
import com.mycompany.floormasteryproject.dto.Products;
import com.mycompany.floormasteryproject.dto.Tax;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author michael
 */
public class FloorMasteryServiceLayerImpl implements FloorMasteryServiceLayer {

    private FloorMasteryAuditDao audit;
    private FloorMasteryDao dao;
    private PriceProductDao priceDao;
    private StateTaxDao taxDao;
    private int Training = 0;
    private List<Products> productList = new ArrayList();
    private List<Tax> taxList = new ArrayList();

    public FloorMasteryServiceLayerImpl(FloorMasteryAuditDao audit, FloorMasteryDao dao, PriceProductDao priceDao, StateTaxDao taxDao) {
        this.dao = dao;
        this.audit = audit;
        this.priceDao = priceDao;
        this.taxDao = taxDao;
    }

    @Override
    public List<Order> findOrderDate(Order activeOrder)
            throws FloorMasteryServiceException {

        LocalDate placeHolder = activeOrder.getOrderDate();
        List<Order> ordersForDisplay = null;
        try {
            ordersForDisplay = dao.getAllOrders(activeOrder);
        } catch (FloorMasteryDaoException ex) {
            throw new FloorMasteryServiceException("An Invalid date was used");
        }
        if (ordersForDisplay == null) {
            return null;
        }

        for (Order currentOrder : ordersForDisplay) {
            currentOrder.setOrderDate(placeHolder);
        }
        return ordersForDisplay;

    }

    @Override
    public Order addAnOrder(Order activeOrder) throws FloorMasteryServiceNegativeFundsException, PriceAndTaxProductDaoException {
        int indexNumber = 0;
        int maxNumber = 0;
        List<Order> allOrderFromDate = new ArrayList();
        try {
            allOrderFromDate = dao.getAllOrders(activeOrder);
        } catch (FloorMasteryDaoException ex) {
            throw new FloorMasteryServiceNegativeFundsException("Something went geeting the file out");
        }
        if (allOrderFromDate == null || allOrderFromDate.size() == 0) {
            indexNumber = 1;
        } else {
            indexNumber = allOrderFromDate.size() - 1;
            maxNumber = allOrderFromDate.get(indexNumber).getOrderNumber();
        }
        maxNumber = maxNumber + 1;
        try {
            activeOrder = getProduct(activeOrder);
        } catch (PriceAndTaxProductDaoException ex) {
            throw new PriceAndTaxProductDaoException("Couldn't locate a Product");
        }
        try {
            activeOrder = getTax(activeOrder);
        } catch (PriceAndTaxProductDaoException ex) {
            throw new PriceAndTaxProductDaoException("Couldn't Locat a Tax");
        }
        if (activeOrder.getProductComponent() == null || activeOrder.getTaxComponent() == null) {
            return null;
        }

        String customerName = activeOrder.getCustomerName();
        String name = customerName.replace(",", "%");
        activeOrder.setCustomerName(name);
        activeOrder.setOrderNumber(maxNumber);
        activeOrder = computesAllOrders(activeOrder);

        try {
            dao.addOrderToDao(activeOrder);
        } catch (FloorMasteryDaoException ex) {
            throw new FloorMasteryServiceNegativeFundsException("Something went wrong feeding the file in");

        }

        String displayCustomerName = activeOrder.getCustomerName();
        displayCustomerName = displayCustomerName.replace("%", ",");
        activeOrder.setCustomerName(displayCustomerName);

        BigDecimal negativeChecker = new BigDecimal("0.00");
        int negativeLabor = activeOrder.getFinalLaborCosts().compareTo(negativeChecker);
        int negFinalTaxCosts = activeOrder.getMaterialCost().compareTo(negativeChecker);

        int negFinalTotal = activeOrder.getFinalTaxCosts().compareTo(negativeChecker);
        int negMaterialCosts = activeOrder.getFinalTotal().compareTo(negativeChecker);
        if (negativeLabor >= 0 && negFinalTaxCosts >= 0 && negFinalTotal >= 0 && negMaterialCosts >= 0) {
        } else {
            throw new FloorMasteryServiceNegativeFundsException("A negative number is present");
        }
        return activeOrder;
    }

    @Override
    public void replaceOrder(Order activeOrder) throws FloorMasteryServiceNegativeFundsException {

        List<Order> userEditedOrders = new ArrayList<Order>();
        List<Order> allOrders = null;

        try {
            allOrders = dao.getAllOrders(activeOrder);
        } catch (FloorMasteryDaoException ex) {
            throw new FloorMasteryServiceNegativeFundsException("Something went wrong feeding the file in");
        }

        try {
            activeOrder = getProduct(activeOrder);

        } catch (PriceAndTaxProductDaoException ex) {

        }
        try {
            activeOrder = getTax(activeOrder);
        } catch (PriceAndTaxProductDaoException ex) {

        }

        for (Order currentOrder : allOrders) {
            if (currentOrder.getOrderNumber() == activeOrder.getOrderNumber()) {

                currentOrder.setOrderNumber(activeOrder.getOrderNumber());
                currentOrder.setCustomerName(activeOrder.getCustomerName());
                currentOrder.getTaxComponent().setState(activeOrder.getTaxComponent().getState());
                currentOrder.getTaxComponent().setTaxRate(activeOrder.getTaxComponent().getTaxRate().setScale(2, RoundingMode.DOWN));
                currentOrder.getProductComponent().setProduct(activeOrder.getProductComponent().getProduct());
                currentOrder.setArea(activeOrder.getArea());
                currentOrder.getProductComponent().setCostperSquareFoot(activeOrder.getProductComponent().getCostperSquareFoot());
                currentOrder.getProductComponent().setLaborCostperSquareFoot(activeOrder.getProductComponent().getLaborCostperSquareFoot());
                currentOrder.setMaterialCost(activeOrder.getMaterialCost());
                currentOrder.setFinalLaborCosts(activeOrder.getFinalLaborCosts());
                currentOrder.setFinalTaxCosts(activeOrder.getFinalTaxCosts());
                currentOrder.setFinalTotal(activeOrder.getFinalTotal());
                computesAllOrders(activeOrder);
            }
            userEditedOrders.add(currentOrder);
        }
        for (Order currentOrder : userEditedOrders) {
            BigDecimal negativeChecker = new BigDecimal("0.00");
            int negativeLabor = currentOrder.getFinalLaborCosts().compareTo(negativeChecker);
            int negFinalTaxCosts = currentOrder.getMaterialCost().compareTo(negativeChecker);
            int negFinalTotal = currentOrder.getFinalTaxCosts().compareTo(negativeChecker);
            int negMaterialCosts = currentOrder.getFinalTotal().compareTo(negativeChecker);
            if (negativeLabor >= 0 && negFinalTaxCosts >= 0 && negFinalTotal >= 0 && negMaterialCosts >= 0) {
            } else {
                throw new FloorMasteryServiceNegativeFundsException("A negative number is present");
            }
        }
        try {
            dao.update(userEditedOrders, activeOrder);
        } catch (FloorMasteryDaoException ex) {
            throw new FloorMasteryServiceNegativeFundsException("Something went wrong feeding the file in");
        }
    }

    private Order computesAllOrders(Order activeOrder) throws FloorMasteryServiceNegativeFundsException {
        Order currentOrder = new Order();

        BigDecimal areaSize = activeOrder.getArea();
        BigDecimal materialCostPerSqFoot = activeOrder.getProductComponent().getCostperSquareFoot();
        BigDecimal laborCostsPerSqFoot = activeOrder.getProductComponent().getLaborCostperSquareFoot();
        BigDecimal taxRate = activeOrder.getTaxComponent().getTaxRate().setScale(2, RoundingMode.DOWN);
        BigDecimal zeroOne = new BigDecimal(".01");
        taxRate = taxRate.multiply(zeroOne);
        activeOrder.setMaterialCost(areaSize.multiply(materialCostPerSqFoot));
        activeOrder.setFinalLaborCosts(areaSize.multiply(laborCostsPerSqFoot));
        BigDecimal preTaxCost = activeOrder.getMaterialCost().add(activeOrder.getFinalLaborCosts());
        activeOrder.setFinalTaxCosts(preTaxCost.multiply(zeroOne));
        activeOrder.setFinalTotal(activeOrder.getFinalTaxCosts().add(preTaxCost));

        BigDecimal negativeChecker = new BigDecimal("0.00");

        int negativeLabor = activeOrder.getFinalLaborCosts().compareTo(negativeChecker);
        int negFinalTaxCosts = activeOrder.getMaterialCost().compareTo(negativeChecker);
        int negFinalTotal = activeOrder.getFinalTaxCosts().compareTo(negativeChecker);
        int negMaterialCosts = activeOrder.getFinalTotal().compareTo(negativeChecker);
        if (negativeLabor >= 0 && negFinalTaxCosts >= 0 && negFinalTotal >= 0 && negMaterialCosts >= 0) {
        } else {
            throw new FloorMasteryServiceNegativeFundsException("A negative number is present");
        }

        return activeOrder;

    }

    @Override
    public List<Order> findAllOrder(Order activeOrder) throws FloorMasteryServiceException {
        LocalDate dateHolder = activeOrder.getOrderDate();
        try {
            dao.setDate(activeOrder);
        } catch (FloorMasteryDaoException ex) {
            throw new FloorMasteryServiceException("Something went wrong settng the date");
        }
        List<Order> ordersForDisplay = null;
        try {
            ordersForDisplay = dao.getAllOrders(activeOrder);
        } catch (FloorMasteryDaoException ex) {
            throw new FloorMasteryServiceException("Failed to gather all orders");
        }
        if (ordersForDisplay == null) {
            return null;

        }
        for (Order currentOrder : ordersForDisplay) {

            currentOrder.setOrderDate(dateHolder);
        }
        for (Order currentOrder : ordersForDisplay) {
            int orderLackskey = currentOrder.getOrderNumber();
            if (orderLackskey < 0) {
                throw new FloorMasteryServiceException("An null(empty) order was was found");
            }
        }

        for (Order selectedOrder : ordersForDisplay) {
            String toMakeNameCorrect = selectedOrder.getCustomerName();
            toMakeNameCorrect = toMakeNameCorrect.replace("%", ",");
            activeOrder.setCustomerName(toMakeNameCorrect);
        }

        return ordersForDisplay;

    }

    @Override
    public void findAndRemoveOrder(Order activeOrder) throws FloorMasteryServiceException {
     
        List<Order> allOrders = null;
        allOrders = dao.removeOrder(activeOrder);

    }

    @Override
    public void saveChanges(Order activeOrder) throws FloorMasteryServiceException {
        try {
            dao.saveChanges();
        } catch (FloorMasteryDaoException ex) {
            throw new FloorMasteryServiceException("We were unable to locate the file to remove");
        }
    }

    @Override
    public Order findAndEditOrder(Order orderToEdit) {

        List<Order> allOrders = null;
        Order orderFoundToEdit = new Order();
        orderFoundToEdit = null;
        try {
            orderFoundToEdit = dao.getOneOrder(orderToEdit);
        } catch (FloorMasteryDaoException ex) {
        }
        if (orderFoundToEdit == null) {
            return null;
        }

        String customerName = orderFoundToEdit.getCustomerName();
        String updatedName = customerName.replace("%", ",");
        customerName = updatedName;
        orderFoundToEdit.setCustomerName(customerName);

        return orderFoundToEdit;
    }

    private Order getProduct(Order activeOrder) throws FloorMasteryServiceNegativeFundsException, PriceAndTaxProductDaoException {

        Products foundProduct = priceDao.getInventoryPrices(activeOrder.getProductComponent().getProduct());
        activeOrder.setProductComponent(foundProduct);
        if (foundProduct == null) {
            throw new FloorMasteryServiceNegativeFundsException("An Invalid Product is present");
        }

        return activeOrder;
    }

    private Order getTax(Order activeOrder) throws FloorMasteryServiceNegativeFundsException, PriceAndTaxProductDaoException {
        Tax foundTax = taxDao.getTaxAmounts(activeOrder.getTaxComponent().getState());
        if (foundTax == null) {
            throw new FloorMasteryServiceNegativeFundsException("An Invalid Tax is present");
        }
        activeOrder.setTaxComponent(foundTax);

        return activeOrder;
    }

    @Override
    public List<Tax> getTaxsList() {
        taxList = taxDao.getListofTax();
        return taxList;
    }

    @Override
    public List<Products> getProductList() {
        productList = priceDao.getListOfProducts();
        return productList;
    }

}
