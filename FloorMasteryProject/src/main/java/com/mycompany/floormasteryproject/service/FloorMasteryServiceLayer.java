/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.service;

import com.mycompany.floormasteryproject.dao.FloorMasteryDaoException;
import com.mycompany.floormasteryproject.dto.Order;
import com.mycompany.floormasteryproject.dto.Products;
import com.mycompany.floormasteryproject.dto.Tax;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author michael
 */
public interface FloorMasteryServiceLayer {

    public List<Order> findOrderDate(Order activeOrder) throws FloorMasteryServiceException;

    public Order addAnOrder(Order activeOrder) throws FloorMasteryServiceNegativeFundsException, com.mycompany.floormasteryproject.dao.PriceAndTaxProductDaoException;

    public void replaceOrder(Order activeOrder) throws FloorMasteryServiceNegativeFundsException;

    public List<Order> findAllOrder(Order activeOrder) throws FloorMasteryServiceException;

    public void findAndRemoveOrder(Order activeOrder) throws FloorMasteryServiceException;

    public void saveChanges(Order activeOrder) throws FloorMasteryServiceException;

    public Order findAndEditOrder(Order activeOrder) throws FloorMasteryServiceException;

    public List<Tax> getTaxsList();

    public List<Products> getProductList();

}
