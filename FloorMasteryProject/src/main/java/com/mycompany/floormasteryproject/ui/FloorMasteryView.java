/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.ui;

import com.mycompany.floormasteryproject.dto.Order;
import com.mycompany.floormasteryproject.dto.Products;
import com.mycompany.floormasteryproject.dto.Tax;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.eclipse.core.internal.runtime.Product;

/**
 *
 * @author michael
 */
public class FloorMasteryView {

    private String placeHolderForDateDisplay;
    private String placeHolderForTrainingDisplay;
    private FloorMasteryUserIO io;

    public FloorMasteryView(FloorMasteryUserIO io) {
        this.io = io;

    }

    public int trainingMenu() {
        io.print("*************************************************************************");
        io.print("*                  please select a mode                                 *");
        io.print("*************************************************************************");
        io.print("1) Training:");
        io.print("2) Live    :");
        int selection = io.readInt("Please Select an option:", 1, 2);
        if (selection == 1) {
            placeHolderForTrainingDisplay = "training:";
        } else {
            placeHolderForTrainingDisplay = "Live";
        }

        return selection;

    }

    public void menuSelection(int training) {
        io.print("*************************************************************************");
        if (training == 1) {
            placeHolderForTrainingDisplay = "Training";
            io.print("* Training mode is activated this means that no information will be     *");
            io.print("* Saved and no records altered the program will still function          *");
        }

        if (training == 2) {
            placeHolderForTrainingDisplay = "Live";
            io.print("* Program is Live and all record changes are permanent                  *");
            io.print("* All databases will be accessable and alterable                        *");
        }
        io.print("*************************************************************************");
    }

    public int mainMenuSelection(Order activeOrder) {

        io.print("  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ");
        io.print("*  <<Flooring Program>> current mode:" + placeHolderForTrainingDisplay);
        io.print("* 1. Display Orders   ");
        io.print("* 2. Add an Order     ");
        io.print("* 3. Edit an Order    ");
        io.print("* 4. Remove an Order  ");
        io.print("* 5. Save Current Work");
        io.print("* 6. Quit             ");
        io.print("* ");
        io.print(" * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ");
        return io.readInt("Please make a choice ", 1, 6);
    }

    public Order addOrderDisplay(Order activeOrder, List<Products> productList, List<Tax> taxList, boolean editDate) {

        io.print("Please enter the following information Status :" + placeHolderForTrainingDisplay);
        io.print("Information is now case sensitive:");
        if (activeOrder.getCustomerName() == null) {
            activeOrder.setCustomerName(io.readString("Please enter customers name"));
        } else {
            String isNameblank = (io.readString("Please enter customers name: (Current name):" + activeOrder.getCustomerName()));
            if (isNameblank.contentEquals("")) {
                io.print("Current Name will remain");
            } else {
                activeOrder.setCustomerName(isNameblank);
            }
        }
        boolean dateFail = true;
        LocalDate enteredDate = null;
        if (editDate == true) {
            do {

                if (activeOrder.getOrderDate() == null) {
                    enteredDate = io.readDate("Please enter a date MMDDYYYY format");
                } else {
                    try {
                        String dateBlank = io.readString("Please enter date: (Current name):" + activeOrder.getOrderDate());
                        if (dateBlank.equals("")) {
                            io.print("Current Date will remain");
                            dateFail = false;
                        } else {
                            enteredDate = LocalDate.parse(dateBlank, DateTimeFormatter.ofPattern("MMddyyyy"));
                            activeOrder.setOrderDate(enteredDate);
                            dateFail = false;
                        }
                    } catch (java.time.format.DateTimeParseException ex) {
                        enteredDate = io.readDate("Please enter a date MMDDYYYY format");
                        dateFail = false;
                    }
                }

            } while (dateFail == true);
        }
        boolean viewFail = true;
        do {

            String stateSelector = null;

            if (activeOrder.getTaxComponent() == null) {
                io.print("Please select a state");
            } else {
                io.print("Please select a state: (Current State):" + activeOrder.getTaxComponent().getState());
            }

            for (Tax toDisplay : taxList) {
                io.print(toDisplay.getState());
            }

            if (activeOrder.getTaxComponent() == null) {
                stateSelector = io.readString("select a State");
            } else {
                stateSelector = io.readString("Select a State");
                if (stateSelector.contentEquals("")) {
                    io.print("Current State will remain");
                    stateSelector = activeOrder.getTaxComponent().getState();
                }

            }
            for (Tax taxItem : taxList) {
                if (taxItem.getState().equals(stateSelector)) {
                    activeOrder.setTaxComponent(taxItem);
                    viewFail = false;
                }
            }
        } while (viewFail == true);

        viewFail = true;
        do {
            String productSelector = null;
            if (activeOrder.getProductComponent() == null) {
                io.print("Please select a product");
            } else {
                io.print("Please select a product: (Current Product):" + activeOrder.getProductComponent().getProduct());
            }
            for (Products toDisplay : productList) {
                io.print(toDisplay.getProduct());
            }
            if (activeOrder.getProductComponent() == null) {
                productSelector = io.readString("Please select a product:");
            } else {
                productSelector = io.readString("Select a Product");
                if (productSelector.contentEquals("")) {
                    io.print("Current Product will remain");
                    productSelector = activeOrder.getProductComponent().getProduct();
                }
            }
            for (Products productItem : productList) {
                if (productItem.getProduct().equals(productSelector)) {
                    activeOrder.setProductComponent(productItem);
                    viewFail = false;
                }
            }
        } while (viewFail == true);

        int negArea = 0;
        BigDecimal forCompare = new BigDecimal("1.00");
        do {
            if (negArea == 0) {
                activeOrder.setArea(io.readBigDecimal("Please enter the Area of the room"));
            } else {
                activeOrder.setArea(io.readBigDecimal("Please enter an Area of the room that is greater than 0"));
            }

            negArea = activeOrder.getArea().compareTo(forCompare);
        } while (negArea < 0);

        return activeOrder;
    }

    public LocalDate displayOrderDate() {
        LocalDate enteredDate = io.readDate("Please enter a date MMDDYYYY format");
        String convertedDate = enteredDate.format(DateTimeFormatter.ofPattern("MMddyyyy"));
        placeHolderForDateDisplay = convertedDate;
        return enteredDate;
    }

    public void displayOrders(List<Order> forDisplay) {
        ;
        io.print("********************************************************************");
        io.print("Date of:" + placeHolderForDateDisplay + "  Orders found:");
        io.print("********************************************************************");
        io.readString("Press Enter to Continue");
        for (Order toDisplay : forDisplay) {
            io.print("Status :" + placeHolderForTrainingDisplay);
            io.print("Order Date :" + toDisplay.getOrderDate());
            io.print("Customer Name:" + toDisplay.getCustomerName());
            io.print("OrderNumber :" + toDisplay.getOrderNumber());
            io.print("State: " + toDisplay.getTaxComponent().getState());
            io.print("TaxRate :" + toDisplay.getTaxComponent().getTaxRate());
            io.print("Product :" + toDisplay.getProductComponent().getProduct());
            io.print("Area :" + toDisplay.getArea());
            io.print("Cost Per Square Foot :" + toDisplay.getProductComponent().getCostperSquareFoot());
            io.print("Labor Costs Per Sq Foot :" + toDisplay.getProductComponent().getLaborCostperSquareFoot());
            io.print("Material Cost :" + toDisplay.getMaterialCost());
            io.print("Final Labor costs :" + toDisplay.getFinalLaborCosts());
            io.print("Final Tax costs :" + toDisplay.getFinalTaxCosts());
            io.print("Final Total :" + toDisplay.getFinalTotal());
            io.print("********************************************************************");
            io.print("********************************************************************");
            io.print("********************************************************************");
            io.print("********************************************************************");

        }
    }

    public void printSingleOrder(Order orderToPrint) {
        io.print("********************************************************************");
        io.print("             Order has been added to the file                       ");
        io.print("Added to:" + placeHolderForDateDisplay + "  Orders added at:" + orderToPrint.getOrderNumber());
        io.print("********************************************************************");
        io.readString("Press Enter to Continue");
        io.print("Status :" + placeHolderForTrainingDisplay);
        io.print("Order Date :" + orderToPrint.getOrderDate());
        io.print("Customer Name:" + orderToPrint.getCustomerName());
        io.print("OrderNumber :" + orderToPrint.getOrderNumber());
        io.print("State: " + orderToPrint.getTaxComponent().getState());
        io.print("TaxRate :" + orderToPrint.getTaxComponent().getTaxRate());
        io.print("Product :" + orderToPrint.getProductComponent().getProduct());
        io.print("Area :" + orderToPrint.getArea());
        io.print("Cost Per Square Foot :" + orderToPrint.getProductComponent().getCostperSquareFoot());
        io.print("Labor Costs Per Sq Foot :" + orderToPrint.getProductComponent().getLaborCostperSquareFoot());
        io.print("Material Cost :" + orderToPrint.getMaterialCost());
        io.print("Final Labor costs :" + orderToPrint.getFinalLaborCosts());
        io.print("Final Tax costs :" + orderToPrint.getFinalTaxCosts());
        io.print("Final Total :" + orderToPrint.getFinalTotal());
    }

    public int SelectOrderEdit(Order activeOrder, int sizeOfFile) {
        sizeOfFile = sizeOfFile + 1;

        io.print("***********************************************************************");
        io.print("       Edit Order selected            Status:" + placeHolderForTrainingDisplay + "");
        io.print("***********************************************************************");

        io.print("***********************************************************************");
        int orderToEdit = io.readInt("Please enter the Order Number you would like to edit", 1, sizeOfFile);
        io.print("**********************************************************************");

        return orderToEdit;

    }

    public Order SelectOrderRemove(Order activeOrder, int maximumNumberOfRecords) {
        Order editedOrder = new Order();
        io.print("***********************************************************************");
        io.print("       Remove Order selected            Status:" + placeHolderForTrainingDisplay + " ");
        io.print("***********************************************************************");

        io.print("***********************************************************************");
        int orderToEdit = io.readInt("Please enter the Order Number you would like to edit", 1, maximumNumberOfRecords);
        io.print("**********************************************************************");
        editedOrder.setOrderNumber(orderToEdit);

        return editedOrder;
    }

    public void fireEndProgram() {
        io.print("***********************************************************************");
        io.print("    Program end            Status:" + placeHolderForTrainingDisplay + " ");
        io.print("***********************************************************************");
    }

    public void fireNoMatchError() {
        io.print("***********************************************************************");
        io.print("    Date not found            Status:" + placeHolderForTrainingDisplay + " ");
        io.print(" No date matches the following value " + placeHolderForDateDisplay);
        io.print("***********************************************************************");
    }

    public int keepOrDiscard() {
        io.print("***********************************************************************");
        io.print(" Would you like to keep these changes?");
        io.print(" 1) Yes");
        io.print(" 2) No");
        io.print("***********************************************************************");
        return io.readInt(": ", 1, 2);
    }

    public void abortOrder(Order activeOrder) {
        io.print("***********************************************************************");
        io.print(" The change was aborted , no changes were made to Orders               ");
        io.print("***********************************************************************");
    }

    public void noDataInDate() {
        io.print("***********************************************************************");
        io.print("No Data in the file sending you back to main  menu          ");
        io.print("***********************************************************************");
    }

    public void noDataInOrder() {
        io.print("***********************************************************************");
        io.print("No Data in the with that Order Number it was likely deleted.       ");
        io.print("***********************************************************************");
    }

    public void negativeAmountFound() {
        io.print("***********************************************************************");
        io.print("Negative Amount found                                                  ");
        io.print("***********************************************************************");
    }

}
