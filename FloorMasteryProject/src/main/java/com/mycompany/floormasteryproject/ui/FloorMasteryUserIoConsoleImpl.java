/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject.ui;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.FormatStyle;
import static java.time.temporal.TemporalQueries.localDate;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author michael
 */
public class FloorMasteryUserIoConsoleImpl implements FloorMasteryUserIO {

    Scanner VendingMachineScanner = new Scanner(System.in);
    PrintWriter VendingMachinePrinter = new PrintWriter(System.out);

    @Override
    public void print(String msg) {
        System.out.println(msg);
    }

    @Override
    public double readDouble(String msg, double minRange, double maxRange) {
        boolean badInput = true;
        double result = 0;
        do {
            try {
                System.out.println(msg);
                result = Double.parseDouble(VendingMachineScanner.nextLine());
                badInput = false;
                if (result < minRange) {
                    System.out.println(" Please enter a number no lower than " + minRange);
                } else if (result > maxRange) {
                    System.out.println(" Please enter a number no higher than " + maxRange);
                }
            } catch (NumberFormatException e) {
                System.out.println(" Please enter valid input, double");

            }
        } while (result < minRange || result > maxRange || badInput == true);
        return result;
    }

    @Override
    public float readFloat(String msg, float minRange, float maxRange) {
        boolean badInput;
        float result = 0;
        do {
            try {
                System.out.println(msg);
                result = Float.parseFloat(VendingMachineScanner.nextLine());
                badInput = false;
                if (result < minRange) {
                    System.out.println(" Please enter a number no lower than " + minRange);
                } else if (result > maxRange) {
                    System.out.println(" Please enter a number no higher than " + maxRange);
                }

            } catch (NumberFormatException e) {
                badInput = true;
                System.out.println(" Please enter valid input, float");;

            }
        } while (result < minRange || result > maxRange || badInput == true);
        return result;
    }

    @Override
    public long readLong(String msg, long minRange, long maxRange) {
        boolean badInput;
        long result = 0;
        do {
            try {
                System.out.println(msg);
                result = Long.parseLong(VendingMachineScanner.nextLine());
                badInput = false;
                if (result < minRange) {
                    System.out.println(" Please enter a number no lower than " + minRange);
                } else if (result > maxRange) {
                    System.out.println(" Please enter a number no higher than " + maxRange);
                }
            } catch (NumberFormatException e) {
                badInput = true;
                System.out.println(" Please enter valid input, long");

            }
        } while (result < minRange || result > maxRange || badInput == true);
        return result;
    }

    @Override
    public int readInt(String msg, int minRange, int maxRange) {
        boolean badInput;
        int result = 0;
        do {
            try {
                System.out.println(msg);
                result = Integer.parseInt(VendingMachineScanner.nextLine());
                badInput = false;
                if (result < minRange) {
                    System.out.println(" Please enter a number no lower than " + minRange);
                } else if (result > maxRange) {
                    System.out.println(" Please enter a number no higher than " + maxRange);
                }
            } catch (NumberFormatException e) {
                badInput = true;
                System.out.println(" Please enter valid input, int");

            }
        } while (result < minRange || result > maxRange || badInput == true);
        return result;
    }

    @Override
    public BigDecimal readBigDecimal(String message, BigDecimal minRange, BigDecimal maxRange) {
        boolean badInput;
        BigDecimal result = new BigDecimal("0.00");
        int minimumRange = 0;
        int maximumRange = 0;
        do {
            try {
                System.out.println(message);
                result = VendingMachineScanner.nextBigDecimal();
                badInput = false;
                minimumRange = result.compareTo(minRange);
                maximumRange = result.compareTo(maxRange);
                if (minimumRange < 0) {
                    System.out.println(" Please enter a number no lower than " + minRange);
                } else if (maximumRange > 0) {
                    System.out.println(" Please enter a number no higher than " + maxRange);
                }
            } catch (NumberFormatException e) {
                badInput = true;
                System.out.println(" Please enter valid input, int");

            }
        } while (minimumRange < 0 || maximumRange > 0 || badInput == true);
        return result;
    }

    @Override
    public int readInt(String msg) {
        boolean badinput;
        int result = 0;
        do {
            try {
                System.out.println(msg);
                result = result = Integer.parseInt(VendingMachineScanner.nextLine());
                badinput = false;

            } catch (NumberFormatException e) {
                badinput = true;
                System.out.println(" Please enter valid input, Int");
            }
        } while (badinput == true);
        return result;
    }

    @Override
    public double readDouble(String msg) {
        boolean badinput;
        double result = 0;
        do {
            try {
                System.out.println(msg);
                result = Double.parseDouble(VendingMachineScanner.nextLine());
                badinput = false;
            } catch (NumberFormatException e) {
                badinput = true;
                System.out.println(" Please enter valid input, Double");
            }
        } while (badinput = true);
        return result;
    }

    @Override
    public float readFloat(String msg) {
        boolean badinput = true;
        float result = 0;
        do {
            try {
                System.out.println(msg);
                result = Float.parseFloat(VendingMachineScanner.nextLine());
                badinput = false;
            } catch (NumberFormatException e) {

                System.out.println(" Please enter valid input, float");
            }
        } while (badinput = true);
        return result;
    }

    @Override
    public long readLong(String msg) {
        boolean badinput;
        long result = 0;
        do {
            try {
                System.out.println(msg);
                result = Long.parseLong(VendingMachineScanner.nextLine());
                badinput = false;
            } catch (NumberFormatException e) {
                badinput = true;
                System.out.println(" Please enter valid input, Long");
            }
        } while (badinput = true);
        return result;
    }

    @Override
    public BigDecimal readBigDecimal(String message) {
        boolean badInput = false;
        String result;
        BigDecimal returnMe = new BigDecimal("0.00");
        do {

            try {
                System.out.println(message);
                badInput = false;
                result = VendingMachineScanner.nextLine();
                returnMe = new BigDecimal(result).setScale(2, RoundingMode.DOWN);
            } catch (java.lang.NumberFormatException e) {
                badInput = true;
                System.out.println("Please enter a valid BigDecimal");
            }

        } while (badInput == true);

        return returnMe;
    }

    @Override
    public String readString(String prompt) {
        System.out.println(prompt);
        return VendingMachineScanner.nextLine();

    }

    @Override
    public LocalDate readDate(String msg) {
        String result = null;
        LocalDate information = null;
        boolean badInput = false;
        do {
            try {
                System.out.println(msg);
                badInput = false;
                result = VendingMachineScanner.nextLine();
                information = LocalDate.parse(result, DateTimeFormatter.ofPattern("MMddyyyy"));
            } catch (DateTimeParseException e) {
                badInput = true;
                System.out.println("Please enter a valid Date");

            }
        } while (badInput == true);

        result = information.format(DateTimeFormatter.ofPattern("MMddyyyy"));
        return information;
    }

}
