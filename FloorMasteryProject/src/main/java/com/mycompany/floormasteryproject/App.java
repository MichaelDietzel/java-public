/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.floormasteryproject;

import com.mycompany.floormasteryproject.controller.FloorMasteryController;
import com.mycompany.floormasteryproject.dao.FloorMasteryDao;
import com.mycompany.floormasteryproject.dao.FloorMasteryDaoException;
import com.mycompany.floormasteryproject.dao.PriceAndTaxProductDaoException;
import com.mycompany.floormasteryproject.service.FloorMasteryServiceNegativeFundsException;
import com.mycompany.floormasteryproject.service.FloorMasteryServiceException;
import com.mycompany.floormasteryproject.service.FloorMasteryServiceNonMatchException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author michael
 */
public class App {

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        FloorMasteryController controller = ctx.getBean("controller", FloorMasteryController.class);

        controller.run();

    }

}
