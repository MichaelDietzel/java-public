
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class Pass implements UserIO {

    Scanner userInput = new Scanner(System.in);

    @Override
    public void print(String message) {
        System.out.println(" Please pick again");
    }

    @Override
    public double readDouble(String prompt) {
        double input;
        System.out.println("Please enter an double");
        System.out.println(prompt);
        input = userInput.nextInt();
        return input;
    }

    @Override
    public double readDouble(String prompt, double min, double max) {
        double input;
        int compare = 0;
        System.out.println("Please enter an double between" + min +"&" + max);
        System.out.println(prompt);
        input = userInput.nextInt();

        // You mentioned this should be a do while I disagree. The If is conditionla
        // The do would be a sure thing, I don't know what their pick will be and need 
        // to validate this against other outcomes.
        if (input >= min && input <= max) {
            compare = 1;
        }
        while (compare != 1) {
            print(prompt);
            input = userInput.nextInt();
            if (input > min && input < max) {
                compare = 1;
            }
        }

        return input;
    }


    @Override
    public float readFloat(String prompt) {
        float input;
        System.out.println("Please enter an float");
        System.out.println(prompt);
        input = userInput.nextInt();

        return input;
    }

    @Override
    public float readFloat(String prompt, float min, float max) {
        float input;
        int compare = 0;
        System.out.println("Please enter an float between" + min +"&" + max);
        System.out.println(prompt);
        input = userInput.nextInt();

        // You mentioned this should be a do while I disagree. The If is conditionla
        // The do would be a sure thing, I don't know what their pick will be and need 
        // to validate this against other outcomes.
        if (input >= min && input <= max) {
            compare = 1;
        }
        while (compare != 1) {
            input = userInput.nextInt();
            print(prompt);
            if (input > min && input < max) {
                compare = 1;
            }
        }

        return input;
    }

    @Override
    public int readInt(String prompt) {
        int input;
        System.out.println("Please enter an integer");
        System.out.println(prompt);
        input = userInput.nextInt();

        return input;
    }

    @Override
    public int readInt(String prompt, int min, int max) {
        int input;
        int compare = 0;
        System.out.println("Please enter an int between" + min +"&" + max);
        System.out.println(prompt);
        input = userInput.nextInt();

        // You mentioned this should be a do while I disagree. The If is conditionla
        // The do would be a sure thing, I don't know what their pick will be and need 
        // to validate this against other outcomes.
        if (input >= min && input <= max) {
            compare = 1;
        }
        while (compare != 1) {
            input = userInput.nextInt();
            print(prompt);
            if (input > min && input < max) {
                compare = 1;
            }
        }

        return input;
    }

    @Override
    public long readLong(String prompt) {
        long input;
        System.out.println("Please enter a long");
        System.out.println(prompt);
        input = userInput.nextInt();

        return input;
    }

    @Override
    public long readLong(String prompt, long min, long max) {
        long input;
        int compare = 0;
        System.out.println("Please enter a long between" + min +"&" + max);
        System.out.println(prompt);
        input = userInput.nextInt();

        // You mentioned this should be a do while I disagree. The If is conditionla
        // The do would be a sure thing, I don't know what their pick will be and need 
        // to validate this against other outcomes.
        if (input >= min && input <= max) {
            compare = 1;
            
        }
        while (compare != 1) {
            input = userInput.nextInt();
            print(prompt);
            if (input > min && input < max) {
                compare = 1;
            }
        }
        return input;
    }

    @Override
    public String readString(String prompt) {
        System.out.println(prompt);
        String input = userInput.nextLine();

        return input;
    }

}
