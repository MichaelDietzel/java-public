/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class TheOrderOfThings {
    public static void main(String[] args) {
        
        // Variable Declartion
        double number;
        String opinion, size, age, shape, color, origin, material, purpose;
        String noun;
        
        number = 5.0;
        opinion = "Awesome";
        size = "teensy-weensy";
        age = "new";
        shape = "oblong";
        color = "BRIGHT yellow";
        origin = "AlphaCentaurian";
        material= "Platinum";
        purpose = "good";
        
        noun = "dragons";
        
        // Using the + with strings doesn't add it concatenes!
        System.out.println(number + opinion + size + age + shape + color + origin + material + purpose + noun);
        // Honestly the output sounds fine in the context of space or item discovery so I didn't find it just added context that fits.
        
        
        // Better print out
        System.out.println("Findings on alpha Centurai : ");
        System.out.println(" number of objects found : " + number); 
        System.out.println(" Object Opinon : " + opinion);
        System.out.println(" Object size : " + size );
        System.out.println(" Age of Object : " + age );
        System.out.println(" Shape of Object : " + shape);
        System.out.println(" Color of Onject : " + color);
        System.out.println(" Origin of Object : "+ origin);
        System.out.println(" Material of Object : " + material);
        System.out.println("Purpose of Object : "+ purpose);
        System.out.println("Name of Object : " + noun);
    }
}
