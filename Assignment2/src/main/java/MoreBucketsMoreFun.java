/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class MoreBucketsMoreFun {
    public static void main(String[] args){
    // Variable Decleration
    int butterflies, beetles, bugs;
    String color, size, shape, thing;
    double number;
    // Double seems to just wrap an int as a double
    // pre set values sources sales
    butterflies =2;
    beetles = 4;
    
    // 3) Bugs is determined here frezing it in memory
    bugs = butterflies + beetles;
    System.out.println("there are only " + butterflies +" Butterflies");
    System.out.println("but " + bugs + " Bugs in total.");
    System.out.println(" Uh oh , my dog ate one ");
    
    butterflies--;
    // 2)Butterflies -- is causing a single point reduction in the butterflies
    // 3.b) bug = butterflies + bettles // would cause this funciton
    // 3.b bugs was  tallied on 23 and butterflies was reduced on 31. What happened on 31 will not influence line 23 
    // what is displayed on 34 is the value added on 23
    System.out.println("now there are only " + butterflies + " Butterflies left.");
    System.out.println("Bust still " + bugs + " bugs left wait a minute!!!");
    System.out.println("Maybe I just counted wrong the first time ...");
    }    
}
