/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class SpaceRustlers {
    public static void main(String[] args){
        
        // decarle ints
        int spaceships = 10;
        int aliens = 25;
        int cows = 100;
        
        if (aliens > spaceships){
            System.out.println(" there are enough pilots lets go");
        } else { 
            System.out.println("there aren't enough green guys to drive these ships");
        }
        
        if (cows == spaceships ){
            System.out.println("there are just enough cows for the ships");
        }else if (cows > spaceships){
            // Else if is  conditional statement related to the inital if. Where failure in this case != causes a secondary else to fire.
            // If you remove the else you run the risk or (desired or not_) of having them both fire.
            // With Else if you prevent that by  creating a success state or failure state with defined outcomes.
            System.out.println("Yum , Yum eat a bunch  before we go ");     
        }else {
            System.out.println(" More ships than cows!");
        }
        
        // 4
        
        if (aliens > cows){
            System.out.println("hamburger party on alpha centuraio");
        } else if (cows > aliens) { 
            System.out.println(" Looks like aliens are the new hamburger");
            
        }
    }
}
