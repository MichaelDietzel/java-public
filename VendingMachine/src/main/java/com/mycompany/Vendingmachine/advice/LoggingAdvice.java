/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Vendingmachine.advice;

import com.mycompany.vendingmachine.dao.VendingMachineAuditDao;
import com.mycompany.vendingmachine.dao.VendingMachineDaoException;
import com.mycompany.vendingmachine.service.VendingMachineServiceException;
import com.mycompany.vendingmachine.service.VendingMachineServiceInsufficentFundsException;
import com.mycompany.vendingmachine.service.VendingMachineServiceLayerImpl;
import org.aspectj.lang.JoinPoint;

/**
 *
 * @author michael
 */
public class LoggingAdvice {
   VendingMachineAuditDao auditDao;
   
 public LoggingAdvice(VendingMachineAuditDao auditDao){
      this.auditDao = auditDao;
  }
 
public void createExceptionAuditEntry(JoinPoint jp, VendingMachineServiceInsufficentFundsException dataAccessEx){
      Object[] args = jp.getArgs();
      String auditEntry = jp.getSignature().getName() + ": VendingMachineServiceInsufficentFundsException";
      for (Object currentArg : args) {
          auditEntry += currentArg;
          
      }
      try {
          auditDao.writeAuditEntry(auditEntry);
          
      }catch(VendingMachineDaoException ex){
          System.err.println("ERROR: could not create audit entery in logging advice:");
      }
}
   
 
  public void  createAuditEntry(JoinPoint jp){
      Object[] args = jp.getArgs();
      String auditEntry = jp.getSignature().getName() + ": ";
      for (Object currentArg : args) {
          auditEntry += currentArg;
          
      }
      try {
          auditDao.writeAuditEntry(auditEntry);
          
      }catch(VendingMachineDaoException e){
          System.err.println("ERROR: could not create audit entery in logging advice:");
      }
  }
}
