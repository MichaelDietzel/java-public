/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.controller;

import com.mycompany.vendingmachine.dao.VendingMachineDaoException;
import com.mycompany.vendingmachine.dto.Coins;
import com.mycompany.vendingmachine.dto.Item;
import com.mycompany.vendingmachine.service.VendingMachineServiceException;
import com.mycompany.vendingmachine.service.VendingMachineServiceInsufficentFundsException;
import com.mycompany.vendingmachine.service.VendingMachineServiceLayer;
import com.mycompany.vendingmachine.service.VendingMachineServiceLayerImpl;
import com.mycompany.vendingmachine.ui.VendingMachineView;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author michael
 */
public class VendingMachineController {
  // ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
  // VendingMachineView view = ctx.getBean("myView", VendingMachineView.class);
  // VendingMachineServiceLayer service = ctx.getBean("myService", VendingMachineServiceLayer.class);    
        
    private VendingMachineView view;
    private VendingMachineServiceLayer service;

   public VendingMachineController(VendingMachineServiceLayer service, VendingMachineView view) {
      this.service = service;
      this.view = view;

    }

    public void run() throws VendingMachineServiceException, VendingMachineServiceInsufficentFundsException, VendingMachineDaoException {
        List<Item> inventory = loadInventory();
        intialInventoryDisplay(inventory);
        mainMenu(inventory);

    }
    private void mainMenu(List<Item> inventory) throws VendingMachineServiceInsufficentFundsException, VendingMachineServiceException, VendingMachineDaoException {
        boolean mainMoneyMenuIsActive = true;
        int mainMoneyMenuSelection = 0;
        BigDecimal totalFunds = new BigDecimal("0.00");
        while (mainMoneyMenuIsActive == true) {
            mainMoneyMenuSelection = getMenuSelection();

            switch (mainMoneyMenuSelection) {
                case 1:

                    BigDecimal money = new BigDecimal("0.00");
                    money = view.addMoneyToMachine();
                    addMoney(money);
                    break;
                case 2:
                    try {
                        makePurchase(inventory);
                    } catch (VendingMachineDaoException e) {
                        view.anErrorOccured();
                    }
                    break;
                case 3:
                    getChange();
                case 4:
                    mainMoneyMenuIsActive = false;

            }
        }
    }
    private int getMenuSelection() {
        return view.menuSelection();
    }

    private void addMoney(BigDecimal addedMoney) throws VendingMachineServiceInsufficentFundsException {
        BigDecimal totalFunds = service.addmoney(addedMoney);
        view.displayMoney(totalFunds);
    }

    private void makePurchase(List<Item> inventory)
            throws VendingMachineServiceException, VendingMachineServiceInsufficentFundsException, VendingMachineDaoException {

        boolean hasErrors = true;

        do {
            String userVendingSelection = view.getVendingSelection(inventory);
            try {
                Item selectedItem = service.retreiveItem(userVendingSelection);
                service.subtractFunds(selectedItem);
                view.dispensed(selectedItem);
                service.updateInventory(selectedItem);
                Coins coin = service.changeDispense();
                view.dispenseMoney(coin);
                hasErrors = false;

            } catch (VendingMachineServiceInsufficentFundsException e) {
                view.insufficentfunds(userVendingSelection);
                Coins coin = service.changeDispense();
                view.dispenseMoney(coin);
                mainMenu(inventory);
                
            } catch (VendingMachineServiceException e) {
                view.inventorySoldOut(userVendingSelection);
                mainMenu(inventory);
            }
        } while (hasErrors == true);
    }

    private List<Item> loadInventory() throws VendingMachineServiceException {

        List<Item> inventory = service.getInventory();

        return inventory;
    }

    private void intialInventoryDisplay(List<Item> inventory) {
        view.intialInventorydisplay(inventory);
    }



    private void getChange() throws VendingMachineServiceInsufficentFundsException {
        Coins coin = service.changeDispense();
        view.dispenseMoney(coin);
    }

}
