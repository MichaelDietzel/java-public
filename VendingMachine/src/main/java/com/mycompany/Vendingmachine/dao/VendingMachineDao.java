/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.dao;


import com.mycompany.vendingmachine.dto.Item;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author michael
 */
public interface VendingMachineDao {

    public void getInventory() throws VendingMachineDaoException;

    public Item retreiveItem(String userVendingSelection)throws VendingMachineDaoException;

    public void writeInventory(List AllItems)throws VendingMachineDaoException;
    
    public void createInventory(Item currentItem) throws VendingMachineDaoException;
    
    public void deleteInventory(Item CurrentItem) throws VendingMachineDaoException;
    
    public List <Item> allItems() throws VendingMachineDaoException;
    
    public void updateInventory(Item updateItem) throws VendingMachineDaoException;
}
