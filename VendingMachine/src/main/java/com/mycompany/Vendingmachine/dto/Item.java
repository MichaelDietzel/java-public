/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.dto;

import java.math.BigDecimal;
import java.util.Objects;

/**
 *
 * @author michael
 */
public class Item {
    private String itemIndexlocation;
    private String itemName;
    private BigDecimal itemCost;
    private int numbeOfItems;
    
    public Item() {}
    
    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public BigDecimal getItemCost() {
        return itemCost;
    }

    public void setItemCost(BigDecimal itemCost) {
        this.itemCost = itemCost;
    }

    public int getNumbeOfItems() {
        return numbeOfItems;
    }

    public void setNumbeOfItems(int numbeOfItems) {
        this.numbeOfItems = numbeOfItems;
    }

    public String getItemIndexlocation() {
        return itemIndexlocation;
    }

    public void setItemIndexlocation(String itemIndexlocation) {
        this.itemIndexlocation = itemIndexlocation;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.itemIndexlocation);
        hash = 71 * hash + Objects.hashCode(this.itemName);
        hash = 71 * hash + Objects.hashCode(this.itemCost);
        hash = 71 * hash + this.numbeOfItems;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Item other = (Item) obj;
        if (this.numbeOfItems != other.numbeOfItems) {
            return false;
        }
        if (!Objects.equals(this.itemIndexlocation, other.itemIndexlocation)) {
            return false;
        }
        if (!Objects.equals(this.itemName, other.itemName)) {
            return false;
        }
        if (!Objects.equals(this.itemCost, other.itemCost)) {
            return false;
        }
        return true;
    }
    @Override
    public String toString(){
        return "index location:" +itemIndexlocation + "| itemName:" + itemName;
                
                
    }
}
