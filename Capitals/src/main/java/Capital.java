
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class Capital {
    private String name;
    private int population;
    private int squareMilage;
    private String location;

   
    
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public int getSquareMilage() {
        return squareMilage;
    }

    public void setSquareMilage(int squareMilage) {
        this.squareMilage = squareMilage;
    }
    
    public void PrintOut (){
        
        
        System.out.println("Name          :" + name);
        System.out.println("Location      :" + location);
        System.out.println("Square mile   :" + squareMilage );
        System.out.println("Poulation     :" + population);
        
    }
    public static int ranGen(){
        int generateNumber;
        Random rGen = new Random();
        generateNumber = rGen.nextInt(1000000);
        
        return generateNumber;
    }
}
    